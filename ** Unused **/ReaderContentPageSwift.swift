//
//  ReaderContentPageSwift.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 12/11/16.
//
//

import Foundation

class ReaderContentPageSwift: UIView {

  var BASE_ZOOM: CGFloat = (UIDevice.current.userInterfaceIdiom == .pad ? 1.0 : 1.55 )

  
  var _PDFDocRef: CGPDFDocument! = nil
  var _PDFPageRef: CGPDFPage! = nil
  
  var _pageAngle: Int = 0
  
  var _pageWidth: CGFloat = 0
  var _pageHeight: CGFloat = 0
  
  var _pageOffsetX: CGFloat = 0
  var _pageOffsetY: CGFloat = 0
  
  var _effectiveRect: CGRect = CGRect.zero
  
  weak var _animationView: UIView? = nil
  
  override class var layerClass: AnyClass {
    return ReaderContentTile.self
  }

  /*
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
    if let touch = touches.first {
    let point =  pdfPointFromViewPoint( touch.location(in: self))
    NSLog("point %.0f %.0f ", point.x, point.y);
    }
    
  DataSource.selectRange(NSMakeRange(0, 10000) as NSValue)
    setNeedsDisplay()
    
    super.touchesBegan(touches, with: event)
  }*/

  func pdfPointFromViewPoint(_ point: CGPoint) -> CGPoint {
    let y =  _effectiveRect.size.height - point.y / BASE_ZOOM + _effectiveRect.origin.y;
    let x = point.x / BASE_ZOOM  + _effectiveRect.origin.x;
    return CGPoint(x: x, y: y)
  }
  
  func pdfPointToViewPoint(_ point: CGPoint) -> CGPoint {
    let y = (_effectiveRect.size.height - point.y + _effectiveRect.origin.y ) * BASE_ZOOM;
    let x = (point.x - _effectiveRect.origin.x ) * BASE_ZOOM;
    return CGPoint(x: x, y: y)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    autoresizesSubviews = false
    isUserInteractionEnabled = true
    contentMode = .redraw
    autoresizingMask = []
    backgroundColor = UIColor.white
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  convenience init(pdfDocument: CGPDFDocument, page: Int, password phrase: String) {
    
    var viewRect = CGRect.zero; // View rect
    self.init(frame: viewRect)

    var page = page
    if page < 1 { page = 1 }
    
    _PDFDocRef = pdfDocument
    
    let pages = _PDFDocRef.numberOfPages;
    
    if (page > pages) { page = pages } // Check the upper page bounds
    
    _PDFPageRef = _PDFDocRef.page(at: page); // Get page
    
    if (_PDFPageRef != nil) // Check for non-NULL CGPDFPageRef
    {
      //CGPDFPageRetain(_PDFPageRef); // Retain the PDF page
      
      let cropBoxRect = _PDFPageRef.getBoxRect(.cropBox);
      let mediaBoxRect = _PDFPageRef.getBoxRect(.mediaBox);
      let effectiveRect = cropBoxRect.intersection(mediaBoxRect);
      
      _effectiveRect = effectiveRect;
      _pageAngle = Int(_PDFPageRef.rotationAngle); // Angle
      
      if _pageAngle == 0 || _pageAngle == 180 {
        _pageWidth = effectiveRect.size.width;
        _pageHeight = effectiveRect.size.height;
        _pageOffsetX = effectiveRect.origin.x;
        _pageOffsetY = effectiveRect.origin.y;
      }
      
      if _pageAngle == 90 || _pageAngle == 270  {
        _pageWidth = effectiveRect.size.height;
        _pageHeight = effectiveRect.size.width;
        _pageOffsetX = effectiveRect.origin.y;
        _pageOffsetY = effectiveRect.origin.x;
      }
      
      
      var page_w = Int(_pageWidth) // Integer width
      var page_h = Int(_pageHeight) // Integer height
      
      if (page_w % 2) == 1 { page_w -= 1 }
      if (page_h % 2) == 1 { page_h -= 1 } // Even
      
      viewRect.size = CGSize(width: CGFloat(page_w) * BASE_ZOOM, height: CGFloat(page_h) * BASE_ZOOM); // View size
    }
    else // Error out with a diagnostic
    {
      //CGPDFDocumentRelease(_PDFDocRef), _PDFDocRef = NULL;
      
        assert(false, "CGPDFPageRef == NULL");
      }

    self.frame = viewRect

  }
  
  override func removeFromSuperview() {
    self.layer.delegate = nil
    super.removeFromSuperview()
  }
  
  override func didMoveToWindow() {
    contentScaleFactor = 1.0
  }
  
  NOT USED
  override func draw(_ layer: CALayer, in context: CGContext) {
    
    let readerContentPage = self; // Retain self
    
    context.setFillColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0); // White
    context.fill(context.boundingBoxOfClipPath); // Fill
    
    let pnum = _PDFPageRef.pageNumber;
    
    
    //NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(CGContextGetClipBoundingBox(context)));
    
    context.translateBy(x: 0.0, y: self.bounds.size.height);
    context.scaleBy(x: 1.0 * BASE_ZOOM, y: -1.0 * BASE_ZOOM );
    
    var bounds = self.bounds;
    bounds.size.width /= BASE_ZOOM;
    bounds.size.height /= BASE_ZOOM;
    
    context.concatenate(_PDFPageRef.getDrawingTransform(.cropBox, rect: bounds, rotate: 0, preserveAspectRatio: true));
    
    //CGContextSetRenderingIntent(context, kCGRenderingIntentDefault); CGContextSetInterpolationQuality(context, kCGInterpolationDefault);
    
    context.saveGState();
    func drawHighlights() {
      UIGraphicsPushContext(context);
      DataSource.drawFindHighlightForPage( NSNumber(value: pnum - 1))
      DataSource.drawSelectionForPage( NSNumber(value: pnum - 1))
      DataSource.drawUsersHighlightsForPage( NSNumber(value: pnum - 1))
      
      UIGraphicsPopContext();
    }
    
    if Thread.isMainThread { drawHighlights() }
    else { DispatchQueue.main.sync { drawHighlights() } }
    
    context.restoreGState();
    
    context.setBlendMode(.normal);
    context.drawPDFPage(_PDFPageRef); // Render the PDF page into the context
    
    //if (readerContentPage != nil) { readerContentPage = nil } // Release self
    
  }
  
  func animate(at p: CGPoint) {
    if( _animationView != nil ) {
      _animationView?.removeFromSuperview()
      _animationView = nil;
    }
    
    let view = UIView(frame: CGRect(origin: p, size: CGSize(width: 50, height: 50)))
    _animationView = view;
    view.backgroundColor = UIColor.red
    view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
    view.layer.shadowRadius = 10.0;
    view.layer.shadowOpacity = 1;
    view.layer.shadowOffset = CGSize(width: 0, height: 10);
    view.layer.cornerRadius = 10;
    
    addSubview(view)
    
    _animationView?.transform = CGAffineTransform.identity
    _animationView?.frame = CGRect(x: p.x - 10, y: p.y - 10, width: 20, height: 20)
    
    UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseOut, animations: {
      self._animationView?.transform = CGAffineTransform(scaleX: 3.0, y: 3.0);
      self._animationView?.alpha = 0.0;
    }, completion:
      nil);

  }
  
}
