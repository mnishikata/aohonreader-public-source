//
//  CloudKitRepository.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation
import CloudKit

final class CloudKitRepository {
  private var hasPrivateAccount_: Bool? = nil
  private var container_: CKContainer? = nil
  private var database_: CKDatabase? = nil
  static var shared = CloudKitRepository()
  
  init() {
    
    container_  = CKContainer(identifier: "iCloud.com.catalystwo.AohonReader")
    
    if container_ != nil {
      
      container_?.accountStatus { (status, error) -> Void in
        
        print("CloudKit Status \(status)")
        
        if .available == status || .couldNotDetermine == status {
          self.hasPrivateAccount_ = true
        }
        
        if .restricted == status || .noAccount == status{
          self.hasPrivateAccount_ = false
        }
      }
      
      database_ = container_!.publicCloudDatabase
    }
  }
  
  class func updateIfHasPrivateAccount()  {
    shared.hasPrivateAccount(callback: {(hasPrivateAccount: Bool)->Void in})
  }
  
  func hasPrivateAccount(callback: @escaping ((_ hasPrivateAccount: Bool)->Void))  {
    
    hasPrivateAccount_ = nil
    
    container_?.accountStatus { (status, error) -> Void in
      
      DispatchQueue.main.async {
        
        if .available == status || .couldNotDetermine == status {
          self.hasPrivateAccount_ = true
          callback(true)
          
        }else if .restricted == status {
          self.hasPrivateAccount_ = false
          callback(false)
          
        }else {
          callback(false)
        }
      }
    }
  }
  
  
  func report(_ highlightRange: ClosedRange<Int>) {
    
    let record = CKRecord(recordType: RecordType.highlight)
    record[RecordType.HighlightKey.startIndex] = highlightRange.lowerBound as CKRecordValue?
    record[RecordType.HighlightKey.endIndex] = highlightRange.upperBound as CKRecordValue?
    record[RecordType.HighlightKey.edition] = Law.edition as CKRecordValue?


    let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: nil)
    
    saveOperation.savePolicy = .changedKeys
    saveOperation.qualityOfService = .userInitiated
    saveOperation.queuePriority = .normal
    
    saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in }
    
    database_?.add(saveOperation)
  }
  
  func saveRating(_ source: RatingsSource) {
    let record = CKRecord(recordType: RecordType.review)
    record[RecordType.ReviewKey.rating] = source.rating as CKRecordValue?
    record[RecordType.ReviewKey.reviewText] = source.reviewText as CKRecordValue?
    record[RecordType.ReviewKey.title] = source.title as CKRecordValue?
    record[RecordType.ReviewKey.userUuid] = source.userUuid as CKRecordValue?
    record[RecordType.ReviewKey.version] = source.version as CKRecordValue?
    
    let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: nil)
    
    saveOperation.savePolicy = .changedKeys
    saveOperation.qualityOfService = .userInitiated
    saveOperation.queuePriority = .normal
    
    saveOperation.modifyRecordsCompletionBlock = { (records: [CKRecord]?, recordID: [CKRecordID]?, error: Error?) -> Void in }
    
    database_?.add(saveOperation)
  }
  
  func getBl(_ handler:@escaping ((_ blackListed: Bool)->Void)) {
    
    let ub = NSUbiquitousKeyValueStore.default
    ub.synchronize()
    
    guard let myUuid = ub.object(forKey: "userUuid") as? String else {
      return // HAVENT WRITTEN YET
    }
    
    if let bl = UserDefaults.standard.object(forKey: "bl") as? [String] {
      if bl.contains(myUuid) {
        handler(true)
        return
      }
    }
    
    let recordId = CKRecordID(recordName: RecordType.blackListRecordID) // BL RECORD ID
    database_?.fetch(withRecordID: recordId) { (record, error) in
      
      if error == nil {
        //print(record)
        if let userUuids = record?["userUuids"] as? [String] {
          
          UserDefaults.standard.set(userUuids, forKey:"bl")
          if userUuids.contains(myUuid) {
            // ******
            DispatchQueue.main.async {
              handler(true)
            }
          }
        }
        
      }else {
        
        print(error!.localizedDescription)
      }
    }
  }
}
