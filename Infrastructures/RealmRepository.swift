//
//  RealmRepository.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation
import RealmSwift

final class RealmRepository {
  
  //MARK:- CONST
  static var documentUrl: URL = {
    
    let folder = FileManager.default.urls(for: .documentDirectory,  in: .userDomainMask)[0]
    let destUrl = folder.appendingPathComponent("Document.pdf")
    return destUrl
  }()
  
  static private var databaseUrl: URL! = {
    let cachesDirectoryPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
    let cachesDirectoryURL = NSURL(fileURLWithPath: cachesDirectoryPath)
    guard let fileURL = cachesDirectoryURL.appendingPathComponent("Default.realm") else { return nil }
    return fileURL
  }()
  
  static let databaseSchemaVersion: UInt64 = 1
  
  static var shared = RealmRepository()
  
  lazy private var realm: Realm = { [weak self] in
    
    assert( nil == self?.installDatabase() )
    
    var config = Realm.Configuration(
      
      // Get the URL to the bundled file
      fileURL: RealmRepository.databaseUrl,
      // Open the file in read-only mode as application bundles are not writeable
      /*
       encryptionKey: kEncriptionKey.data(using: .utf8, allowLossyConversion: false),*/
      readOnly: true)
    
    // Open the Realm with the configuration
    config.schemaVersion = RealmRepository.databaseSchemaVersion
    let realm = try! Realm(configuration: config)
    
    return realm
    }()
  
  lazy private var diffRealm: Realm = { [weak self] in
    
    var config = Realm.Configuration(
      
      // Get the URL to the bundled file
      fileURL: URL(fileURLWithPath: Bundle.main.path(forResource: Law.diffName, ofType: "realm")!),
      // Open the file in read-only mode as application bundles are not writeable
      /*
       encryptionKey: kEncriptionKey.data(using: .utf8, allowLossyConversion: false),*/
      readOnly: true)
    
    // Open the Realm with the configuration
    config.schemaVersion = RealmRepository.databaseSchemaVersion
    let realm = try! Realm(configuration: config)
    
    return realm
    }()

  
  var isDocumentInstalled: Bool {
    if FileManager.default.fileExists(atPath: RealmRepository.documentUrl.path) {
      return isCorrectDocument(at: RealmRepository.documentUrl)
    }
    return false
  }
  
  func isCorrectDocument(at url: URL) -> Bool {
    if let data = try? Data(contentsOf: url) {
      let hashes =  Law.documentHashes//80623104//42510832
      let counts = Law.documentDataCounts//6846675//6609497
      //38314272 7040860
      if hashes.contains(data.hashValue) && counts.contains(data.count) {
        _ = url.setExcludedFromBackup(false)
        return true
      }
    }
    return false
  }
  
  var shouldDecompressDatabase: Bool {
    
    if Int(RealmRepository.databaseSchemaVersion) != UserDefaults.standard.integer(forKey: "DatabaseSchemaVersion") {
      return true
    }
    
    if FileManager.default.fileExists(atPath: RealmRepository.databaseUrl!.path) { return false }
    
    return true
  }
  
  
  func installDatabase() -> Error? {
    
    if shouldDecompressDatabase == false { return nil }
    
    guard let url = Bundle.main.url(forResource: "Database\(Law.editionString)", withExtension: "compressed") else { return LawError.fileError }
    guard let encryptedData = NSData(contentsOf: url) else { return LawError.fileError }
    guard let decrypted = encryptedData.aesDecrypt(key: kEncriptionKey, iv: "iv-salt-string--") else { return LawError.fileError }
    
    do {
      
      let decompressed = try BZipCompression.decompressedData(with: decrypted as Data!)
      try decompressed.write(to: RealmRepository.databaseUrl)
      
      UserDefaults.standard.set(RealmRepository.databaseSchemaVersion, forKey: "DatabaseSchemaVersion")
      
    } catch let error {
      return error
    }
    return nil
  }
  
  //MARK:- FETCHING PAGES

  func pages(in range: ClosedRange<Int16>) -> Results<Page> {
    return realm.objects(Page.self).filter("page >= %@ && page <= %@", range.lowerBound, range.upperBound).sorted(byKeyPath: "page")
  }

  func page(at index: Int16) -> Page? {
    return realm.objects(Page.self).filter("page == %@", index).first
  }
  
  //MARK:- FETCHING MOJIS
  func moji(at index: Int) -> Moji? {
    return realm.objects(Moji.self).filter("validCharacterNumber == %@", index).first
  }
  
  func mojis(in range: ClosedRange<Int>, beginsWith: String) -> Results<Moji> {
    return realm.objects(Moji.self).filter("character BEGINSWITH[c] %@ && validCharacterNumber <= %@ && validCharacterNumber >= %@", beginsWith,  range.upperBound, range.lowerBound)
  }
  
  //MARK:- FETCHING ARTICLES

  var articles: Results<Article> {
    let objects = realm.objects(Article.self).sorted(byKeyPath: "validCharacterNumber", ascending: true)
    
    return objects
  }
  
  func article(encodedTitle: String, law: Law) -> Article? {
    return realm.objects(Article.self).filter("encodedTitle == %@ && lawRaw == %@", encodedTitle , law.rawValue).first
  }
  
  func pageTitle(atPage page: Int16? = nil, atIndex validCharacterNumber: Int? = nil) -> Article? {
    if page != nil {
      return realm.objects(Article.self).filter("page < %@", page!).sorted(byKeyPath: "validCharacterNumber", ascending: false).first
    }
    
    if validCharacterNumber != nil {
      return realm.objects(Article.self).filter("validCharacterNumber <= %@", validCharacterNumber!).sorted(byKeyPath: "page", ascending: false).first
    }
    
    return nil
  }
  
  //MARK:- HIGHLIGHTS
  
  private var highlightRealm: Realm {
    let defaultRealm = try! Realm()
    return defaultRealm
  }
  
  func addHighlight(_ highlight: Highlight) {
    let realm = highlightRealm
    try! realm.write {
      highlight.timeStamp = NSDate()
      realm.add(highlight)
    }
  }
  
  func updateHighlight(_ highlight: Highlight) {
    let realm = highlightRealm
    try! realm.write {
      realm.add(highlight, update: true)
    }
  }
  
  func updateNotesFor(_ highlight: Highlight, notes: String) {
    let realm = highlightRealm
    try! realm.write {
      highlight.timeStamp = NSDate()
      highlight.notes = notes

      realm.add(highlight, update: true)
    }
  }
  
  func updateColorFor(_ highlight: Highlight, color: HighlightColor) {
    let realm = highlightRealm
    try! realm.write {
      highlight.timeStamp = NSDate()
      highlight.color = color
      
      realm.add(highlight, update: true)
    }
  }
  
  func deleteNotesFor(_ highlight: Highlight) {
    
    let realm = highlightRealm
    try! realm.write {
      highlight.timeStamp = NSDate()
      highlight.notes = nil
      realm.add(highlight, update: true)
    }
  }
  
  func deleteHighlight(_ highlight: Highlight) {
    
    let realm = highlightRealm
    try! realm.write {
      highlight.timeStamp = NSDate()
      highlight.isDeleted = true
      highlight.notes = nil
      highlight.highlightedText = nil
      realm.add(highlight, update: true)
    }
  }
  
  func destroyHighlight(_ highlight: Highlight) {
    
    let realm = highlightRealm
    try! realm.write {
      realm.delete(highlight)
    }
  }
  
  var highlights: Results<Highlight> {
    let objects = highlightRealm.objects(Highlight.self).filter("isDeleted != %@", true).sorted(byKeyPath: "startIndex", ascending: true)
    
    return objects
  }
  
  var allHighlights: Results<Highlight> {
    let objects = highlightRealm.objects(Highlight.self)
    return objects
  }
  
  func highlightsInPages(_ pages: ClosedRange<Int16>) -> Results<Highlight> {
    let objects = highlightRealm.objects(Highlight.self).filter("startPage <= %@ && %@ <= endPage && isDeleted != %@", pages.lowerBound, pages.upperBound, true).sorted(byKeyPath: "startIndex", ascending: true)
    
    return objects
  }
  
  func diffsInPages(_ pages: ClosedRange<Int16>) -> Results<Highlight> {
    let objects = diffRealm.objects(Highlight.self).filter("startPage <= %@ && %@ <= endPage", pages.lowerBound, pages.upperBound).sorted(byKeyPath: "startIndex", ascending: true)
    
    return objects
  }
  
  func writeHighlightRealmCopy(toFile newURL: URL) throws {
    objc_sync_enter(self)

    if FileManager.default.fileExists(atPath: newURL.path) {
      try FileManager.default.removeItem(at: newURL)
    }
    
    try highlightRealm.writeCopy(toFile: newURL)
    objc_sync_exit(self)
  }
  
  func mingleHighlights(from url: URL, completion: (()->Void)?) {
    
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
				queue.async {
          
          objc_sync_enter(self)
          print("== LOAD FROM ICLOUD \(url) ==")
          
          let config = Realm.Configuration(
            
            // Get the URL to the bundled file
            fileURL: url,
            // Open the file in read-only mode as application bundles are not writeable
            /*
             encryptionKey: kEncriptionKey.data(using: .utf8, allowLossyConversion: false),*/
            readOnly: true)
          
          // Open the Realm with the configuration
          guard let iCloudRealm = try? Realm(configuration: config) else { return }
          
          let highlightRealm = RealmRepository.shared.highlightRealm
          
          
          let objs =  iCloudRealm.objects(Highlight.self).sorted(byKeyPath: "timeStamp", ascending: false)
          try? highlightRealm.write {
            for obj in objs {
              autoreleasepool {
                let localCounterpart = highlightRealm.objects(Highlight.self).filter("uuid == %@", obj.uuid)
                if let localHighlight = localCounterpart.first {
                  
                  if localHighlight.timeStamp!.timeIntervalSinceReferenceDate < obj.timeStamp!.timeIntervalSinceReferenceDate {
                    
                    highlightRealm.create(Highlight.self, value: obj, update: true)
                    print(" - updating from iCloud \(String(describing: obj.highlightedText))")
                    
                  }else {
                    
                    // END ?
                    
                    //break
                  }
                  
                }else {
                  highlightRealm.create(Highlight.self, value: obj)
                  print(" - creating from iCloud \(String(describing: obj.highlightedText))")
                  
                }
              }
            }
          }
          objc_sync_exit(self)
          
          if completion != nil {
            DispatchQueue.main.async(execute: completion!)
          }
    }
  }
}
