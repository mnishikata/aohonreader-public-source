//
//  Law.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation

enum Law: Int8 {
  case patent = 0, utility, design, trademark, tokurei, kokugan
  
  // PDF METADATA
  
  // DATABSE PAGE RANGE
  #if EDITION19
  
  // EDITION 19
  static let edition = 19
  static let editionString = ""
  static let diffName = "Diff20-19"

  static let documentHashes: [Int] = [42510832, 38314272]
  static let documentDataCounts: [Int] = [6609497, 7040860]
  static let patentRange: ClosedRange<Int> = 50 ... 647
  static let utilityRange: ClosedRange<Int> = 832 ... 982
  static let designRange: ClosedRange<Int> = 1102 ... 1236
  static let trademarkRange: ClosedRange<Int> = 1290 ... 1636
  static let tokureiRage: ClosedRange<Int> = 1787 ... 1883
  static let kokuganRange: ClosedRange<Int> = 1915 ... 1956
  #else
  
  // EDITION 20
  static let edition = 20
  static let editionString = "20"
  static let diffName = "Diff19-20"

  static let documentHashes: [Int] = [80623104]
  static let documentDataCounts: [Int] = [6846675]
  static let patentRange: ClosedRange<Int> = 46 ... 705
  static let utilityRange: ClosedRange<Int> = 901 ... 1061
  static let designRange: ClosedRange<Int> = 1183 ... 1356
  static let trademarkRange: ClosedRange<Int> = 1407 ... 1783
  static let tokureiRage: ClosedRange<Int> = 1946 ... 2047
  static let kokuganRange: ClosedRange<Int> = 2073 ... 2123
  #endif
  
  //static let singleUilityRange: ClosedRange<Int> = 6 ... 156
  
  var humanReadableTitle: String {
    switch self {
    case .patent: return "特許法"
    case .utility: return "実用新案法"
    case .design: return "意匠法"
    case .trademark: return "商標法"
    case .tokurei: return "工業所有権に関する手続等の特例に関する法律"
    case .kokugan: return "国際出願法"
    }
  }
  
  var humanReadableAbbreviation: String {
    switch self {
    case .patent: return "特"
    case .utility: return "実"
    case .design: return "意"
    case .trademark: return "商"
    case .tokurei: return "工"
    case .kokugan: return "国"
    }
  }
  
  init?(page: Int) {
    
    if Law.patentRange.contains(page) { self = .patent }
    else if Law.utilityRange.contains(page) { self = .utility }
    else if Law.designRange.contains(page) { self = .design }
    else if Law.trademarkRange.contains(page) { self = .trademark }
    else { return nil }
  }
  
  func pageRange() -> ClosedRange<Int> {
    switch self {
    case .patent: return Law.patentRange
    case .utility: return Law.utilityRange
    case .design: return Law.designRange
    case .trademark: return Law.trademarkRange
    case .tokurei: return Law.tokureiRage
    case .kokugan: return Law.kokuganRange
      
    }
  }
}

