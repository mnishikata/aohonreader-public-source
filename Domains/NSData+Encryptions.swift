//
//  NSData+Encryptions.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 17/12/16.
//
//

import Foundation

extension NSData {
  
  func aesEncrypt(key:String, iv:String, options:Int = kCCOptionPKCS7Padding) -> NSData? {
    if let keyData = key.data(using: .utf8) as NSData?,
      let cryptData    = NSMutableData(length: Int((self.length)) + kCCBlockSizeAES128) {
      
      
      let keyLength              = size_t(kCCKeySizeAES128)
      let operation: CCOperation = UInt32(kCCEncrypt)
      let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES128)
      let options:   CCOptions   = UInt32(options)
      
      
      
      var numBytesEncrypted :size_t = 0
      
      let cryptStatus = CCCrypt(operation,
                                algoritm,
                                options,
                                keyData.bytes, keyLength,
                                iv,
                                self.bytes, self.length,
                                cryptData.mutableBytes, cryptData.length,
                                &numBytesEncrypted)
      
      if UInt32(cryptStatus) == UInt32(kCCSuccess) {
        cryptData.length = Int(numBytesEncrypted)
        return cryptData
        
        
      }
      else {
        return nil
      }
    }
    return nil
  }
  
  func aesDecrypt(key:String, iv:String, options:Int = kCCOptionPKCS7Padding) -> NSData? {
    if let keyData = key.data(using: .utf8) as NSData?,
      let cryptData    = NSMutableData(length: Int((self.length)) + kCCBlockSizeAES128) {
      
      let keyLength              = size_t(kCCKeySizeAES128)
      let operation: CCOperation = UInt32(kCCDecrypt)
      let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES128)
      let options:   CCOptions   = UInt32(options)
      
      var numBytesEncrypted :size_t = 0
      
      let cryptStatus = CCCrypt(operation,
                                algoritm,
                                options,
                                keyData.bytes, keyLength,
                                iv,
                                self.bytes, self.length,
                                cryptData.mutableBytes, cryptData.length,
                                &numBytesEncrypted)
      
      if UInt32(cryptStatus) == UInt32(kCCSuccess) {
        cryptData.length = Int(numBytesEncrypted)
        return cryptData
      }
      else {
        return nil
      }
    }
    return nil
  }
}
