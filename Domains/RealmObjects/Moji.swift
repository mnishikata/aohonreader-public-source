//
//  Moji.swift
//  PDFTest
//
//  Created by Masatoshi Nishikata on 6/11/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import RealmSwift

#if os(iOS)
  import CoreGraphics
  typealias NSRect = CGRect
#endif

final class Moji: Object {
  var lawRaw = RealmOptional<Int8>()
  @objc dynamic var page: Int16 = 0
  //dynamic var characerIndex: Int16 = 0
  @objc dynamic var validCharacterNumber: Int = 0
  @objc dynamic var x: Int16 = 0
  @objc dynamic var y: Int16 = 0
  var width = RealmOptional<Int16>()
  var height = RealmOptional<Int16>()
  let defaultLetterWidth: Int16 = 9
  @objc dynamic var character: String = ""
  var singleCharacter: String {
    return String(describing: character.characters.first!)
  }

  
  #if BUILDING_MODE
  
  #else
  /*

  override static func indexedProperties() -> [String] {
  return ["page", "validCharacterNumber", "character", "lawRaw"]
  }

  override static func primaryKey() -> String? {
  return "validCharacterNumber"
  }
   */

  #endif
  
  
  override static func ignoredProperties() -> [String] {
    return ["bounds", "law"]
  }
  
  var law: Law? {
    get {
    if lawRaw.value == nil { return nil }
    return Law(rawValue: lawRaw.value! )
    }
    set {
      if newValue == nil {
        lawRaw.value = nil
      }else {
      lawRaw.value = newValue!.rawValue
      }
    }
  }
  
  var bounds: NSRect {
    set {
      x = Int16(round(newValue.origin.x))
      y = Int16(round(newValue.origin.y))
      if Int16(round(newValue.size.width)) != defaultLetterWidth {
        width.value = Int16(ceil(newValue.size.width))
      }
      if Int16(round(newValue.size.height)) != defaultLetterWidth {
        height.value = Int16(ceil(newValue.size.height))
      }
    }
    get {
      return NSRect(x: Int(x), y: Int(y), width: Int(width.value ?? defaultLetterWidth), height: Int(height.value ?? defaultLetterWidth))
    }
  }
}

