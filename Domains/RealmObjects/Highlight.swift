//
//  Highlight.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation
import RealmSwift

class Organizable: Object {
  @objc dynamic var uuid: String = ""
  @objc dynamic var filepath: String = ""
}

final class Highlight: Organizable {
  
  //dynamic var serialId: String = ""
  //dynamic var edition: String = "19"
  @objc dynamic var timeStamp: NSDate? = nil // char index
  @objc dynamic var isDeleted: Bool = false // char index
  
  @objc dynamic var highlightedText: String? = nil
  
  @objc dynamic var startPage: Int16 = 0 // char index
  @objc dynamic var endPage: Int16 = 0 // char index
  
  @objc dynamic var startIndex: Int = 0 // char index
  @objc dynamic var endIndex: Int = 0 // char index
  
  @objc dynamic var articleEncodedTitle: String?
  @objc dynamic var lawRaw: Int8 = 0
  var law: Law {
    set {
      lawRaw = Int8(newValue.rawValue)
    }
    
    get {
      return Law(rawValue: Int8(lawRaw)) ?? .patent
    }
  }
  
  
  @objc dynamic var startString: String = "" // prefix: 2 letters at the start position
  @objc dynamic var endString: String = "" // suffix: 2 letters at the end position
  
  @objc dynamic var startN: Int16 = 0 // from N-th location from articleEncodedTitle
  @objc dynamic var endN: Int16 = 0 // from N-th location from　articleEncodedTitle
  @objc dynamic var n: Int16 = 0 // from N-th location from　articleEncodedTitle
  
  @objc dynamic var notes: String? = nil
  
  @objc dynamic var colorRaw: Int8 = Int8(HighlightColor.yellow.rawValue)
  var color: HighlightColor {
    set {
      colorRaw = Int8(newValue.rawValue)
    }
    
    get {
      return HighlightColor(rawValue: Int(colorRaw))
    }
  }
  
  override static func indexedProperties() -> [String] {
    return ["notes", "uuid", "highlightedText", "timeStamp", "startPage", "endPage", "isDeleted"]
  }
  
  override static func primaryKey() -> String? {
    return "uuid"
  }
  
}
