//
//  Page.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation
import RealmSwift

final class Page: Object {
  
  class func databasePageToBookPage(_ page: Int) -> Int {
    return page - 40 + 1
  }
  
  class func bookPageToDatabasePage(_ page: Int) -> Int {
    return page + 40 - 1
  }
  
  @objc dynamic var startValidCharacterNumber: Int = 0
  @objc dynamic var endValidCharacterNumber: Int = 0
  //dynamic var headerString: String = ""
  @objc dynamic var page: Int16 = 0
  let mojis = List<Moji>()
  
  override static func indexedProperties() -> [String] {
    return ["page"]
  }
  
}
