//
//  Article.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation
import RealmSwift

final class Article: Object {
  
  @objc dynamic var encodedTitle: String = ""
  @objc dynamic var validCharacterNumber: Int = 0
  @objc dynamic var lawRaw: Int8 = 0
  @objc dynamic var page: Int16 = 0
  
  var law: Law {
    set {
      lawRaw = newValue.rawValue
    }
    get {
      return Law(rawValue: lawRaw)!
    }
  }
  
  override static func indexedProperties() -> [String] {
    return ["encodedTitle", "lawRaw"]
  }
}
