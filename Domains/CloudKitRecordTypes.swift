//
//  CloudKitRecordTypes.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 17/12/16.
//
//

import Foundation

enum RecordType {
  static let highlight = "Highlight"
  static let review = "Review"
  
  enum HighlightKey {
    static let startIndex = "startIndex", endIndex = "endIndex", edition = "edition"
  }
  
  enum ReviewKey {
    static let rating = "rating"
    static let reviewText = "reviewText"
    static let title = "title"
    static let userUuid = "userUuid"
    static let version = "version"
  }
  
  enum ReportKey {
    static let title = "title"
  }
  
  static let blackListRecordID = "c5a8d85c-8eac-4e7f-a477-f6d34eff6713"
}
