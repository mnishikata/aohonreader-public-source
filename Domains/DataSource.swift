//
//  DataSource.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 7/11/16.
//
//

import Foundation
import RealmSwift

extension String {
  var decodedArticleTitle: String {
    // [660]-1
    
    var returnValue = ""
    for character in characters {
      var kanji: String = ""
      switch character{
      case "0" : kanji = "０"
      case "1" : kanji = "１"
      case "2" : kanji = "２"
      case "3" : kanji = "３"
      case "4" : kanji = "４"
      case "5" : kanji = "５"
      case "6" : kanji = "６"
      case "7" : kanji = "７"
      case "8" : kanji = "８"
      case "9" : kanji = "９"
      case "(" : kanji = "第"
      case ")" : kanji = "条"
      case "-" : kanji = "の"
      default: kanji = String(character)
      }
      
      returnValue += kanji
    }
    return returnValue
  }
  
  func transliterateToKanjiNumber() -> String {
    var total: String = ""
    var digit: String
    
    for character in characters {
      switch character {
        
      case "１" : digit = "一"
      case "２" : digit = "二"
      case "３" : digit = "三"
      case "４" : digit = "四"
      case "５" : digit = "五"
      case "６" : digit = "六"
      case "７" : digit = "七"
      case "８" : digit = "八"
      case "９" : digit = "九"
      case "０" : digit = "〇"
      default: digit = String(character)
      }
      
      total += digit
    }
    
    return total
  }

}


// NOTIFICATION

enum DataSourceNotification {
  static let selectionDidChange = Notification.Name("DataSourceSelectionDidChange")
  static let highlightTouched = Notification.Name("DataSourceHighlightTouched")
  static let setNeedsDisplay = Notification.Name("DataSourceSetNeedsDisplay")
  static let focus = Notification.Name("DataSourceFocus")
}


final class DataSource: NSObject {
  
  //MARK:-
  static let shared = DataSource()
  
  var errorHandler: ((_ error:Error) -> Void)?
  
  override init() {
    super.init()
    let iCloudRepo = ICloudRepository.shared
    iCloudRepo.cloudDidUpdateHandler = { url in self.cloudDidChange(at: url) }
    iCloudRepo.conflictHandler = { ( allVersions: [NSFileVersion], decisionHandler: @escaping ICloudConflictDecisionHandler) in
      print("### Handling Conflicts ###")
      decisionHandler(allVersions.first!)
    }
    iCloudRepo.errorHandler = errorHandler // TODO
    iCloudRepo.startICloudQuery()
  }
  
  //MARK:- USE CASES
  
  var isReady: Bool {
    return RealmRepository.shared.isDocumentInstalled && RealmRepository.shared.shouldDecompressDatabase == false
  }
  
  func installDatabase() -> Error? {
    return RealmRepository.shared.installDatabase()
  }
  
  var document: ReaderDocument! {
    return ReaderDocument.withDocumentFilePath(RealmRepository.documentUrl.path, password: nil)
  }
  
  func installPDF(from url: URL) throws -> Bool {
    guard true == RealmRepository.shared.isCorrectDocument(at: url) else { return false }
    try FileManager.default.copyItem(at: url, to: RealmRepository.documentUrl)
    _ = url.setExcludedFromBackup()
    return true
  }
  
  func exportingHighlights(completion handler: (Data)->Void) {
    let count = DataSource.shared.numberOfHighlights
    var dictionary: [[String: Any]] = []
    for i in 0 ..< count {
      autoreleasepool {
        let highlight = DataSource.shared.highlight(at: i)
        
        /*
         Highlight data (such as articleEncodedTitle) weren't correct. Get Proper article now
         */
        var dict: [String: Any] = DataSource.shared.highlightDictionaryForMigration(highlight)
        
        //print("\(highlight.articleEncodedTitle!):\(dict["articleEncodedTitle"]!) \(highlight.startN):\(dict["startN"]!)")
        
        dict["uuid"] = highlight.uuid
        dict["timeStamp"] = highlight.timeStamp?.timeIntervalSinceReferenceDate
        dict["edition"] = Law.edition
        
        dict["notes"] = highlight.notes
        dict["colorRaw"] = highlight.colorRaw
        
        dictionary.append(dict)
      }
    }
    let data: Data = (try? JSONSerialization.data(withJSONObject:dictionary, options: [])) ?? Data()
    
    handler(data)
  }
  
  
  func importHighlights(at url: URL, completion handler: (NSAttributedString)->Void) {
    guard let data = try? Data(contentsOf: url) else { return }
    guard let dictArray = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]  else { return }
    
    var migradted = 0
    var missing = 0
    var alreadyAdded = 0
    
    let attributedText = NSMutableAttributedString(string: "")
    let boldFont = UIFont.boldSystemFont(ofSize: 14)
    let plainFont = UIFont.systemFont(ofSize: 14)

    
    for dict in dictArray! {
        // 1. Already exists?
      guard let uuid = dict["uuid"] as? String else { continue }
      guard let highlightedText = dict["highlightedText"] as? NSString else { continue }
      guard let startN = dict["startN"] as? Int else { continue }
      guard let n = dict["n"] as? Int else { continue }
      let color = HighlightColor(rawValue: dict["colorRaw"] as? Int ?? 0)
      let notes = dict["notes"] as? String
      let results = RealmRepository.shared.allHighlights.filter("uuid == %@", uuid)
      if results.count != 0 {
        if let h0 = results.first {
          updateHighlight(h0, color, notes: notes)
        }
        alreadyAdded += 1
        continue
      }
      
      
      // 2. Get article location
      
      var lowerLimit = 0
      var lawMask: [Law] = []
      if let encodedTitle = dict["articleEncodedTitle"] as? String,
        let lawRaw = dict["lawRaw"]  as? Int8,
        let law = Law(rawValue: lawRaw ) {
        
        if let article = RealmRepository.shared.article(encodedTitle: encodedTitle , law: law) {
          lowerLimit = article.validCharacterNumber
          lawMask = [law]
        }
      }
      
      // 3. Find highlightedText using find func
      
      let ranges = find(highlightedText, in: lawMask, after: lowerLimit)
      var matched = false
      var selectionRange: Selection? = nil
      // 4. Do startN, endN, and n match?
      for hoge in 0 ..< ranges.count {
        let range = ranges[hoge]
        
        let from = range.lowerBound
        if articleAt(at: from)?.validCharacterNumber != lowerLimit { break } // OUT OF RANGE
        
        if n == hoge {
          matched = true
          
          guard let startMoji = RealmRepository.shared.moji(at: range.lowerBound) else { return }
          guard let endMoji = RealmRepository.shared.moji(at: range.upperBound-1) else { return }

          
          selectionRange = Selection(selectedRange: range.lowerBound...range.upperBound-1, pageRange: startMoji.page...endMoji.page)
          break
        }
        
        if n <= hoge {
          break
        }
      }
      
      // 5. --> If yes, add
      if matched {
        print("*** match ***")
        
        migradted += 1
        addHighlight(color, range: selectionRange!, uuid: uuid, notes: notes)
      }
        
        // 6. --> If no, ??
      else {
        
        print("--- NOT match ---")
        missing += 1
        
        // BUILD REPORT
        
        let law = Law(rawValue: dict["lawRaw"] as? Int8 ?? 0 )!

        var title = (dict["articleEncodedTitle"] as? String)?.decodedArticleTitle ?? ""
        title = law.humanReadableTitle + title

        let titleAttr = NSAttributedString(string: "\n" + title + " ", attributes: [.font: boldFont])
        
        var attributes = [.font: plainFont, .backgroundColor: color.color()] as [NSAttributedStringKey : Any]
        
        if color == .underline {
          attributes = [.font: plainFont, .underlineStyle: NSNumber(value: 2),  .underlineColor: UIColor.red]
        }
        
        let bodyAttr = NSAttributedString(string: (dict["highlightedText"] as? String ?? ""), attributes: attributes)
        attributedText.append(titleAttr)
        attributedText.append(bodyAttr)

        let returnAttrr = NSAttributedString(string: "\n", attributes: [.font: boldFont])
        attributedText.append(returnAttrr)

      }
      
      print(dict)
    }
    
    if attributedText.length > 0 {
      
      let attr = NSMutableAttributedString(string: "以下のハイライトは移行できませんでした。この版では対象の文章が削除されたようです。\n")
      
      attributedText.insert(attr, at: 0)
    }
    
    let attr = NSMutableAttributedString(string: "移行処理が終了しました\n移行: \(migradted)、移行できず: \(missing)、すでに移行済み: \(alreadyAdded) \n\n")
    
    attributedText.insert(attr, at: 0)
    handler(attributedText)
  }
  
  //MARK:- ICLOUD DOCUMENT
  
  func cloudDidChange(at url: URL) {
    
    RealmRepository.shared.mingleHighlights(from: url) {
      NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil) // NOT CALLED??
    }
  }
  
  //MARK:- LAYOUT CALCULATIONS
  
  func mojiLocation(at point: CGPoint, page: Int16, boundaryBased: Bool = false) -> Int? {
    
    guard let pageObj = RealmRepository.shared.page(at: page) else { return nil } // UNKNOWN ERROR
    
    let intX = Int(point.x)
    let intY = Int(point.y)
    
    let possibleMojis = pageObj.mojis.filter("(x > %@) && (x < %@) && (y > %@) && (y < %@)", intX - 20, intX + 20, intY - 20, intY + 20)
    
    var currentLength: CGFloat? = nil
    var closest: Moji? = nil
    
    for moji in possibleMojis {
      let bounds = moji.bounds
      
      if boundaryBased {
        let center = CGPoint(x: bounds.midX, y: bounds.maxY)
        let length = pow(center.x - point.x , 2) + pow(center.y - point.y , 2)
        if currentLength == nil || currentLength! > length {
          currentLength = length
          closest = moji
        }
      }else {
        let bounds = moji.bounds
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let length = pow(center.x - point.x , 2) + pow(center.y - point.y , 2)
        if currentLength == nil || currentLength! > length {
          currentLength = length
          closest = moji
        }
      }
    }
    return closest?.validCharacterNumber
  }

  func wordRange(at location: Int, page: Int16) -> (validCharacterNumberRange: ClosedRange<Int>, pageRange: ClosedRange<Int16>) {
   
    // get -20 ... 20
    let range: ClosedRange<Int> = location - 10 ... location + 10
    var (substring, pages) = string(with: range, possiblePage: page)
    
    var targetRange: Range<String.Index>? = nil
    substring.enumerateSubstrings(in: substring.startIndex ..< substring.endIndex, options: .byWords) { (str, range1, range2, stop) -> () in
      if range1.contains(substring.index(substring.startIndex, offsetBy: 10)) {
        stop = true
        targetRange = range1
      }
    }
    if targetRange == nil {
      return (location...location, pages)
    }
    
    let lower = location + substring.distance(from: substring.startIndex, to: targetRange!.lowerBound ) - 10
    let upper = location + substring.distance(from: substring.startIndex, to: targetRange!.upperBound ) - 10
    
    if pages.lowerBound != pages.upperBound {
      
      if let lowerMoji = RealmRepository.shared.moji(at: lower),
        let upperMoji = RealmRepository.shared.moji(at: upper - 1) {
        
        let pa = lowerMoji.page
        let pb = upperMoji.page
        
        pages = min(pa,pb)...max(pa,pb)
      }
    }
    
    return (lower ... (upper - 1), pages)
  }
  
  func string(with range: ClosedRange<Int>, possiblePage page: Int16? = nil) -> (string: String, pages: ClosedRange<Int16>) {
    var string = ""
    
    var pageObj: Page? = nil
    var pages: ClosedRange<Int16>! = nil
    
    if page != nil {
      pageObj = RealmRepository.shared.page(at: page!)
      pages = page! ... page!
    }
    
    func updatePageRange(_ moji: Moji) {
      if pages == nil {
        pages = moji.page...moji.page
      }
      
      if pages!.lowerBound > moji.page {
        pages = moji.page...pages!.upperBound
      }
      
      if pages!.upperBound < moji.page {
        pages = pages!.lowerBound...moji.page
      }
    }
    
    for n in [Int](range.lowerBound ... range.upperBound) {
      if let moji = pageObj?.mojis.filter("validCharacterNumber == %@", n).first {
        string += moji.singleCharacter
        updatePageRange(moji)

      }else {
        if let moji = RealmRepository.shared.moji(at: n) {
          string += moji.singleCharacter
          
          // FETCH PAGE OBJ
          if pageObj == nil {
            pageObj = RealmRepository.shared.page(at: moji.page)
          }

         updatePageRange(moji)
        }
      }
    }
    
    return (string, pages!)
  }
  
  func mojis(with range: ClosedRange<Int>, page: Int16? = nil) -> [Moji] {
    
    var pageObj: Page? = nil
    var pages: ClosedRange<Int16>!
    if page != nil {
      pageObj = RealmRepository.shared.page(at: page!)
      pages = page! ... page!
    }
    var mojis: [Moji] = []
    for n in [Int](range.lowerBound ... range.upperBound) {
      if let moji = pageObj?.mojis.filter("validCharacterNumber == %@", n).first {
        mojis.append(moji)
        
        if pages == nil {
          pages = moji.page...moji.page
        }
      }else {
        if let moji = RealmRepository.shared.moji(at: n) {
          mojis.append(moji)
          
          if pages == nil {
            pages = moji.page...moji.page
          }
          
          if pages!.lowerBound > moji.page {
            pages = moji.page...pages!.upperBound
          }
          
          if pages!.upperBound < moji.page {
            pages = pages!.lowerBound...moji.page
          }
        }
      }
    }
    
    return mojis
  }
  
  class func overlapped(with probeRange: CountableRange<Int>, in array: [Range<Int>], _ range: Range<Int>) -> Range<Int>? {
    
    if range.isEmpty { return nil }
    let half = (range.lowerBound + range.upperBound ) / 2

    
    if array[half].lowerBound >= probeRange.upperBound && range.lowerBound != half {
     return overlapped(with: probeRange, in: array, range.lowerBound ..< half)
    }
    
    if array[half].upperBound <= probeRange.lowerBound && range.lowerBound != half {
      return overlapped(with: probeRange, in: array, half ..< range.upperBound)
    }
    
    var lowerBound = half
    var upperBound = half + 1
    
    for n in (range.lowerBound ..< half).reversed() {
      if array[n].overlaps(probeRange) {
        lowerBound = n
      }else {
        //break
      }
    }
    
    for n in (half ..< range.upperBound){
      if array[n].overlaps(probeRange) {
        upperBound = n + 1
      }else {
        //break
      }
    }
    
    return lowerBound ..< upperBound
  }
  
  private func mojisInPage(_ page: Int16) -> List<Moji> {
    return RealmRepository.shared.page(at: page)!.mojis
  }
  
  func pageTitle(atPage page: Int16? = nil, atIndex validCharacterNumber: Int? = nil) -> Article? {
    if let article = RealmRepository.shared.pageTitle(atPage: page, atIndex: validCharacterNumber) {
      
      return article
    }
    return nil
  }
  
  
  
  //MARK: - CLOUD KIT

  func saveRating(_ source: RatingsSource) {
    CloudKitRepository.shared.saveRating(source)
  }
  
  func getBl(_ handler:@escaping ((_ blackListed: Bool)->Void)) {
    CloudKitRepository.shared.getBl(handler)
  }

  //MARK: - HIGHLIGHTS
  
  func highlight(at index: Int) -> Highlight {
    let highlight = RealmRepository.shared.highlights[index]
    return highlight
  }
  
  var numberOfHighlights: Int {
    return RealmRepository.shared.highlights.count
  }
  
  func locationOfHighlight(at index: Int) -> Int {
    return RealmRepository.shared.highlights[index].startIndex
  }
  
  func findHighlightMojiBounds(inPage page: Int16) -> [CGRect]? {
    
    guard let foundLocations = foundLocations else { return nil }
    guard let pageObj = RealmRepository.shared.page(at: page) else { return nil } // UNKNOWN ERROR
    
    let thisPagesCharRange = pageObj.startValidCharacterNumber ..< pageObj.endValidCharacterNumber
    
    let highlightedValidCharacterNumbers = foundLocations.results
    
    
    if let theRange = DataSource.overlapped(with: thisPagesCharRange, in: highlightedValidCharacterNumbers, 0 ..< highlightedValidCharacterNumbers.count) {
      
      let highlightsInThisPage2 = Array(highlightedValidCharacterNumbers[theRange])

      
      var validCharacterNumbers: [Int] = []
      highlightsInThisPage2.forEach { validCharacterNumbers += [Int]($0.lowerBound ..< $0.upperBound) }
      
      let highlightedMojis = pageObj.mojis.filter("validCharacterNumber IN %@", validCharacterNumbers)
      
      return Array(highlightedMojis).map {$0.bounds}
    }
    
    return nil
  }

  func highlightDictionaryForMigration(_ highlight: Highlight) -> [String: Any] {
    let article = articleAt(at: highlight.startIndex)
    
    let lowerLimit = (article?.validCharacterNumber ?? 0 )
    let from = highlight.startIndex
    //let to = highlight.endIndex
    let range: ClosedRange<Int> = lowerLimit <= from-1 ? lowerLimit...from-1 : lowerLimit...lowerLimit
    
    let startNResults = RealmRepository.shared.mojis(in: range, beginsWith: highlight.startString)
    let startN = startNResults.count
    var n = 0
    if from > 0 && highlight.highlightedText != nil {
      let upperLimit = from - 1
      
      if upperLimit >= lowerLimit {
        let nResults = find(highlight.highlightedText! as NSString, in: [], after: lowerLimit, before: upperLimit)
        n = nResults.count
      }
    }
    
    var dict: [String: Any] = [:]
    dict["startN"] = startN
    dict["n"] = n
    dict["highlightedText"] = highlight.highlightedText

    dict["lawRaw"] = article?.law.rawValue
    dict["articleEncodedTitle"] = article?.encodedTitle

    return dict
  }
  
  func setNotesForEditingHighlight(_ token: Any, notes: String) {
    guard let highlight = token as? Highlight else { return }
    
    RealmRepository.shared.updateNotesFor(highlight, notes: notes)
    ICloudRepository.shared.saveToICloud()
  }
  
  func colorForEditingHighlight(_ token: Any?) -> HighlightColor? {
    guard let highlight = token as? Highlight else { return nil }
    return highlight.color
  }
  
  func notesForEditingHighlight(_ token: Any) -> String? {
    guard let highlight = token as? Highlight else { return nil }
    return highlight.notes ?? ""
  }
  
  func deleteEditingHighlight(_ token: Any) {
    guard let highlight = token as? Highlight else { return }
    
    RealmRepository.shared.deleteHighlight(highlight)
    ICloudRepository.shared.saveToICloud()
  }
  
  func deleteNotesForEditingHighlight(_ token: Any) {
    guard let highlight = token as? Highlight else { return }
    
    RealmRepository.shared.deleteNotesFor(highlight)
    ICloudRepository.shared.saveToICloud()
  }
  
  func addHighlight(_ color: HighlightColor? = nil, range: Selection, uuid: String? = nil, notes: String? = nil) {
    let from = range.selectedRange.lowerBound
    let to = range.selectedRange.upperBound
    var color = color
    guard let startMoji = RealmRepository.shared.moji(at: from) else { return }
    guard let endMoji = RealmRepository.shared.moji(at: to) else { return }
    
    let highlight = Highlight()
    highlight.uuid = uuid ?? shortUUIDString()
    
    if color == nil {
      let colorRaw = UserDefaults.standard.integer(forKey: "DataSourceDefaultHighlightColor")
      color = HighlightColor(rawValue: colorRaw)
    }
    highlight.color = color!
    highlight.notes = notes
    let val = string(with: range.selectedRange, possiblePage: range.pageRange.lowerBound)
    highlight.highlightedText = val.string
    highlight.startIndex = from
    highlight.endIndex = to
    highlight.startPage = range.pageRange.lowerBound
    highlight.endPage = range.pageRange.upperBound
    
    let article = pageTitle(atIndex: from)
    
    if article != nil {
      highlight.law = article!.law
      highlight.articleEncodedTitle = article!.encodedTitle
    }else {
      highlight.law = .patent
      highlight.articleEncodedTitle = ""
    }
    
    highlight.startString = startMoji.character
    highlight.endString = endMoji.character
    
    let lowerLimit = (article?.validCharacterNumber ?? 0 )
    
    var range: ClosedRange<Int> = lowerLimit <= from-1 ? lowerLimit...from-1 : lowerLimit...lowerLimit
    
    let startNResults = RealmRepository.shared.mojis(in: range, beginsWith: startMoji.character)
    highlight.startN = Int16(startNResults.count)
    
    range = lowerLimit <= to-1 ? lowerLimit...to-1 : lowerLimit...lowerLimit
    let endNResults = RealmRepository.shared.mojis(in: range, beginsWith: endMoji.character)
    highlight.endN = Int16(endNResults.count)
    
    if from > 0 {
      let upperLimit = from - 1
      
      if upperLimit >= lowerLimit {
        let nResults = find(val.string as NSString, in: [], after: lowerLimit, before: upperLimit)
        highlight.n = Int16(nResults.count)
      }else {
        highlight.n = 0
      }
    }
    
    RealmRepository.shared.addHighlight(highlight)
    ICloudRepository.shared.saveToICloud()
    
    CloudKitRepository.shared.report(from...to)
  }
  
  func updateHighlight(_ token: Any, _ color: HighlightColor, notes: String?) {
    guard let highlight = token as? Highlight else { return }
    
    if notes != nil {
      RealmRepository.shared.updateNotesFor(highlight, notes: notes!)
    }
    RealmRepository.shared.updateColorFor(highlight, color: color)
    
    UserDefaults.standard.set(color.rawValue, forKey:"DataSourceDefaultHighlightColor")
    ICloudRepository.shared.saveToICloud()
  }

  
  //MARK:- FIND
  
  // FIND
  typealias FindResult = (results: [Range<Int>], findString: NSString, currentIndex: Int?)
  var foundLocations: FindResult? = nil
  
  func highlightForString(_ string: NSString?, in lawMask: [Law]) {
    
    if string == nil {
      foundLocations = nil
      return
    }
    let t0 = Date.timeIntervalSinceReferenceDate
    foundLocations = (find(string!, in: lawMask), string!, nil)
    let t1 = Date.timeIntervalSinceReferenceDate
    
    print("found count \(foundLocations!.results.count) (\(t1-t0)sec)")
  }
  
  func findNext() -> (bounds: CGRect, page: Int, foundLocations: FindResult?)? {
    if foundLocations == nil || foundLocations!.results.count == 0 {
      //let userInfo = ["total": 0] as [String : Any]
      return nil
    }
    
    var currentIndex = foundLocations!.currentIndex
    if currentIndex == nil {
      currentIndex = 0
    }else {
      if foundLocations!.results.count == currentIndex! + 1 { return nil }
      currentIndex! += 1
    }
    
    let thisResult = foundLocations!.results[currentIndex!]
    guard let theMoji = RealmRepository.shared.moji(at: thisResult.lowerBound) else { return nil }

    foundLocations!.currentIndex = currentIndex
    return  (theMoji.bounds, Int(theMoji.page), foundLocations)
  }
  
  func findPrevious() -> (bounds: CGRect, page: Int, foundLocations: FindResult?)? {
    if foundLocations == nil || foundLocations!.results.count == 0 { return nil }
    
    var currentIndex = foundLocations!.currentIndex
    if currentIndex == nil {
      currentIndex = foundLocations!.results.count - 1
    }else {
      if 0 == currentIndex!  { return nil }
      currentIndex! -= 1
    }
    
    let thisResult = foundLocations!.results[currentIndex!]
    guard let theMoji = RealmRepository.shared.moji(at: thisResult.lowerBound) else { return nil }
    foundLocations!.currentIndex = currentIndex
    return (theMoji.bounds, Int(theMoji.page), foundLocations)
  }
  
  private func findInSlices(_ slices: [NSString], in lawMask: [Law], after lowerValidCharacterNumber: Int = 0, before upperValidCharacterNumber: Int = Int.max) -> [RealmSwift.Results<Moji>]? {
    
    var resultsArray: [RealmSwift.Results<Moji>] = []
    for slice in slices {
      
      var results = RealmRepository.shared.mojis(in: lowerValidCharacterNumber...upperValidCharacterNumber, beginsWith: slice as String)

      if lawMask.count == 0 || lawMask.count == 4 {
        resultsArray.append(results)
      }else {
        
        results = results.filter( "lawRaw IN %@", lawMask.map { $0.rawValue }).sorted(byKeyPath: "validCharacterNumber")
        resultsArray.append(results)
      }
    }
    
    return resultsArray
  }
  
  func find(_ string: NSString, in lawMask: [Law], after lowerValidCharacterNumber: Int = 0, before upperValidCharacterNumber: Int = Int.max) -> [Range<Int>] {
    
    var calcCount = 0
    func binarySearch(_ realmResultsArray: RealmSwift.Results<Moji>, cachArray: inout [Int?], _ validCharacterNumber: Int) -> Int? {
      var lowerIndex = 0
      var upperIndex = cachArray.count - 1
      
      while true {
        let currentIndex = (lowerIndex + upperIndex) / 2
        var thisNumber = cachArray[currentIndex]
        
        if thisNumber == nil {
          thisNumber = realmResultsArray[currentIndex].validCharacterNumber
          calcCount += 1

          cachArray[currentIndex] = thisNumber
        }
        
        if thisNumber == validCharacterNumber {
          return currentIndex
        } else if lowerIndex > upperIndex {
          return nil
        } else {
          if thisNumber! > validCharacterNumber {
            upperIndex = currentIndex - 1
          } else {
            lowerIndex = currentIndex + 1
          }
        }
      }
    }
  
    func twoCharacterSlices(_ string: NSString) -> [NSString] {
      if string.length < 2 { return [string] }
      
      var slices: [NSString] = []
      var idx = 0
      var length = 2
      
      while true {
        let range = NSMakeRange(idx, length)
        let substring = string.substring(with: range) as NSString
        slices.append(substring)
        if idx + length == string.length { break }
        idx += 2
        if idx + 1 == string.length { length = 1 }
      }
      return slices
    }
    
    var slices = twoCharacterSlices(string)
    var compareStrings = false
    if slices.count > 4 {
      slices = Array(slices[0...4])
      compareStrings = true
    }
    
    guard var sliceResults = findInSlices( slices, in: lawMask, after: lowerValidCharacterNumber, before: upperValidCharacterNumber) else { return [] }
   
    var results: [Range<Int>] = []
    var countArray: [(index: Int, results: RealmSwift.Results<Moji>, count: Int)] = []
    
    for i in 0 ..< sliceResults.count {
      let count = sliceResults[i].count // ACTUAL FETCH OCCURS
      if count == 0 { return [] } // NO MATCH
      countArray.append( (index: i, results: sliceResults[i], count: count) )
    }

    
    countArray.sort { $0.count < $1.count }
    let minI = countArray[0].index

    
    if sliceResults.count > 1 {
      var intResults: [Array<Int?>] = []
      
      for i in 0 ..< sliceResults.count {
        let placeholder = Array<Int?>(repeating: nil, count: countArray[i].count)
        intResults.append(placeholder)
      }
      
      for moji in countArray[0].results {
        var found = true
        sliceLoop: for i in 1 ..< countArray.count {
          
          if nil == binarySearch(countArray[i].results, cachArray: &intResults[i], moji.validCharacterNumber + (countArray[i].index - minI) * 2) {
            found = false
            break sliceLoop
          }
        }
        
        // FOUND
        if found == true {
          results.append(moji.validCharacterNumber - minI * 2 ..< moji.validCharacterNumber - minI * 2 + string.length)
        }
      }
    }else {
      results = sliceResults[0].map { $0.validCharacterNumber - minI * 2 ..< $0.validCharacterNumber - minI * 2 + string.length }
    }
    
    //print("coparison \(calcCount)") //127157 -> 9936
    
    if compareStrings {
      var comparedResults: [Range<Int>] = []
      for range in results {
        
        let fullRange: Range<Int> = range.lowerBound ..< range.lowerBound + string.length
        let (thisString, _) = self.string(with: ClosedRange<Int>(fullRange))
        if string as String == thisString {
          comparedResults.append(fullRange)
        }
      }
      results = comparedResults
    }
    
    return results
  }
  
  private func binarySearch<T: Comparable>(inputArr: Array<T>, searchItem: T) -> Int {
    var lowerIndex = 0
    var upperIndex = inputArr.count - 1
    
    while (true) {
      let currentIndex = (lowerIndex + upperIndex) / 2
      if inputArr[currentIndex] == searchItem {
        return currentIndex
      } else if lowerIndex > upperIndex {
        return -1
      } else {
        if inputArr[currentIndex] > searchItem {
          upperIndex = currentIndex - 1
        } else {
          lowerIndex = currentIndex + 1
        }
      }
    }
  }
  
  private func findString(string: NSString, after precedent: Int, in realmResults: RealmSwift.Results<Moji>) -> [Moji]? {
    
    let letter = string.substring(with: NSMakeRange(0, 1))
    let validCharacterNumber = precedent + 1
    
    if realmResults.count <= validCharacterNumber { return nil }
    
    let nextMoji = realmResults[validCharacterNumber]
    if nextMoji.singleCharacter.compare(letter, options: String.CompareOptions.caseInsensitive) != .orderedSame { return nil }

    let mojis = realmResults[validCharacterNumber ..< validCharacterNumber + string.length]
    
    let trailingString = (Array(mojis).map { $0.singleCharacter }).joined()
    
    if trailingString != string as String {
      return nil
    }

    return Array(mojis)
  }
  
  func mojiBounds(at index: Int) -> (bounds: CGRect, page: Int16)? {
    if let moji = RealmRepository.shared.moji(at: index) {
      return ( moji.bounds, Int16(moji.page))
    }
    
    return nil
  }
  
  func jumpTo(_ string: String, in law: Law, encoded: Bool = false) -> (bounds: CGRect, page: Int16)? {
    
    func encodeForTOC2(_ string: NSString) -> String {
      
      let mstrg = NSMutableString(string: string)
      mstrg.replaceOccurrences(of: "の", with: "-", options: .literal, range: NSMakeRange(0, mstrg.length))
      //条+num -> "."
      //項 -> "_"
      //号 -> ""
      
      // 自作の場合は、第4条　優先権第D.項第 (1)号 🔱1@8@4
      
      mstrg.replaceOccurrences(of: "[0-9]+(?=号)", with: "_$0", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
      mstrg.replaceOccurrences(of: "[条第]+(?=[0-9])", with: ".", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
      mstrg.replaceOccurrences(of: "項(?=[0-9])", with: "_", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
      mstrg.replaceOccurrences(of: "[第条項号]", with: "", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
      mstrg.replaceOccurrences(of: "^[0-9]*", with: "($0)", options: .regularExpression, range: NSMakeRange(0, mstrg.length))
      
      debugPrint("encodeForTOC2 \(mstrg)")
      return mstrg as String
    }
    
    let encodedTitle = encoded ? string : encodeForTOC2(string as NSString)
    if let article = RealmRepository.shared.article(encodedTitle: encodedTitle , law: law) {
    // TODO: Select Law

      if let moji = RealmRepository.shared.moji(at: article.validCharacterNumber) {
        return ( moji.bounds, Int16(moji.page))
      }
    }
    
    return nil
  }
  
  func clearSearch() {
    foundLocations = nil
  }
  
  
  func articleAt(at validCharacterNumber: Int) -> Article? {
    
    let allArticles = RealmRepository.shared.articles
    
    var lowerArticle: Article? = nil
    var upperArticle: Article? = nil
    
    for n in 0 ..< allArticles.count {
      let a1 = allArticles[n]
      if a1.validCharacterNumber <= validCharacterNumber {
        lowerArticle = a1
      }
      
      if a1.validCharacterNumber > validCharacterNumber {
        upperArticle = a1
        break
      }
    }
    
    return lowerArticle
  }
}
