//
//  HighlightDrawer.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation
import CoreText

class HighlightDrawer {
  
  static var shared = HighlightDrawer()
  
  // DRAW HIGHLIGHTS
  @objc class func drawFindHighlightForPage(_ page: NSNumber) {
    
    let scheme = DocumentViewModel.currentDocumentViewModel?.backgroundColorScheme
    let inverted = ( scheme?.inverted == true )

    if let mojiBounds = DataSource.shared.findHighlightMojiBounds(inPage: Int16(page)) {
      HighlightDrawer.shared.drawHighlightInContext(mojiBounds, hasShadow: true, inverted: inverted)
    }
  }
  
  // DRAW SELECTION
  @objc class func drawSelectionForPage(_ page: NSNumber) {
    guard let _ = DocumentViewModel.currentDocumentViewModel?.selection?.selectedRange else { return }
    if DocumentViewModel.currentDocumentViewModel?.selectedMojis == nil { return }
    
    let scheme = DocumentViewModel.currentDocumentViewModel?.backgroundColorScheme
    let inverted = ( scheme?.inverted == true )

    let selectedMojisInPage = DocumentViewModel.currentDocumentViewModel?.selectedMojis!.filter { $0.page == page.int16Value }.map { $0.bounds }
    
    if selectedMojisInPage != nil {
      HighlightDrawer.shared.drawHighlightInContext(selectedMojisInPage!, color: .selection, hasShadow: false, isSelection: true, inverted: inverted)
    }
    
  }
  // DRAW USER'S HIGHLIGHT
  @objc class func drawUsersHighlightsForPage(_ page: NSNumber) {
    
    let scheme = DocumentViewModel.currentDocumentViewModel?.backgroundColorScheme
    let inverted = ( scheme?.inverted == true )
    let disableDiff = UserDefaults.standard.bool(forKey: "DisableDiff")
    
    if disableDiff != true {
      DocumentViewModel.currentDocumentViewModel?.enumerateUserDiffMojiBounds(inPage: Int16(page)) { (bounds, color, note) in
        
        HighlightDrawer.shared.drawHighlightInContext(bounds, color: color, hasShadow: false, icon: nil, inverted: inverted, sideNote: note)
      }
    }
    
    DocumentViewModel.currentDocumentViewModel?.enumerateUserHighlightMojiBounds(inPage: Int16(page)) { (bounds, color, notes) in
      var icon: String? = nil
      if notes?.isEmpty == false {
        icon = "💬"
      }
      HighlightDrawer.shared.drawHighlightInContext(bounds, color: color, hasShadow: false, icon: icon, inverted: inverted)
    }
  }
  /*
  private func debugDrawInContext(_ mojis:RealmSwift.Results<Moji>) {
    
    UIColor.yellow.setFill()
    
    var currentRect: CGRect? = nil
    for moji in mojis {
      let rect = moji.bounds
      
      if currentRect == nil { currentRect = rect; continue }
      
      if abs(currentRect!.maxX - rect.origin.x) < 1 && currentRect!.origin.y == rect.origin.y {
        currentRect = currentRect!.union(rect)
        continue
      }else {
        UIRectFill(currentRect!)
        currentRect = rect
      }
      
      
      let str = moji.singleCharacter as NSString
      str.draw(in: rect, withAttributes: nil)
      
    }
    if currentRect != nil {
      UIRectFill(currentRect!)
    }
  }
  */

  
  func drawHighlightInContext(_ mojis:[CGRect], color: HighlightColor? = nil, hasShadow: Bool = false, isSelection: Bool = false, icon: String? = nil, inverted: Bool = false, sideNote: String? = nil) {
    let ctx = UIGraphicsGetCurrentContext()
    var icon = icon
    var alpha: CGFloat = 1.0
    if color != nil {
      ctx?.setFillColor(color!.color().cgColor)
      //color!.setFill()
    }else {
      if inverted {
        UIColor.yellow.withAlphaComponent(0.8).setFill()
      }else {
        UIColor.yellow.setFill()
      }
    }

    
    if hasShadow {
      ctx?.setShadow(offset: CGSize(width:0, height:1), blur: 5.0, color: UIColor(white: 0, alpha: 0.5).cgColor)
      alpha = 1.0
    }else {
      ctx?.setShadow(offset: CGSize.zero, blur: 0.0)
      alpha = 0.6
    }
    
    if inverted {
      ctx?.setBlendMode(.screen)

      if isSelection {
        alpha = 0.5
      }else {
        alpha = 0.7
      }
    }else {
      ctx?.setBlendMode(.sourceAtop)
      
      if isSelection {
        alpha = 0.4
      }
    }
    
    func modifyRect(_ rect: CGRect) -> CGRect {
      var rect = rect
      //rect.origin.x += 0.5
        rect.origin.y -= 1
        rect.size.height += 2
      return rect
    }
    let diffFillAlpha: CGFloat = 0.15
    let diffWidth: CGFloat = 0.5
    
    var currentRect: CGRect? = nil
    for moji in mojis {
      var rect = moji
      if isSelection {
        rect.origin.x -= 1;
        rect.size.width += 2;
      }
      
      if currentRect == nil { currentRect = rect; continue }
      
      if abs(currentRect!.minY - rect.maxY) < 3 && abs(currentRect!.origin.x - rect.origin.x) < 2 {
        currentRect = currentRect!.union(rect)
        continue
      }else {
        
        if color == .underline {
          var underlineRect = currentRect!
          underlineRect.origin.x = underlineRect.maxX + 1
          underlineRect.size.width = 1
          ctx?.setAlpha(1.0)
          ctx?.fill(underlineRect)
        }else  if color == .added {
          
          var fillRect = currentRect!
          #if EDITION19
            fillRect.origin.y = fillRect.maxY
            fillRect.size.height = 2
          #endif
          fillRect.origin.x -= 2
          fillRect.origin.y -= 1
          fillRect.size.width += 3
          fillRect.size.height += 2
          ctx?.setAlpha(diffFillAlpha)
          ctx?.fill(fillRect)
          
          var underlineRect = modifyRect(currentRect!)
          #if EDITION19
            underlineRect.origin.x -= 2
            underlineRect.origin.y = underlineRect.maxY
            underlineRect.size.height = 1
            underlineRect.size.width += 4
          #else
          underlineRect.origin.x = underlineRect.maxX + 1
          underlineRect.size.width = diffWidth
            #endif
          ctx?.setAlpha(1.0)
          ctx?.fill(underlineRect)
        }else if color == .changed {
          var fillRect = currentRect!
          fillRect.origin.x -= 2
          fillRect.origin.y -= 1
          fillRect.size.width += 3
          fillRect.size.height += 2
          ctx?.setAlpha(diffFillAlpha)
          ctx?.fill(fillRect)
          
          var underlineRect = modifyRect(currentRect!)
          underlineRect.origin.x = underlineRect.maxX + 1
          underlineRect.size.width = diffWidth
          ctx?.setAlpha(1.0)
          ctx?.fill(underlineRect)
          
        }else if color == .deleted {
          
          var fillRect = currentRect!
          #if EDITION20
            fillRect.origin.y = fillRect.maxY - 1
            fillRect.size.height = 2
          #endif
          fillRect.origin.x -= 2
          fillRect.origin.y -= 1
          fillRect.size.width += 3
          fillRect.size.height += 3
          ctx?.setAlpha(diffFillAlpha)
          ctx?.fill(fillRect)
          
          var underlineRect = currentRect!
          #if EDITION20
            underlineRect.origin.x -= 2
            underlineRect.origin.y = underlineRect.maxY
            underlineRect.size.height = 1
            underlineRect.size.width += 4
          #else
            underlineRect.origin.x = underlineRect.maxX + 1
            underlineRect.size.width = diffWidth
            
          #endif
          ctx?.setAlpha(1.0)
          ctx?.fill(underlineRect)
        }else {
          ctx?.setAlpha(alpha)
          ctx?.fill(currentRect!)
        }
        
        currentRect = rect
      }
      
      /*
       let str = moji.singleCharacter as NSString
       str.draw(in: rect, withAttributes: nil)
       */
    }
    if currentRect != nil {
      var sidenoteY = currentRect!.maxY

      if color == .underline {
        var underlineRect = currentRect!
        underlineRect.origin.x = underlineRect.maxX + 1
        underlineRect.size.width = 1
        ctx?.setAlpha(1.0)
        ctx?.fill(underlineRect)
        
      }else  if color == .added {
        var fillRect = currentRect!
        #if EDITION19
          fillRect.origin.y = fillRect.maxY - 1
          fillRect.size.height = 2
        #endif
        fillRect.origin.x -= 2
        fillRect.origin.y -= 1
        fillRect.size.width += 3
        fillRect.size.height += 3
        ctx?.setAlpha(diffFillAlpha)
        ctx?.fill(fillRect)
        
        var underlineRect = currentRect!
        #if EDITION19
          underlineRect.origin.x -= 2
          underlineRect.origin.y = underlineRect.maxY
          underlineRect.size.height = 1
          underlineRect.size.width += 3
          
          sidenoteY += 3
        #else
        underlineRect.origin.x = underlineRect.maxX + 1
        underlineRect.size.width = diffWidth
        #endif
        ctx?.setAlpha(1.0)
        ctx?.fill(underlineRect)
      }else if color == .changed {
        var fillRect = currentRect!
        fillRect.origin.x -= 2
        fillRect.origin.y -= 1
        fillRect.size.width += 3
        fillRect.size.height += 2
        ctx?.setAlpha(diffFillAlpha)
        ctx?.fill(fillRect)
        
        var underlineRect = modifyRect(currentRect!)
        underlineRect.origin.x = underlineRect.maxX + 1
        underlineRect.size.width = diffWidth
        ctx?.setAlpha(1.0)
        ctx?.fill(underlineRect)
        
      }else if color == .deleted {
        
        var fillRect = currentRect!
        #if EDITION20
          fillRect.origin.y = fillRect.maxY - 1
          fillRect.size.height = 3
        #endif
        fillRect.origin.x -= 2
        fillRect.origin.y -= 1
        fillRect.size.width += 3
        fillRect.size.height += 2
        ctx?.setAlpha(diffFillAlpha)
        ctx?.fill(fillRect)
        
        var underlineRect = currentRect!
        #if EDITION20
          underlineRect.origin.x -= 2
          underlineRect.origin.y = underlineRect.maxY
          underlineRect.size.height = 1
          underlineRect.size.width += 3
          
          sidenoteY += 3
        #else
          underlineRect.origin.x = underlineRect.maxX + 1
          underlineRect.size.width = diffWidth
        #endif
        ctx?.setAlpha(1.0)
        ctx?.fill(underlineRect)
      }
      else {
        ctx?.setAlpha(alpha)
        ctx?.fill(currentRect!)
      }
      
      if icon != nil {
        ctx?.setAlpha(1.0)
        let image = UIImage(named: "Bubble")!
        ctx?.draw(image.cgImage!, in: CGRect(x: currentRect!.maxX - 1, y: currentRect!.origin.y, width: 8, height: 8))
        
        icon = nil
      }
      
      
      if sideNote != nil {
        var sideNote = sideNote!
        sideNote = sideNote.replacingOccurrences(of: "(", with: "（")
        sideNote = sideNote.replacingOccurrences(of: ")", with: "）")
        let color: UIColor = color!.color()
        
        let attributes: [NSAttributedStringKey : Any] = [.font: UIFont.systemFont(ofSize: 6), .foregroundColor: color, .verticalGlyphForm: 1]
        let attributedString = NSAttributedString(string: sideNote, attributes: attributes)
        
        let line = CTLineCreateWithAttributedString(attributedString as CFAttributedString)
        
        var  descent: CGFloat = 0
        var  ascent: CGFloat = 0
        var  leading: CGFloat = 0
        
        _ = CTLineGetTypographicBounds(line, &ascent, &descent, &leading);

        ctx!.saveGState()
        
        var transform = CGAffineTransform(scaleX: 1, y: 1);
        transform = transform.translatedBy(x: 0, y: 0);
        ctx!.textMatrix = transform;
        
        var t: CGAffineTransform
        
        // Drawing left part
        let MARGIN: CGFloat = 0
        t = CGAffineTransform( translationX: currentRect!.maxX + descent - 0.5, y: sidenoteY + MARGIN );
        
        ctx!.concatenate(t);
        //ctx?.setShadow(offset: CGSize(width:0, height:0.5), blur: 4.0, color: UIColor(white: 0, alpha: 0.3).cgColor)

        let runs = CTLineGetGlyphRuns(line) as NSArray
        for run in runs {
          CTRunDraw(run as! CTRun, ctx!, CFRangeMake(0, 0));
        }
        
        ctx!.restoreGState();

        
      }
    }
    ctx?.setAlpha(1.0)
  }
}
