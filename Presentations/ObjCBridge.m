//
//  ObjCBridge.m
//  Reader
//
//  Created by Masatoshi Nishikata on 7/11/16.
//
//

#import "ObjCBridge.h"
static ObjCBridge* sharedObjCBridge = nil;
CGFloat gDocumentScaling = 1.0;

@implementation ObjCBridge

+(instancetype)shared {
  
  if( sharedObjCBridge == nil )
  { sharedObjCBridge = [[ObjCBridge alloc] init]; }
  
  return sharedObjCBridge;
}

+(NSString * _Nonnull)versionString
{
  CFBundleRef bundle = CFBundleGetMainBundle();
  CFStringRef versionString = CFBundleGetValueForInfoDictionaryKey(bundle, CFSTR("CFBundleShortVersionString"));
  CFStringRef buildVersionString = CFBundleGetValueForInfoDictionaryKey(bundle, CFSTR("CFBundleVersion"));
  
  NSString* string = [NSString stringWithFormat:@"Version %@ (%@)",  (__bridge NSString*)versionString,  (__bridge NSString*)buildVersionString];
  
  return string;
}

- (instancetype)init
{
  self = [super init];
  if (self) {

  }
  return self;
}

-(void)drawInContext:(CGContextRef)ctx page: (NSInteger)page
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
  
  if( [NSThread isMainThread] ) {
    Class TheClass = NSClassFromString(@"Reader.HighlightDrawer");
    UIGraphicsPushContext(ctx);
    [TheClass performSelector:@selector(drawUsersHighlightsForPage:) withObject:@(page)];
    [TheClass performSelector:@selector(drawFindHighlightForPage:) withObject:@(page)];
    [TheClass performSelector:@selector(drawSelectionForPage:) withObject:@(page)];

    UIGraphicsPopContext();
    
  }else {
    dispatch_sync(dispatch_get_main_queue(), ^{
      
      Class TheClass = NSClassFromString(@"Reader.HighlightDrawer");
      UIGraphicsPushContext(ctx);
      [TheClass performSelector:@selector(drawUsersHighlightsForPage:) withObject:@(page)];
      [TheClass performSelector:@selector(drawFindHighlightForPage:) withObject:@(page)];
      [TheClass performSelector:@selector(drawSelectionForPage:) withObject:@(page)];

      UIGraphicsPopContext();
      
    });
    
  }
#pragma clang diagnostic pop
}

@end
