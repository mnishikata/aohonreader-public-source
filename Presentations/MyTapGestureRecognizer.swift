//
//  MyTapGestureRecognizer.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 5/12/16.
//
//

import UIKit

final class MyTapGestureRecognizer: UIGestureRecognizer {
  
  var lastTouchedPoint: CGPoint? = nil
  
  var touchedPoint: CGPoint? = nil {
    didSet {
      // if oldValue == touchedPoint || oldValue == nil { return }
      (delegate as? ReaderViewControllerSwift)?.tap(touchedPoint)
      
    }
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
    if touches.count > 1 { return }
    touchedPoint = touches.first?.location(in: self.view)
    state = .possible
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
    let currentPoint = touches.first?.location(in: self.view)
    
    if currentPoint == nil || shouldCancel(currentPoint!) == true {
      self.state = .cancelled
      touchedPoint = nil
    }
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
    self.state = .ended
    
    //doubleTap(
    if touches.first?.tapCount == 2 && touchedPoint != nil && lastTouchedPoint != nil {
      
      if abs(touchedPoint!.x - lastTouchedPoint!.x) < 10 && abs(touchedPoint!.y - lastTouchedPoint!.y) < 10 {
        (delegate as? ReaderViewControllerSwift)?.doubleTap(touchedPoint!)
      }
    }else {
      
      if touchedPoint != nil {
        (delegate as? ReaderViewControllerSwift)?.tap(touchedPoint, tapped: true)
      }
      lastTouchedPoint = touchedPoint
      touchedPoint = nil
    }
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
    self.state = .cancelled
    touchedPoint = nil
    
  }
  
  func shouldCancel(_ point: CGPoint) -> Bool {
    if touchedPoint == nil { return true }
    if abs(touchedPoint!.x - point.x) > 10 { return true }
    if abs(touchedPoint!.y - point.y) > 10 { return true }
    
    return false
  }
  
}
