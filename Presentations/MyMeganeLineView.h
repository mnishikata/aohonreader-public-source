//
//  MeganeLineView.h
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 27/12/10.
//  Copyright 2010 Catalystwo Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MyMeganeLineView : UIView {

	UIInterfaceOrientation orientation_;

	CGFloat height_;
	CGPoint focusCenter_;
	CGPoint drawCenter_;

	UIWindow *window_;
	
}
+(MyMeganeLineView*)sharedMeganeLineView;
-(void)dealloc;
-(void)setFocusCenter:(CGPoint)point;
-(void)hide;
-(void)setInterfaceOrientation:(UIInterfaceOrientation)orientation;
-(void)setHeight:(CGFloat)val;
-(void)setHidden:(BOOL)flag;
-(void)drawHorizontalGradientInRect:(CGRect)rect fromColor:(UIColor*)fromColor  toColor:(UIColor*)toColor inContext:(CGContextRef)ctx;
-(void)addRoundedRectToPath:(CGContextRef) context rect:(CGRect) rect ovalWidth:(CGFloat) ovalWidth  ovalHeight:(CGFloat)ovalHeight;
-(void)addFukidashiRectToPath:(CGContextRef) context rect:(CGRect) rect ovalWidth:(CGFloat) ovalWidth  ovalHeight:(CGFloat)ovalHeight;
-(void)drawRect:(CGRect)rect;


@property (nonatomic) 	UIInterfaceOrientation interfaceOrientation;
@property (nonatomic) CGFloat height;
@property (nonatomic) CGFloat horizontalFocalOffset;
@property (nonatomic) CGPoint focusCenter;

@property (nonatomic, retain) 	UIWindow *window;

@end
