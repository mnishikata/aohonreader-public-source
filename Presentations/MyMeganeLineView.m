//
//  MeganeLineView.m
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 27/12/10.
//  Copyright 2010 Catalystwo Ltd. All rights reserved.
//

#import "MyMeganeLineView.h"
#import <QuartzCore/QuartzCore.h>
#import "DrawUtils.h"
#import "Tui.h"

static MyMeganeLineView* sharedMeganeLineView = nil;

@implementation MyMeganeLineView
@synthesize focusCenter = focusCenter_;
@synthesize window = window_;
@synthesize height = height_;
@synthesize interfaceOrientation = orientation_;

#define SIZE 150
#define ABOVE -20
#define LINE_HEIGHT 75

#define NSFoundationVersionNumber_iOS_7_x 1047.99


+(MyMeganeLineView*)sharedMeganeLineView
{
	
	if( sharedMeganeLineView == nil )
	{
		sharedMeganeLineView = [[MyMeganeLineView alloc] initWithFrame:CGRectMake(-SIZE/2,-25,SIZE,LINE_HEIGHT)];
	
		sharedMeganeLineView.height = LINE_HEIGHT;
		sharedMeganeLineView.userInteractionEnabled = NO;
		sharedMeganeLineView.opaque = NO;
    sharedMeganeLineView.hidden = YES;
    sharedMeganeLineView.layer.shadowColor = [UIColor blackColor].CGColor;
    sharedMeganeLineView.layer.shadowOpacity = 0.3;
    sharedMeganeLineView.layer.shadowRadius = 20;
    sharedMeganeLineView.layer.shadowOffset = CGSizeMake(0, 10);
    
		sharedMeganeLineView.backgroundColor = [UIColor clearColor];
    //sharedMeganeLineView.windowLevel = UIWindowLevelStatusBar+1;
		sharedMeganeLineView.interfaceOrientation = UIInterfaceOrientationLandscapeLeft;
	}
	
	return sharedMeganeLineView;
}

-(void)handleStatusBarChangeFromHeight:(CGFloat)fromHeight toHeight:(CGFloat)toHeight
{
}

-(void)dealloc
{
	[window_ release];
	[super dealloc];
}

-(void)setFocusCenter:(CGPoint)point
{
	focusCenter_ = point;
	drawCenter_ = point;
	CGSize screenSize = [[UIScreen mainScreen] bounds].size;

	CGFloat height_2 = floorf( self.height/2);
	
	if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_x)
	{
		if( orientation_ == UIInterfaceOrientationPortrait )
		{
      CGFloat yy = MAX( SIZE/2, focusCenter_.y);
      yy = MIN( yy, screenSize.height - SIZE/2);
      
			self.transform = CGAffineTransformMakeTranslation(focusCenter_.x  - SIZE/2 - ABOVE, yy);
		}
		
		if( orientation_ == UIInterfaceOrientationLandscapeLeft )
		{
			self.transform = CGAffineTransformMakeTranslation( focusCenter_.y , screenSize.width - (focusCenter_.x - SIZE/2 - ABOVE) );
		}
		
		if( orientation_ == UIInterfaceOrientationPortraitUpsideDown )
		{
			self.transform = CGAffineTransformMakeTranslation(screenSize.width - focusCenter_.x  + SIZE/2 + ABOVE, screenSize.height - focusCenter_.y );
		}
		
		if( orientation_ == UIInterfaceOrientationLandscapeRight )
		{
			self.transform = CGAffineTransformMakeTranslation( screenSize.height - (focusCenter_.y ), focusCenter_.x  - SIZE/2 - ABOVE);
		}
		
	}else {
		if( orientation_ == UIInterfaceOrientationPortrait )
		{
			focusCenter_.y = fmaxf(focusCenter_.y,  self.height + ABOVE + height_2);
			self.transform = CGAffineTransformMakeTranslation(focusCenter_.x, focusCenter_.y - self.height - ABOVE);
		}
		
		if( orientation_ == UIInterfaceOrientationLandscapeLeft )
		{
			focusCenter_.x = fmaxf(focusCenter_.x,  self.height + ABOVE + height_2);
			self.transform = CGAffineTransformMakeTranslation(focusCenter_.x  - self.height - ABOVE, focusCenter_.y);
		}
		
		if( orientation_ == UIInterfaceOrientationPortraitUpsideDown )
		{
			CGFloat screenHeight = screenSize.height;
			
			focusCenter_.y = fminf(focusCenter_.y,  screenHeight - (self.height + ABOVE + height_2) );
			self.transform = CGAffineTransformMakeTranslation(focusCenter_.x, focusCenter_.y + self.height + ABOVE);
		}
		
		if( orientation_ == UIInterfaceOrientationLandscapeRight )
		{
			CGFloat screenWidth = screenSize.width;
			
			focusCenter_.x = fminf(focusCenter_.x,  screenWidth - (self.height + ABOVE + height_2));
			self.transform = CGAffineTransformMakeTranslation( floorf(focusCenter_.x  + self.height + ABOVE), floorf(focusCenter_.y));
		}
	}
}

-(void)hide
{
	[super setHidden:YES];	
}

-(void)setInterfaceOrientation:(UIInterfaceOrientation)orientation
{
	orientation_ = UIInterfaceOrientationPortrait;
	[self setHeight: height_];
}

-(void)setHeight:(CGFloat)val
{
	height_ = val;
	
	sharedMeganeLineView.transform = CGAffineTransformIdentity;
	
	if( orientation_ == UIInterfaceOrientationPortrait || orientation_ == UIInterfaceOrientationPortraitUpsideDown )
	{
		sharedMeganeLineView.frame = CGRectMake(-height_/2,-SIZE/2, height_, SIZE);
	}
	
	if( orientation_ == UIInterfaceOrientationLandscapeLeft || orientation_ == UIInterfaceOrientationLandscapeRight )
	{
		sharedMeganeLineView.frame = CGRectMake(-SIZE/2, -height_/2, SIZE,height_);
	}
	
	[self setFocusCenter: drawCenter_];
	[self setNeedsDisplay];
}

-(void)setHidden:(BOOL)flag
{
	if( self.hidden == flag ) return;
	
	if( flag )
	{
		CGAffineTransform t1 = CGAffineTransformMakeTranslation(focusCenter_.x, drawCenter_.y );
		t1 = CGAffineTransformScale(t1, 0.1, 0.1);
		
		[UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn   animations:^{
			self.alpha = 0;
		} completion:^(BOOL finished){[super setHidden:flag];}];
		
	}
	
	else {
		
		[super setHidden:flag];
    [self.window addSubview: self];
		
		CGAffineTransform t1 = self.transform;
		CGAffineTransform t0 = CGAffineTransformMakeTranslation(focusCenter_.x, drawCenter_.y );
		t0 = CGAffineTransformScale(t0, 0.1, 0.1);
		
		self.transform = t1;
    self.alpha = 0;

		[UIView animateWithDuration:0.05 delay:0 options:UIViewAnimationOptionCurveEaseIn   animations:^{
      self.alpha = 1;
		} completion:^(BOOL finished){  }];
	}
}

-(void)drawHorizontalGradientInRect:(CGRect)rect fromColor:(UIColor*)fromColor  toColor:(UIColor*)toColor inContext:(CGContextRef)ctx
{
	CGContextSaveGState(ctx);
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGPoint startPoint = rect.origin;
	CGPoint endPoint = CGPointMake(CGRectGetMaxX(rect), rect.origin.y);
	
	const CGFloat *fromComponents;
	const CGFloat *toComponents;
	
	fromComponents = CGColorGetComponents(fromColor.CGColor);
	toComponents = CGColorGetComponents(toColor.CGColor);
	
	
	CGColorRef colorRef1 = CGColorCreate( colorSpace, fromComponents);
	CGColorRef colorRef2 = CGColorCreate( colorSpace, toComponents);
	
	CFMutableArrayRef array = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);
	CFArrayAppendValue(array, colorRef1);
	CFArrayAppendValue(array, colorRef2);
	
	CGFloat locations[3];
	locations[0] = 0;
	locations[1] = 1.0;
	
	CGGradientRef gradient = CGGradientCreateWithColors( colorSpace, array, locations);
	CFRelease(array);
	
	CFRelease(colorRef1);
	CFRelease(colorRef2);
	
	CGContextSetBlendMode(ctx, kCGBlendModeNormal);
	
	CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0 );
	
	CGGradientRelease(gradient);
	CGColorSpaceRelease(colorSpace);
	CGContextRestoreGState(ctx);
}

-(void)addRoundedRectToPath:(CGContextRef) context rect:(CGRect) rect ovalWidth:(CGFloat) ovalWidth  ovalHeight:(CGFloat)ovalHeight
{
    CGFloat fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) { // 1
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context); // 2
    CGContextTranslateCTM (context, CGRectGetMinX(rect), // 3
                           CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight); // 4
    fw = CGRectGetWidth (rect) / ovalWidth; // 5
    fh = CGRectGetHeight (rect) / ovalHeight; // 6
    CGContextMoveToPoint(context, fw, fh/2); // 7
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1); // 8
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1); // 9
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1); // 10
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); // 11
    CGContextClosePath(context); // 12
    CGContextRestoreGState(context); // 13
}

-(void)addFukidashiRectToPath:(CGContextRef) context rect:(CGRect) rect ovalWidth:(CGFloat) ovalWidth  ovalHeight:(CGFloat)ovalHeight
{
	CGFloat height = 11;
	CGContextSaveGState(context);

	// Calc p1, p2
	CGPoint p1, p2;
	CGFloat fw, fh, fh_2;

	CGPoint fukidashiPoint;
	
    fw = rect.size.width ;
    fh = rect.size.height ;
	fh_2 = floorf( fh/2 );
	
	if( orientation_ == UIInterfaceOrientationPortraitUpsideDown )
  {
    fukidashiPoint = CGPointMake(rect.origin.x - height, CGRectGetMidY(rect));
    
    p2 = CGPointMake( rect.origin.x, fukidashiPoint.y + height);
    p1 = CGPointMake( rect.origin.x, fukidashiPoint.y - height );
    
    // 6
    CGContextMoveToPoint(context, fw+rect.origin.x, fh_2+rect.origin.y  );
    
    // 7
    CGContextAddArcToPoint(context, fw+rect.origin.x, fh+rect.origin.y, fw+rect.origin.x-ovalWidth, fh+rect.origin.y, ovalWidth);
    
    // 8
    CGContextAddArcToPoint(context, rect.origin.x, fh+rect.origin.y, rect.origin.x, fh_2+rect.origin.y, ovalWidth);
    
    CGContextAddLineToPoint(context, p2.x, p2.y);
    CGContextAddLineToPoint(context, fukidashiPoint.x, fukidashiPoint.y);
    CGContextAddLineToPoint(context, p1.x, p1.y);
    
    // 9
    CGContextAddArcToPoint(context, rect.origin.x, rect.origin.y, fw+rect.origin.x-ovalWidth, rect.origin.y, ovalWidth);
    
    // 10
    CGContextAddArcToPoint(context, fw+rect.origin.x, rect.origin.y, fw+rect.origin.x, fh_2+rect.origin.y, ovalWidth);
    
    // 11
    CGContextClosePath(context);
  }
	
	else if( orientation_ == UIInterfaceOrientationLandscapeLeft  )
	{
		
		fukidashiPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect) - height);
		
    p1 = CGPointMake( CGRectGetMidX(rect) + height,  CGRectGetMinY(rect));
		p2 = CGPointMake( CGRectGetMidX(rect) - height ,  CGRectGetMinY(rect));
		
		// 6
		
		CGContextMoveToPoint(context, fw+rect.origin.x, rect.origin.y);

		// 7
		CGContextAddArcToPoint(context, fw+rect.origin.x, fh+rect.origin.y, fw+rect.origin.x-ovalWidth, fh+rect.origin.y, ovalWidth);

		// 8
		CGContextAddArcToPoint(context, rect.origin.x, fh+rect.origin.y, rect.origin.x, fh_2+rect.origin.y, ovalWidth);
		
		// 9
		CGContextAddArcToPoint(context, rect.origin.x, rect.origin.y, fw+rect.origin.x-ovalWidth, rect.origin.y, ovalWidth);	

    CGContextAddLineToPoint(context, p2.x, p2.y);
    CGContextAddLineToPoint(context, fukidashiPoint.x, fukidashiPoint.y);
    CGContextAddLineToPoint(context, p1.x, p1.y);
    
		// 10
		CGContextAddArcToPoint(context, fw+rect.origin.x, rect.origin.y, fw+rect.origin.x, fh_2+rect.origin.y, ovalWidth);

		// 11
		CGContextClosePath(context);
	}
	
	else if( orientation_ ==  UIInterfaceOrientationLandscapeRight)
  {
    
    fukidashiPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect) + height);
    
    p1 = CGPointMake( CGRectGetMidX(rect) - height ,  CGRectGetMaxY(rect));
    p2 = CGPointMake( CGRectGetMidX(rect) + height,  CGRectGetMaxY(rect));

    // 6
    
    CGContextMoveToPoint(context, fw+rect.origin.x, rect.origin.y + ovalWidth);
    
    // 7
    CGContextAddArcToPoint(context, fw+rect.origin.x, fh+rect.origin.y, fw+rect.origin.x-ovalWidth, fh+rect.origin.y, ovalWidth);
    CGContextAddLineToPoint(context, p2.x, p2.y);
    CGContextAddLineToPoint(context, fukidashiPoint.x, fukidashiPoint.y);
    CGContextAddLineToPoint(context, p1.x, p1.y);
    
    // 8
    CGContextAddArcToPoint(context, rect.origin.x, fh+rect.origin.y, rect.origin.x, fh_2+rect.origin.y, ovalWidth);
    
    // 9
    CGContextAddArcToPoint(context, rect.origin.x, rect.origin.y, fw+rect.origin.x-ovalWidth, rect.origin.y, ovalWidth);

    // 10
    CGContextAddArcToPoint(context, fw+rect.origin.x, rect.origin.y, fw+rect.origin.x, fh_2+rect.origin.y, ovalWidth);
    
    // 11
    CGContextClosePath(context);
  }
	else//if( orientation_ == UIInterfaceOrientationPortrait )
	{
		fukidashiPoint = CGPointMake(fw+rect.origin.x + height, CGRectGetMidY(rect));
		
    p1 = CGPointMake( fw+rect.origin.x, fukidashiPoint.y + height);
		p2 = CGPointMake( fw+rect.origin.x, fukidashiPoint.y - height );
		
		// 6
		CGContextMoveToPoint(context, fw+rect.origin.x, fh_2+rect.origin.y - height );
    CGContextAddLineToPoint(context, p2.x, p2.y);
    CGContextAddLineToPoint(context, fukidashiPoint.x, fukidashiPoint.y);
    CGContextAddLineToPoint(context, p1.x, p1.y);
    
    // 7
		CGContextAddArcToPoint(context, fw+rect.origin.x, fh+rect.origin.y, fw+rect.origin.x-ovalWidth, fh+rect.origin.y, ovalWidth);

		// 8
		CGContextAddArcToPoint(context, rect.origin.x, fh+rect.origin.y, rect.origin.x, fh_2+rect.origin.y, ovalWidth);
		
		// 9
		CGContextAddArcToPoint(context, rect.origin.x, rect.origin.y, fw+rect.origin.x-ovalWidth, rect.origin.y, ovalWidth);	

    // 10
		CGContextAddArcToPoint(context, fw+rect.origin.x, rect.origin.y, fw+rect.origin.x, fh_2+rect.origin.y, ovalWidth);
		
		// 11
		CGContextClosePath(context);
	}
	
	CGContextRestoreGState(context);
	// 13
	
}

-(void)drawRect:(CGRect)rect
{
	if( !self.window ) return;
	
	CGRect frameRect; 
	CGRect bodyRect;  
	CGPoint drawTransform;
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGAffineTransform t = CGAffineTransformIdentity;

	CGFloat scale = 1.5;
	CGFloat buf = self.window.layer.contentsScale;

	CGSize shadowOffset = CGSizeZero;
#define SHWDOW_OFFSET 3

		if( orientation_ == UIInterfaceOrientationPortraitUpsideDown )
		{
			shadowOffset = CGSizeMake(0, -SHWDOW_OFFSET);
			
			frameRect = CGRectMake(rect.origin.x+18, rect.origin.y + 3, rect.size.width-20, rect.size.height-6);
			//drawTransform = CGPointMake(-drawCenter_.x + self.frame.size.width/2/scale, -drawCenter_.y +30 + bodyRect.size.height/2.0/scale);
			
			drawTransform = CGPointMake( -drawCenter_.x + self.frame.size.height/2/scale - SIZE/scale - self.horizontalFocalOffset + 20,
                                  -drawCenter_.y + (frameRect.size.width/2 - 5 )/scale - 65);
			t = CGAffineTransformRotate(t, -M_PI);

		}
  
		else if( orientation_ == UIInterfaceOrientationLandscapeLeft)
		{
			shadowOffset = CGSizeMake(SHWDOW_OFFSET, 0);
			
			frameRect = CGRectMake(rect.origin.x+3, rect.origin.y+18 , rect.size.width-6, rect.size.height-20);
			
			drawTransform = CGPointMake( -drawCenter_.x + self.frame.size.height/2/scale - SIZE/scale +45 - self.horizontalFocalOffset,
                                  -drawCenter_.y + (frameRect.size.width/2 )/scale);
			t = CGAffineTransformRotate(t, -M_PI_2);

		}
		else if( orientation_ == UIInterfaceOrientationLandscapeRight )
		{
			shadowOffset = CGSizeMake( -SHWDOW_OFFSET, 0);
			
			frameRect = CGRectMake(rect.origin.x+3, rect.origin.y+3 , rect.size.width-6, rect.size.height-20);
			
			drawTransform = CGPointMake(-drawCenter_.x + self.frame.size.height/2/scale - 5 - self.horizontalFocalOffset,
                                  -drawCenter_.y + (frameRect.size.width/2 - 2 )/scale - SIZE/scale);
			t = CGAffineTransformRotate(t, M_PI_2);

		}
		else//if( orientation_ == UIInterfaceOrientationPortrait )
		{
			shadowOffset = CGSizeMake(0, SHWDOW_OFFSET);
			
			frameRect = CGRectMake(rect.origin.x+3, rect.origin.y+3, rect.size.width-20, rect.size.height-6);
			
			// iPod 4
			//drawTransform = CGPointMake(-drawCenter_.x + self.frame.size.width/2/scale, -drawCenter_.y +18 + bodyRect.size.height/2.0/scale);
			
			drawTransform = CGPointMake(-drawCenter_.x + self.frame.size.width/2/scale - self.horizontalFocalOffset - 5, -drawCenter_.y + (frameRect.size.height/2  )/scale);
		}

  frameRect = CGRectInset(frameRect, 0.25, 0.25);
	bodyRect = CGRectInset(frameRect, 3, 3);

  //CGContextSetShadowWithColor(ctx, shadowOffset, 4.0, RGBA(0,0,0,0.3).CGColor);
	[self addFukidashiRectToPath:ctx rect: frameRect ovalWidth:7.0  ovalHeight:7.0 ];
	CGContextSetFillColorWithColor(ctx, RGBA(238,239,241,1).CGColor);
	CGContextFillPath(ctx);
  //CGContextSetShadowWithColor(ctx, CGSizeMake(0, 0), 0.0, [UIColor clearColor].CGColor);
	
  CGContextSetLineWidth(ctx, 0.5);
	CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
	[self addFukidashiRectToPath:ctx rect: frameRect ovalWidth:7.0  ovalHeight:7.0 ];
	CGContextStrokePath(ctx);

	CGContextSaveGState(ctx);
	
	[self addRoundedRectToPath:ctx rect:bodyRect ovalWidth:4 ovalHeight:4];
	CGContextClip(ctx);
	
	[[UIColor whiteColor] set];
	CGContextFillRect(ctx, rect);
	
	t = CGAffineTransformScale(t, scale, scale);
	t = CGAffineTransformTranslate(t, drawTransform.x, drawTransform.y);
	
	CGContextConcatCTM(ctx, t);
	self.window.layer.contentsScale = buf*scale;
	[self.window.layer renderInContext: ctx];	
	self.window.layer.contentsScale = buf;
	
	CGContextRestoreGState(ctx);
	CGContextSaveGState(ctx);
	
	[self addRoundedRectToPath:ctx rect:bodyRect ovalWidth:4 ovalHeight:4];
	CGContextClip(ctx);	
	
	if( orientation_ == UIInterfaceOrientationLandscapeLeft  )
	{
		[self drawHorizontalGradientInRect:CGRectMake(bodyRect.origin.x, bodyRect.origin.y, bodyRect.size.width/2, bodyRect.size.height) fromColor:RGBA(255,255,255,0.6)  toColor:RGBA(255,255,255,0.0) inContext:ctx];
	}
	
	else if( orientation_ == UIInterfaceOrientationLandscapeRight  )
	{
		[self drawHorizontalGradientInRect:CGRectMake(bodyRect.origin.x+bodyRect.size.width/2, bodyRect.origin.y, bodyRect.size.width/2, bodyRect.size.height) fromColor:RGBA(255,255,255,0.0)  toColor:RGBA(255,255,255,0.6)  inContext:ctx];
	}
	
	else if( orientation_ == UIInterfaceOrientationPortraitUpsideDown )
	{
		[DrawUtils drawGradientInRect:CGRectMake(bodyRect.origin.x, bodyRect.origin.y + bodyRect.size.height/2, bodyRect.size.width, bodyRect.size.height/2) fromColor:RGBA(255,255,255,0.0)  toColor:RGBA(255,255,255,0.6)  inContext:ctx];
	}
	
	else {
		[DrawUtils drawGradientInRect:CGRectMake(bodyRect.origin.x, bodyRect.origin.y, bodyRect.size.width, bodyRect.size.height/2) fromColor:RGBA(255,255,255,0.6)  toColor:RGBA(255,255,255,0.0) inContext:ctx];
	}
	
	CGContextRestoreGState(ctx);

	CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
	[self addRoundedRectToPath:ctx rect:bodyRect ovalWidth:5 ovalHeight:5];
	CGContextStrokePath(ctx);
}

@end
