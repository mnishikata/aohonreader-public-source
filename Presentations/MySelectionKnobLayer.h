//
//  KnobLayer.h
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 29/12/10.
//  Copyright 2010 Catalystwo Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "ReaderContentView.h"

@interface MySelectionKnobLayer : CALayer {

	BOOL bottom;
}
- (void)drawInContext:(CGContextRef)ctx;
@property (nonatomic) BOOL bottom;

@end
