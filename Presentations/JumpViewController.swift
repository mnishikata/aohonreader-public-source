//
//  JumpViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 28/12/14.
//  Copyright (c) 2014 Catalystwo. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class JumpViewController: UIViewController, APNumberPadDelegate, UITextFieldDelegate {
	
  var viewModel: JumpViewModel!
  
	// User Interface for Popover
	@IBOutlet weak var inputLabel: UILabel?
  @IBOutlet weak var lawU: UIButton!
  @IBOutlet weak var lawD: UIButton!
  @IBOutlet weak var lawT: UIButton!
  @IBOutlet weak var lawP: UIButton!
	// User Interface for Keypad
	@IBOutlet weak var helpLabel: UILabel?
	@IBOutlet weak var textField: UITextField?

  @IBOutlet weak var lawLabel: UILabel!
  
  var keypadView: APNumberPad? = nil
  var jumpAction: ((_ text: String?, _ in:Law)->Void)? = nil
	var cancelBlock: ((_ swtichToFind: Bool)->())? = nil
	var pickerBlock: (()->())? = nil
	var connector: String? = nil
  
	var allDocuments: NSArray? = nil
  var backgroundColorScheme: BackgroundColorSchemeType = BackgroundColorSchemeType.Paper

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	init(showTab: Bool) {
		super.init(nibName: nil, bundle: nil)
    viewModel = JumpViewModel(traitCollection: self.traitCollection)
    viewModel.showTab = showTab
	}
  
  override func loadView() {
    Bundle.main.loadNibNamed(viewModel.nibName, owner: self, options: nil)
  }
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      return viewModel.contentSize
    }
  }
  
  func deferLoadingViewComponents() {
	}

	override func viewDidLoad() {
		super.viewDidLoad()
    
    var className: NSString? = nil
    if backgroundColorScheme.inverted { className = "APDarkPadStyle" }

    if #available(iOS 11.0, *) {
      viewModel.bottomMargin = self.presentingViewController?.view.safeAreaInsets.bottom ?? 0
    }
    
		if viewModel.popoverStyle {
			// Keyboard
			
			if viewModel.backgroundColorScheme == .Dark { className = "APDarkPadStyle" }
			if viewModel.backgroundColorScheme == .Grey { className = "APDarkPadStyle" }
			
      keypadView = APNumberPad(delegate: self, numberPadStyleClass: className as String!, iPadStyle: true, bottomMargin: 0)!
			
      let (rect, mask) = viewModel.setupKeypadViewForPopover(viewSize: self.view.bounds.size)
			keypadView?.frame = rect
      keypadView?.autoresizingMask = mask
			self.view.addSubview(keypadView!)
			
		}else {
			
      //textField?.placeholder = WLoc("Jump to article")
      textField?.textColor = viewModel.backgroundColorScheme.textColor
      textField?.tintColor = viewModel.backgroundColorScheme.tintColor
			// InputView
			keypadView = APNumberPad(delegate: self, numberPadStyleClass: className as String!, iPadStyle: false, bottomMargin: viewModel.bottomMargin)
			
			textField!.inputView = keypadView!
		}
		
		// Ohter setup
		
		inputLabel?.text = viewModel.inputtedString
    inputLabel?.textColor = viewModel.backgroundColorScheme.textColor
    helpLabel?.textColor = viewModel.backgroundColorScheme.subTextColor

    self.view.backgroundColor = UIColor.clear
    if let popover = self.popoverPresentationController {
      popover.backgroundColor = backgroundColorScheme.popoverBackgroundColor
    }
    
    // Setup Pickerview
    
    if viewModel.showTab == false {
      
    }else {
      helpLabel?.isHidden = true
    }
    
    
    lawD.setImage(UIImage(named:"lawDOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawD.setImage(UIImage(named:"lawDOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)
    
    lawU.setImage(UIImage(named:"lawUOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawU.setImage(UIImage(named:"lawUOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)
    
    lawP.setImage(UIImage(named:"lawPOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawP.setImage(UIImage(named:"lawPOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)
    
    lawT.setImage(UIImage(named:"lawTOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawT.setImage(UIImage(named:"lawTOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)
    
    // VERSION 3

		updateUI()
	}
	
	func insertText(_ string: String!) {
		
		viewModel.insertText(string)
    inputLabel?.text = viewModel.inputtedString
		textField?.text = viewModel.inputtedString
		
		updateUI()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		textField?.becomeFirstResponder()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		deferLoadingViewComponents()
	}
	
	func enableConnectorButton() -> Bool {
		return false
	}
	
  //MARK:- Actions
  
  @IBAction func lawSelectionChanged(_ sender: AnyObject) {
    viewModel.targetLaw = Law(rawValue: Int8(sender.tag)) ?? .patent
    updateUI()
  }
  
  func inputConnector() {
  }
  
  func jumpTapped() {
    jumpAction?(viewModel.inputtedString, viewModel.targetLaw)
  }
  
  func deleteBackward() {
    
    if viewModel.inputtedString == "" { return }
    viewModel.deleteBackward()
    inputLabel?.text = viewModel.inputtedString
    textField?.text = viewModel.inputtedString
    
    updateUI()
	}
	
	@IBAction func close(_ sender: AnyObject) {
		cancelBlock?(false)
	}
	
	func dismissKeyboard() { //aka jump
		cancelBlock?(false)
	}
	
	func findTapped() {  //aka jump
		cancelBlock?(true)
	}
	
	@IBAction func pickerTapped(_ sender: AnyObject) {  //aka jump
		pickerBlock?()
	}
	
	func numberPadTouchesBegan() {
		AudioServicesPlaySystemSound(0x450);
	}

	func textFieldShouldClear(_ textField: UITextField) -> Bool {
		viewModel.inputtedString = ""

		updateUI()
		return true
	}
	
	func updateUI() {
		textField?.inputView?.setNeedsLayout()
		keypadView?.setRightButtonTitle("")
    //displayTargetButton.setImage( UIImage(named: "JumpRadioButtons"), for: UIControlState())
    
    lawP.isSelected = viewModel.targetLaw == .patent
    lawU.isSelected = viewModel.targetLaw == .utility
    lawD.isSelected = viewModel.targetLaw == .design
    lawT.isSelected = viewModel.targetLaw == .trademark
    
    lawLabel.text = viewModel.targetLaw.humanReadableAbbreviation
	}
	
	//MARK:- Tab Picker
	
	func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		updateUI()
	}
	
}

