//
//  KnobLayer.m
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 29/12/10.
//  Copyright 2010 Catalystwo Ltd. All rights reserved.
//

#import "MySelectionKnobLayer.h"
#import "DrawUtils.h"
#import "Tui.h"

@implementation MySelectionKnobLayer
@synthesize  bottom;



#define kUpdateFGColor RGBA(255, 131, 48, 1)


-(UIColor*)iOS7SelectedColor
{
  return RGBA(41, 132, 254, 1);
}

-(CGFloat)contentsScale
{
  return 6.0;
}

- (void)drawInContext:(CGContextRef)ctx
{
  BOOL flatDesign = NO;
  
  /*
   if( ![[[UIDevice currentDevice] systemVersion] hasPrefix:@"6"] && ![[[UIDevice currentDevice] systemVersion] hasPrefix:@"5"])
   {
   flatDesign = YES;
   }
   */
  
  CGRect frame = self.frame;
  CGPoint points[2];
  CGFloat RADIUS = 6;
  
  CGFloat height_2 = floorf(frame.size.height/2);
  //CGFloat width_2 = floorf(frame.size.width/2);
  
  CGFloat ALPHA = 0.5;
  
  if( !bottom )
  {
    
    // Bar
    
    if( flatDesign )
    {
      CGContextSetLineWidth(ctx, 2.0);
      
      points[0] = CGPointMake(0, height_2);
      points[1] = CGPointMake(frame.size.width-2*RADIUS, height_2);
      
      CGContextSetStrokeColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
      
      
    }else
    {
      CGContextSetLineWidth(ctx, 1.0);
      
      points[0] = CGPointMake(0, height_2);
      points[1] = CGPointMake(frame.size.width-2*RADIUS, height_2);
      
      CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,1).CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
      
      points[0] = CGPointMake(0, height_2-1);
      points[1] = CGPointMake(frame.size.width-2*RADIUS, height_2-1);
      
      CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
      
      
      points[0] = CGPointMake(0, height_2+1);
      points[1] = CGPointMake(frame.size.width-2*RADIUS, height_2+1);
      
      CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
    }
    
    // Balls
    
    CGRect ballRect;
    
    //ballRect = CGRectMake( width_2 -RADIUS, frame.size.height- RADIUS*2 -2, RADIUS*2, RADIUS*2);
    ballRect = CGRectMake(  frame.size.width- RADIUS*2 -2, height_2 -RADIUS, RADIUS*2, RADIUS*2);
    
    if( flatDesign )
    {
      CGContextSetFillColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
      CGContextAddEllipseInRect(ctx, ballRect);
      CGContextFillPath(ctx);
    }else
    {
      
      CGContextSetShadow(ctx, CGSizeMake(0, 2), 1.0);
      CGContextSetFillColorWithColor(ctx, RGBA(255,255,255,1).CGColor );
      CGContextFillEllipseInRect(ctx, ballRect);
      CGContextSetShadow(ctx, CGSizeMake(0, 0), 0.0);
      
      ballRect = CGRectInset(ballRect, 1, 1);
      CGContextAddEllipseInRect(ctx, ballRect);
      
      CGContextSaveGState(ctx);
      CGContextClip(ctx);
      [DrawUtils drawGradientInRect:ballRect fromColor:RGBA(30,84,234,1) toColor:RGBA(20,54,147,1) inContext:ctx];
      CGContextRestoreGState(ctx);
    }
  }
  
  else
  {
    // Bar
    
    if( flatDesign )
    {
      CGContextSetLineWidth(ctx, 2.0);
      
      points[0] = CGPointMake(RADIUS*2+2, height_2);
      points[1] = CGPointMake(frame.size.width, height_2);
      
      
      CGContextSetStrokeColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
      
    }else
    {
      CGContextSetLineWidth(ctx, 1.0);
      
      points[0] = CGPointMake(RADIUS*2+2, height_2);
      points[1] = CGPointMake(frame.size.width, height_2);
      
      
      CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,1).CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
      
      points[0] = CGPointMake(RADIUS*2+2, height_2-1);
      points[1] = CGPointMake(frame.size.width, height_2-1);
      
      
      CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
      
      points[0] = CGPointMake(RADIUS*2+2, height_2+1);
      points[1] = CGPointMake(frame.size.width, height_2+1);
      
      CGContextSetStrokeColorWithColor(ctx, RGBA(42,77,220,ALPHA).CGColor);
      CGContextStrokeLineSegments(ctx, points, 2);
    }
    
    // Balls
    
    CGRect ballRect = CGRectMake( 2, height_2 - RADIUS, RADIUS*2, RADIUS*2);
    
    if( flatDesign )
    {
      CGContextSetFillColorWithColor(ctx, [self iOS7SelectedColor].CGColor);
      CGContextAddEllipseInRect(ctx, ballRect);
      CGContextFillPath(ctx);
    }else
    {
      CGContextSetShadow(ctx, CGSizeMake(0, 2), 1.0);
      CGContextSetFillColorWithColor(ctx, RGBA(255,255,255,1).CGColor );
      CGContextFillEllipseInRect(ctx, ballRect);
      CGContextSetShadow(ctx, CGSizeMake(0, 0), 0.0);
      
      ballRect = CGRectInset(ballRect, 1, 1);
      CGContextAddEllipseInRect(ctx, ballRect);
      
      CGContextSaveGState(ctx);
      CGContextClip(ctx);
      [DrawUtils drawGradientInRect:ballRect fromColor:RGBA(30,84,234,1) toColor:RGBA(20,54,147,1) inContext:ctx]; 
      CGContextRestoreGState(ctx);
    }
  }
}

@end
