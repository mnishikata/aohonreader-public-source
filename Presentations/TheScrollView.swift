//
//  TheScrollView.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 5/12/16.
//
//

import UIKit

final class TheScrollView: UIScrollView, UIGestureRecognizerDelegate {
  
  var contentOffsetDidChangeHandeler: ((_ offset: CGPoint,_ scrollView: TheScrollView)->Void)? = nil
  
  private var horizontalScrollBar: WKHorizontalScrollBar!
  private var accessoryView: WKAccessoryView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    autoresizesSubviews = false
    contentMode = .redraw
    showsHorizontalScrollIndicator = true
    showsVerticalScrollIndicator = false
    scrollsToTop = false
    delaysContentTouches = false
    canCancelContentTouches = true // ***8
    isPagingEnabled = false
    autoresizingMask = [.flexibleWidth, .flexibleHeight]
    panGestureRecognizer.delegate = self
    
    let bar = WKHorizontalScrollBar(frame: CGRect(x: 0, y: 0, width: 100, height: 10))
    horizontalScrollBar = bar
    horizontalScrollBar.handleHitHeight = 20
    horizontalScrollBar.scrollView = self
    layoutHorizontalScrollBar()
    
    let accessory = WKAccessoryView(frame: CGRect(x: 0, y: 0, width: 65, height: 30) )
    accessoryView = accessory
    horizontalScrollBar.handleAccessoryView = accessoryView
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    self.superview?.addSubview(horizontalScrollBar)
  }
  
  override var frame: CGRect {
    didSet {
      layoutHorizontalScrollBar()
    }
  }
  
  var bottomMargin: CGFloat = 0 {
    didSet {
      layoutHorizontalScrollBar()
    }
  }
  func layoutHorizontalScrollBar() {
    let contentWidth = frame.size.width - contentInset.left - contentInset.right;
    horizontalScrollBar?.frame = CGRect(x: contentInset.left, y: 1, width: contentWidth, height: bounds.size.height - bottomMargin)
  }
  
  func growHandle() {
    horizontalScrollBar.growHandle()
  }
  
  func setAccessoryViewLabel(_ string: String) {
    accessoryView.textLabel.text = string
  }
  
  override var contentOffset: CGPoint {
    didSet {
      contentOffsetDidChangeHandeler?(contentOffset, self)
    }
  }
  
  /*
   func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
   
   print("** DEBUG MODE")
   
   return false
   }
   */
}
