//
//  PopoverSettingsViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 15/08/15.
//  Copyright © 2015 Catalystwo. All rights reserved.
//

import UIKit
import Social
import StoreKit

class GearViewController: UITableViewController {

	var masterViewController: ReaderViewControllerSwift? = nil
	var documentInteractionController_: UIDocumentInteractionController? = nil

  var showQuickNotes = false

  //@IBOutlet weak var uploadButton: UIButton!, appLabel: UILabel!, fileSIzeLabel: UILabel!
  //@IBOutlet weak var settings: UIButton!, sendButton: UIButton!, linkButton: UIButton!, backup: UIButton!
	
  //@IBOutlet weak var debugButton: UIButton!
	//MARK:- Body
  
  #if EDITION20
  let storeID = 1217317638
  #else
  let storeID = 1174701373
  #endif
  
  init() {
    super.init(style: .grouped)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func appStore() {
    let controller = SKStoreProductViewController()

    controller.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier:storeID], completionBlock: nil)
    controller.delegate = self
    present(controller, animated: true, completion: nil)
/*
		let address = "itms-apps://itunes.apple.com/app/law/id916069107?ls=1&mt=8"
		UIApplication.shared.openURL( URL(string: address)! )
 */
	}
  
  func twitter() {
    
    let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
    let address = "itms-apps://itunes.apple.com/app/id\(String(storeID))?ls=1&mt=8"
    
    controller?.add( URL(string: address)! )
    controller?.setInitialText("#青本ブラウザ ")
    present(controller!, animated: true, completion: nil)
  }
  
  func facebook() {
    let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
    let address = "itms-apps://itunes.apple.com/app/law/id\(String(storeID))?ls=1&mt=8"
    
    controller?.add( URL(string: address)! )
    controller?.setInitialText("#青本ブラウザ ")
    present(controller!, animated: true, completion: nil)
  }
	
	func settings() {
    guard let master = masterViewController else { return }

		masterViewController!.dismiss(animated: true) {
			let nav = UINavigationController( rootViewController: master.appSettingsViewController() )
			nav.modalPresentationStyle = .formSheet
			master.appSettingsViewController().showDoneButton = true;
			master.present( nav, animated: true, completion: nil )
		}
	}
  
  func diffSettings() {
    let value = !UserDefaults.standard.bool(forKey: "DisableDiff")
    UserDefaults.standard.set(value, forKey: "DisableDiff")
    tableView.reloadData()
    NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil)
  }
	
  func leadToNewVersion() {
    let path = "http://catalystwo.com/AohonReaderNewVersion.html"
    UIApplication.shared.openURL( URL(string: path)! )
  }

  func exportPDF(from indexPath: IndexPath) {
    let url = RealmRepository.documentUrl
    let controller = UIActivityViewController(activityItems: [url], applicationActivities: [])
    controller.modalPresentationStyle = .popover
    let popover = controller.popoverPresentationController
    //popover?.barButtonItem = sender
    popover?.sourceView = tableView
    popover?.sourceRect = tableView.rectForRow(at: indexPath)
    present(controller, animated: true, completion: nil)
  }
  
	override func viewDidLoad() {
		super.viewDidLoad()
		
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
  }
  
  @objc func done() {
    self.dismiss(animated: true, completion: nil)
  }
  
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Potentially incomplete method implementation.
    // Return the number of sections.
    
    return 3
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    switch section {
    case 0: return ""
    case 1: return ""
    case 2: return "SHARE"
    default: break
    }
    return nil
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0: return 1
    case 1: return 4
    case 2: return 2
    default: break
    }
    return 0
  }
  
  
  override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    if section == 1 {
      return WLoc("DiffDisclaimer")
    }
    
    return nil
  }
  
  
  func versionString() -> String {
    let bundle = CFBundleGetMainBundle();
    let versionString = CFBundleGetValueForInfoDictionaryKey(bundle, "CFBundleShortVersionString" as CFString!) as! String
    let buildVersionString = CFBundleGetValueForInfoDictionaryKey(bundle, "CFBundleVersion" as CFString!) as! String
    
    let string = NSString(format: "Version %@ (%@)", versionString,  buildVersionString)
    
    return string as String;
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ??   UITableViewCell(style: .value1, reuseIdentifier: "Cell")
    cell.imageView?.image = nil
    cell.detailTextLabel?.text = nil
    switch indexPath.section {
    case 0:
      //cell.imageView?.image = UIImage(named:"LawRoundIcon")
      cell.textLabel?.text = "逐条解説 第\(String(Law.edition))版 " + versionString()
    case 1:
      switch indexPath.row {
      case 0: cell.textLabel?.text = WLoc("Settings")
      case 1:
        #if EDITION20
          cell.textLabel?.text = WLoc("旧版からの変更箇所の表示")
        #else
          cell.textLabel?.text = WLoc("新版への変更箇所の表示")
        #endif
        cell.detailTextLabel?.text = UserDefaults.standard.bool(forKey: "DisableDiff") ? "いいえ" : "はい"
      case 2: cell.textLabel?.text = WLoc("PDFをエクスポート")
      case 3: cell.textLabel?.text = WLoc("ヘルプ")
      default: break
      }



    case 2:
      switch indexPath.row {
      case 0: cell.textLabel?.text = "Facebook"
      cell.imageView?.image = UIImage(named:"facebook")

      case 1: cell.textLabel?.text = "Twitter"
      cell.imageView?.image = UIImage(named:"twitter")

      case 2: cell.textLabel?.text = WLoc("Send Data")

      default: break
      }
    default: break
    }
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch indexPath.section {
    case 0: appStore()
    case 1:
        switch indexPath.row {
        case 0: settings()
        case 1: diffSettings()
        case 2: exportPDF(from: indexPath)
        case 3: leadToNewVersion()
        default: break
        }

    case 2:
      switch indexPath.row {
      case 0: facebook()
      case 1: twitter()

      default: break
      }
    default: break
    }
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

extension GearViewController: SKStoreProductViewControllerDelegate {
  func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
   dismiss(animated: true, completion: nil)
  }
}
