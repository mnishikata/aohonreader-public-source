//
//  SearchViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 13/05/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit



final class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

  var viewModel = SearchViewModel()

	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var tableView: UITableView!

  @IBOutlet weak var lawD: UIButton!
  @IBOutlet weak var lawT: UIButton!
  @IBOutlet weak var lawU: UIButton!
  @IBOutlet weak var lawP: UIButton!
  
  @IBAction func lawSelectionChanged(_ sender: UIButton) {
    if let thisLaw = Law(rawValue: Int8(sender.tag)) {
      viewModel.toggleLawSelection(thisLaw)
    }
    
    updateUI()
  }
  
  func updateUI() {
    lawD.isSelected = viewModel.contains(law: .design)
    lawU.isSelected = viewModel.contains(law: .utility)
    lawP.isSelected = viewModel.contains(law: .patent)
    lawT.isSelected = viewModel.contains(law: .trademark)
    
    let menu = UIMenuController.shared
    menu.menuItems = []
  }
  
	var searchViewControllerShouldCancelBlock: (()->())? = nil
  var searchViewControllerDoSearchBlock: ((_ text:String?, _ lawMask: [Law])->())? = nil
	var searchViewControllerExpandBlock: (()->())? = nil

  required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	init() {
		super.init(nibName: "SearchViewController", bundle: Bundle(for: type(of: self)))
	}
	
	deinit {
		print("= Dealloc SearchViewController")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.tintColor = viewModel.backgroundColorScheme.tintColor
		tableView.backgroundColor = .clear
		
		searchBar.searchBarStyle = .minimal
		searchBar.placeholder = WLoc("Search")
    if viewModel.backgroundColorScheme.inverted {
      searchBar.keyboardAppearance = .dark
      searchBar.barStyle = .blackTranslucent
    }
    if let popover = self.popoverPresentationController {
      popover.backgroundColor = viewModel.backgroundColorScheme.popoverBackgroundColor
    }
    if viewModel.backgroundColorScheme.inverted {
      self.tableView.indicatorStyle = .white
      self.tableView.separatorColor = UIColor(white: 1, alpha: 0.3)
    }
    
    lawD.setImage(UIImage(named:"lawDOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawD.setImage(UIImage(named:"lawDOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)

    lawU.setImage(UIImage(named:"lawUOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawU.setImage(UIImage(named:"lawUOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)
    
    lawP.setImage(UIImage(named:"lawPOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawP.setImage(UIImage(named:"lawPOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)
  
    lawT.setImage(UIImage(named:"lawTOff")!.colorizedImage(withTint: viewModel.backgroundColorScheme.textColor, alpha: 1, glow: false), for: .normal)
    lawT.setImage(UIImage(named:"lawTOn")!.colorizedImage(withTint: viewModel.backgroundColorScheme.tintColor, alpha: 1, glow: false), for: .selected)
    
    updateUI()
	}
	
	override func viewDidAppear(_ animated:Bool) {
		super.viewDidAppear( animated )
		searchBar.becomeFirstResponder()
	}
	
	override var preferredContentSize: CGSize {
		set {}
		get {
			return viewModel.contentSize
    }
	}
	
	@IBAction func husoku(_ sender: AnyObject) {
		
		searchViewControllerExpandBlock?()
		tableView.tableHeaderView = nil
	}

	// MARK: - Search bar
	
	func searchBarCancelButtonClicked( _ searchBar: UISearchBar) {
		searchViewControllerShouldCancelBlock?()
	}
	
	func searchBarSearchButtonClicked( _ searchBar: UISearchBar) {
		
		if searchBar.text != nil && searchBar.text!.characters.count > 0 && viewModel.numberOfLaws > 0 {
			
			viewModel.moveTextToTop(searchBar.text!)
			searchViewControllerDoSearchBlock?( searchBar.text, viewModel.lawMask )
		}
	}
	
	// MARK: - Table Delegate
	
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		
		if searchBar.isFirstResponder {
			searchBar.resignFirstResponder()
		}
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.section == 0 {
      let text = viewModel.moveToTop(at: indexPath.row)
			searchViewControllerDoSearchBlock?( text, viewModel.lawMask )
		}
		
		if indexPath.section == 1 {
			
			let alertController = UIAlertController(title: WLoc("Delete History"), message: WLoc("Delete History Message"), preferredStyle: .alert)
			
			let action1 = UIAlertAction(title: WLoc("Delete"), style: .destructive) { (action) -> Void in
				self.viewModel.removeAll()
				self.tableView.reloadData()
			}
			
			let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
			
			alertController.addAction(action1)
			alertController.addAction(cancel)
			
			present(alertController, animated: true, completion: nil)
			tableView.deselectRow(at: indexPath, animated: true)
		}
	}
	
	// MARK: - TableView Data Source
	
  func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
    let header = view as! UITableViewHeaderFooterView
    header.backgroundView?.backgroundColor = viewModel.backgroundColorScheme.tableBackgroundColor
    header.textLabel?.textColor = viewModel.backgroundColorScheme.textColor
  }
  
	func tableView( _ tableView: UITableView,  canEditRowAt indexPath: IndexPath) -> Bool {
	
    if indexPath.section == 0 { return true }
    
    return false
  }
  
  func tableView( _ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    
    viewModel.remove(at: indexPath.row)
    
    if viewModel.numberOfItems == 0 {
      tableView.reloadData()
    }else {
			tableView.deleteRows(at: [indexPath], with: .automatic)
		}
	}
  
	func numberOfSections( in tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if viewModel.numberOfItems == 0 || section != 0 { return nil }
    return WLoc("History")
  }
  
  func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows(at: section)
  }
  
  func tableView( _ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.section == 0 {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")  ??
        UITableViewCell(style: .default, reuseIdentifier: "Cell")
      
      cell.backgroundColor = UIColor.clear
      if viewModel.backgroundColorScheme.inverted {
        let view = UIView()
        view.backgroundColor = viewModel.backgroundColorScheme.tableSelectionColor
        cell.selectedBackgroundView = view
      }
      cell.textLabel!.textColor = viewModel.backgroundColorScheme.textColor
      cell.textLabel!.text = viewModel.string(at: indexPath.row)
      return cell
      
    } else  {
      let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
			cell.textLabel!.textAlignment = .center
			cell.textLabel!.text = WLoc("Clear History")
			cell.textLabel?.textColor = self.view.tintColor
			cell.backgroundColor = UIColor.clear
      if viewModel.backgroundColorScheme.inverted {
        let view = UIView()
        view.backgroundColor = viewModel.backgroundColorScheme.tableSelectionColor
        cell.selectedBackgroundView = view
      }
			return cell
		}
	}
}
