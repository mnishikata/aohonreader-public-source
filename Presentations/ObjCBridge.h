//
//  ObjCBridge.h
//  Reader
//
//  Created by Masatoshi Nishikata on 7/11/16.
//
//

#import <Foundation/Foundation.h>

@interface ObjCBridge : NSObject

-(void)drawInContext:(CGContextRef _Nonnull )ctx page: (NSInteger)page;
+(instancetype _Nonnull )shared;
+(NSString  * _Nonnull )versionString;

@end
