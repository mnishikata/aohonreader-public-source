//
//  ReaderViewControllerSwift.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 12/11/16.
//
//

import Foundation
import StoreKit
import InAppSettingsKit

final class ReaderViewControllerSwift: UIViewController {

  static var startupLocation: [String : Any]? = nil {
    didSet {
      if startupLocation != nil {
      NotificationCenter.default.post(name: DataSourceNotification.focus, object: nil, userInfo: startupLocation)
      }
    }
  }
  fileprivate var appSettingsViewController_: IASKAppSettingsViewController? = nil

  fileprivate var document: DocumentViewModel!
  fileprivate var contentViews: ContentViewController!
  fileprivate lazy var knobs = KnobViewController()

  fileprivate var theScrollView: TheScrollView!
  fileprivate var findToolbar: MyToolBar!
  fileprivate var highlightViewController_: HighlightViewController? = nil
  private weak var backToLawButton: UIButton? = nil
  fileprivate weak var arttitleLabel: MyVerticalTextLabel!

  //fileprivate var lastAppearSize: CGSize = .zero
  fileprivate var lastHideTime: Date!
  fileprivate var ignoreDidScroll: Bool = false
  
  fileprivate var lastContentPosition: (CGFloat, CGSize) = (0, CGSize.zero) // ContentOffset / View Size
  
  //MARK:- BODY
  init(readerDocument object: DocumentViewModel) {
    super.init(nibName: nil, bundle: nil)
    
    let center = NotificationCenter.default // Default notification center
    center.addObserver(self, selector: #selector(applicationWillResign(_ :)), name: .UIApplicationWillTerminate, object: nil)
    center.addObserver(self, selector: #selector(applicationWillResign(_ :)), name: .UIApplicationWillResignActive, object: nil)
    center.addObserver(self, selector: #selector(setNeedsDisplay(_:)), name: DataSourceNotification.setNeedsDisplay, object: nil)
    center.addObserver(self, selector: #selector(focus(_:)), name: DataSourceNotification.focus, object: nil)
    center.addObserver(self, selector: #selector(contentViewDidEndZooming(_:)), name: Notification.Name("ReaderContentViewDidEndZooming"), object: nil)
    
    object.updateProperties()
    document = object; // Retain the supplied ReaderDocument object for our use
    document.delegate = self
    
    contentViews = ContentViewController(readerDocument: object)
    contentViews.canStartPanningHandler = { contentView, fromPoint in
      let point = contentView.convert(fromPoint, to: self.view)
      if self.knobs.hitTest(point, in: self.view, contentViews: self.contentViews) != nil { return false }
      return true
    }
    //ReaderThumbCache.touch(withGUID: object.guid) // Touch the document thumb cache directory
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  //MARK:- LAYOUTING CONTENT VIEWS
  
  func updateContentSize() {
    let spacing: CGFloat = 10
    var width = self.view.bounds.size.width
    let height = self.view.bounds.size.height;
    var contentHeight = height; // Height

    let pageSize = document.pageSize
    //pageSize.width -= 58.9 * 2
    // 483 x 685.4 - 58.9
    
    if DEVICE_IS_IPAD {
      width =  pageSize.width/pageSize.height * height
      document.viewWidth = width + spacing
      contentHeight = height
    }else {
      document.viewWidth = width
      contentHeight = height; // Height
    }
    
    let page = document.pageNumber
    // THIS RESETS CONTENT OFFSET
    theScrollView.contentSize = CGSize(width: document.contentWidth, height: contentHeight);
    showDocumentPage(document.pageCount - page)
  }
  
  func updateContentViews() {
    updateContentSize()
    
    contentViews.updateForSize(theScrollView.bounds.size)
    
    let contentOffset = CGPoint(x: document.contentOffsetX, y: 0.0)
    
    if theScrollView.contentOffset.equalTo(contentOffset) == false // Update
    {
      theScrollView.contentOffset = contentOffset // Update content offset
    }
  }
  
  private func showDocumentPage(_ page: Int, atPdf pdfOrigin: CGPoint? = nil) {
    // if pdfOrigin == nil && page == (document.pageNumber as Int) { return }
    
    guard page >= 1 && page <= (document.pageCount as Int!) else { return }
    
    document.pageNumber = page
    
    let contentOffset = CGPoint(x: document.contentOffsetX, y: 0.0)
    
    if (theScrollView.contentOffset.equalTo(contentOffset) == true) {
      contentViews.layout(theScrollView)
    } else {
      theScrollView.contentOffset = contentOffset
    }
    contentViews.resetZoom()
    contentViews.setNeedsDisplay()

    if pdfOrigin != nil {
      if let p2 = contentViews.pageOrigin(page: page, pdfOrigin: pdfOrigin!) {
        var detailedContentOffset = contentOffset

        detailedContentOffset.x += p2.x
        detailedContentOffset.x -= theScrollView.bounds.size.width/2;
        theScrollView.contentOffset = detailedContentOffset
      }
    }
  }
  
  func showDocument() {
    updateContentSize() // Update content size first
    document.lastOpen = Date() // Update document last opened date
  }
  
  func closeDocument() {
    // DELEGATE METHOD HERE...
  }

  //MARK:- SETTING UP VIEWS

  override func viewDidLoad() {
    super.viewDidLoad()
    
    assert(document != nil); // Must have a valid ReaderDocument
    
    // BLACKLISTING
    /*
    DataSource.shared.getBl { blackListed in
      if blackListed {
        self.view.isUserInteractionEnabled = false
        debugPrint("##")
      }
    }
    */
    
    self.view.backgroundColor = UIColor.lightGray // Neutral gray
    
    let viewRect = self.view.bounds; // View bounds
    
    let scrollViewRect = viewRect.insetBy(dx: -document.scrollViewOutset, dy: 0.0)
    theScrollView = TheScrollView(frame: scrollViewRect) // All
    theScrollView.delegate = self
    theScrollView.backgroundColor = document.backgroundColorScheme.backgroundColor
    self.view.addSubview(theScrollView)
    
    theScrollView.contentOffsetDidChangeHandeler = { [unowned self] (offset, scrollView) in
      self.document.contentOffsetX = offset.x
    }
    
    let findToolbarRect = CGRect(x: viewRect.maxX - 44, y: viewRect.maxY - 370, width: 44, height: 350)
    let view = MyToolBar(frame: findToolbarRect)
    
    view.clearFindHandler = { [weak self] in self?.clearFind() }
    view.findPreviousHandler = { [weak self] in self?.findPrevious() }
    view.findNextHandler = { [weak self] in self?.findNext() }
    view.findHandler = { [weak self] in self?.find() }
    view.jumpHandler = { [weak self] in self?.jump() }
    view.showHighlightListHandler = { [weak self] in self?.showHighlightList() }
    view.fontHandler = { [weak self] in self?.settings() }
    view.viewModel.colorScheme = document.backgroundColorScheme
    
    findToolbar = view;
    self.view.addSubview(findToolbar)
    if #available(iOS 11.0, *) {
      findToolbar.translatesAutoresizingMaskIntoConstraints = false
      
      let rightConstraint = NSLayoutConstraint(item: findToolbar, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0.0)
      let widthConstraint = NSLayoutConstraint(item: findToolbar, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 44)
      let heightConstraint = NSLayoutConstraint(item: findToolbar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 350)
      
      let guide = self.view.safeAreaLayoutGuide
      NSLayoutConstraint.activate([
        findToolbar.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: 0), rightConstraint, widthConstraint, heightConstraint
        ])
    }
    
    
    let singleTapOne = MyTapGestureRecognizer(target: self, action: nil)
    singleTapOne.delegate = self;
    theScrollView.addGestureRecognizer(singleTapOne)
    
    
    // DRAGGING KNOB
    let panGesture = MeganeLineGestureRecognizer(target: self, action: #selector(pan(gesture:)))
    panGesture.delegate = self
    theScrollView.addGestureRecognizer(panGesture)
    
    // LONG PRESS
    let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
    longPressGesture.delegate = self
    theScrollView.addGestureRecognizer(longPressGesture)
    
    lastHideTime = Date()
    let titleViewModel = ArticleTitleViewModel(document: document)
    let titleLabelWidth = titleViewModel.viewWidth

    let titleLabel = MyVerticalTextLabel(frame: CGRect(x: self.view.bounds.size.width - titleLabelWidth, y: 0, width: titleLabelWidth, height: 200))
    arttitleLabel = titleLabel
    arttitleLabel.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
    arttitleLabel.layer.shadowColor = UIColor(white:0, alpha:0.2).cgColor
    arttitleLabel.layer.shadowRadius = 5.0
    arttitleLabel.layer.shadowOpacity = 1
    arttitleLabel.layer.shadowOffset = CGSize(width: 0, height: 1)
    arttitleLabel.attributedString = titleViewModel.attributedTitle
    arttitleLabel.backgroundScheme = document.backgroundColorScheme.rawValue;
    
    self.view.addSubview(arttitleLabel)

    if #available(iOS 11.0, *) {
      arttitleLabel.translatesAutoresizingMaskIntoConstraints = false
      
      let rightConstraint = NSLayoutConstraint(item: arttitleLabel, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0.0)
      let widthConstraint = NSLayoutConstraint(item: arttitleLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: titleLabelWidth)
      let heightConstraint = NSLayoutConstraint(item: arttitleLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)

      let guide = self.view.safeAreaLayoutGuide
      NSLayoutConstraint.activate([
        arttitleLabel.topAnchor.constraint(equalTo: guide.topAnchor, constant: 0), rightConstraint, widthConstraint, heightConstraint
        ])
      
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    
    backToLawButton?.removeFromSuperview()
    backToLawButton = nil
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.becomeFirstResponder()
    
    if theScrollView.contentSize.equalTo(CGSize.zero) == true {
      DispatchQueue.main.async {
        self.showDocument()
        if let userInfo = ReaderViewControllerSwift.startupLocation {
          NotificationCenter.default.post(name: DataSourceNotification.focus, object: nil, userInfo: userInfo)
        }
      }
    }
    
    if #available(iOS 11.0, *) {
      theScrollView.bottomMargin = self.view.safeAreaInsets.bottom
    } else {
      // Fallback on earlier versions
    }
    theScrollView.growHandle()
    #if READER_DISABLE_IDLE // Option
      UIApplication.shared.idleTimerDisabled = true
    #endif // end of READER_DISABLE_IDLE Option
    
    
    
    #if EDITION20
      
      let ud = UserDefaults.standard
      if ud.bool(forKey: "DiffAlert") != true {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          
          let controller = UIAlertController( title: "変更箇所の表示について", message: WLoc("DiffDisclaimer"), preferredStyle: .alert)
          let cancel = UIAlertAction(title: WLoc("OK"), style: .cancel) { (action) -> Void in
            ud.set(true, forKey: "DiffAlert")

          }
          controller.addAction(cancel)
          self.present(controller, animated: true, completion: nil)
        }
      }
      
      
    #else
      let ud = UserDefaults.standard
      
      if ud.bool(forKey: "Edition20") != true {
        
        ud.set(true, forKey: "Edition20")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          
          let controller = UIAlertController( title: "第20版がリリースされました", message: "特許庁が第20版を公開しました。第20版対応版のブラウザをダウンロードしますか？", preferredStyle: .alert)
          let okAction = UIAlertAction(title: WLoc("OK"), style: .default) { (action) -> Void in
            let path = "http://catalystwo.com/AohonReaderNewVersion.html"
            UIApplication.shared.openURL( URL(string: path)! )
          }
          let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in
          }
          controller.addAction(okAction)
          controller.addAction(cancel)
          
          self.present(controller, animated: true, completion: nil)
        }
      }else {
      
        let ud = UserDefaults.standard
        if ud.bool(forKey: "DiffAlert") != true {
          
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            let controller = UIAlertController( title: "変更箇所の表示について", message: WLoc("DiffDisclaimer"), preferredStyle: .alert)
            let cancel = UIAlertAction(title: WLoc("OK"), style: .cancel) { (action) -> Void in
              ud.set(true, forKey: "DiffAlert")
              
            }
            controller.addAction(cancel)
            self.present(controller, animated: true, completion: nil)
          }
        }
      }
    #endif
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    //lastAppearSize = self.view.bounds.size; // Track view size
    
    #if READER_DISABLE_IDLE // Option
      UIApplication.shared.idleTimerDisabled = false
    #endif // end of READER_DISABLE_IDLE Option
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)

    ignoreDidScroll = true
    
    if UIApplication.shared.applicationState == .active {
      self.lastContentPosition = (0, CGSize.zero)
    }
    
    coordinator.animate(alongsideTransition: { (context) in
      
      if self.theScrollView.contentSize.equalTo(CGSize.zero) == false {
        self.updateContentViews()
        //self.lastAppearSize = CGSize.zero;
        
      }
    }, completion: { context in
      self.ignoreDidScroll = false
      
      self.contentViews.layout(self.theScrollView, invalidateAllPages: true)
      self.showDocumentPage(self.document.pageNumber + 1)
      
      if self.lastContentPosition.1.equalTo(self.view.bounds.size) {
        self.theScrollView.contentOffset = CGPoint(x: self.lastContentPosition.0, y: 0)
      }
    })
  }
  
  @objc func applicationWillResign(_ notification:Notification) {
    document.saveProperties()
    backToLawButton?.removeFromSuperview()
    backToLawButton = nil
    
    if findToolbar.alpha != 1 {
      findToolbar.show()
    }
    
    lastContentPosition = (theScrollView.contentOffset.x, self.view.bounds.size)
  }

  func checkReviewScreen() -> Bool {
    let model = SSRatingPickerViewModel()
    if model.shouldReview() {
      
      if #available(iOS 10.3, *) {
        SKStoreReviewController.requestReview()
        model.didReview()
      }
      
      //// Review message
      /*
      let message = WLoc("Review App")
      
      let controller = UIAlertController( title: nil, message: message, preferredStyle: .alert)
      let okAction = UIAlertAction(title: WLoc("Review App Button"), style: .default) { (action) -> Void in
        self.rateMe()
      }
      let cancel = UIAlertAction(title: WLoc("Later"), style: .cancel) { (action) -> Void in
        model.reviewLater()
      }
      controller.addAction(okAction)
      controller.addAction(cancel)
      
      present(controller, animated: true, completion: nil)
     */
      
      return true
    }
    
    return false
  }
  
  @objc func setNeedsDisplay(_ notification: Notification) {
    contentViews.setNeedsDisplay()
  }
  
  func updateTintColor() {
    
    arttitleLabel.backgroundScheme = document.backgroundColorScheme.rawValue;
    focusedPageNumberDidChange()
    
    findToolbar.viewModel.colorScheme = document.backgroundColorScheme
    theScrollView.backgroundColor = document.backgroundColorScheme.backgroundColor
    
    contentViews.updateBackgroundScheme()
    
    // CHECK APP ICON
    if #available(iOS 10.3, *) {
      if UIApplication.shared.supportsAlternateIcons {
        let appIconName = self.document.appIconName
        
        if UIApplication.shared.alternateIconName != appIconName {
          UIApplication.shared.setAlternateIconName(appIconName) { err in
            self.document.appIconName = UIApplication.shared.alternateIconName
            print("** error \(String(describing: err?.localizedDescription))")
            
          }
        }
      }
    }
  }
  
  //MARK:- ACTIONS
  func find() {

    let contentViewController = SearchViewController()
    contentViewController.modalPresentationStyle = .popover
    contentViewController.searchViewControllerShouldCancelBlock = {
      self.dismiss(animated: true, completion: nil)
    }
    
    contentViewController.searchViewControllerDoSearchBlock = { (text: String?, lawMask: [Law]) -> () in
      
      if text != nil {
        DataSource.shared.highlightForString(text! as NSString, in: lawMask)
        self.findNext()
      }
      
      self.dismiss(animated: false) {
        if self.checkReviewScreen() { return }
      }
    }
    
    if let popover = contentViewController.popoverPresentationController {
      popover.permittedArrowDirections = .any
      popover.delegate = self
      popover.sourceView = view
      popover.sourceRect = CGRect( x: self.view.bounds.maxX - 20, y: view.bounds.maxY - 100, width: 1, height: 1 )
    }
    present(contentViewController, animated: false, completion: nil)
  }
  
  func clearFind() {
    DataSource.shared.clearSearch()
    contentViews.setNeedsDisplay()
  }
  
  /*
  func rateMe() {
    
    let ratings = SSRatingPickerViewModel()
    
    let controller = SSRatingPickerViewController()
    controller.title = WLoc("Write a Review")
    controller.delegate = self
    
    let nav = UINavigationController(rootViewController: controller)
    nav.modalPresentationStyle = .formSheet
    present(nav, animated: true, completion: nil)

    if ratings.rating != nil { controller.ratingPicker.selectedNumberOfStars = CGFloat(ratings.rating!) }
    if ratings.title != nil { controller.titleTextField.text = ratings.title! }
    if ratings.reviewText != nil { controller.reviewTextView.text = ratings.reviewText! }
  }
  */
  
  func showHighlightList() {
    let controller = HighlightListTableViewController(style: .grouped)
    controller.jumpAction = { startIndex in
      self.dismiss(animated: false) {
        if let (bounds, page) = DataSource.shared.mojiBounds(at: startIndex) {
          let focusPoint = CGPoint(x: bounds.midX, y: bounds.midY)
          self.showDocumentPage(self.document.pageCount as Int! - Int(page), atPdf: focusPoint)
        }
      }
    }
    
    let nav = UINavigationController(rootViewController: controller)
    nav.modalPresentationStyle = .formSheet
    present(nav, animated: true, completion: nil)
  }
  
  func settings() {
    if appSettingsViewController_ == nil {
      appSettingsViewController_ = IASKAppSettingsViewController()
      appSettingsViewController_!.delegate = self
    }
    
    let controller = GearViewController()
    controller.masterViewController = self
    let nav = UINavigationController(rootViewController: controller)
    nav.modalPresentationStyle = .formSheet
    appSettingsViewController_!.showDoneButton = true
    present(nav, animated: true, completion: nil)
  }
  
  func appSettingsViewController() -> IASKAppSettingsViewController {
    
    if appSettingsViewController_ == nil {
      appSettingsViewController_ = IASKAppSettingsViewController()
      appSettingsViewController_!.delegate = self

    }
    return appSettingsViewController_!
  }
  
  func jump() {
    let controller = JumpViewController(showTab: false)
    controller.backgroundColorScheme = document.backgroundColorScheme
    controller.jumpAction = { text, law in
      self.dismiss(animated: false, completion: { if self.checkReviewScreen() { return } } )
      if text != nil {
        if let (bounds, page) = DataSource.shared.jumpTo(text!, in: law) {
          let focusPoint = CGPoint(x: bounds.midX, y: bounds.midY)
          self.showDocumentPage(self.document.pageCount as Int! - Int(page), atPdf: focusPoint)
        }
      }
    }
    
    controller.cancelBlock = { (swtichToFind: Bool) -> () in
      
      self.dismiss(animated: false) { [weak self] in
        if swtichToFind == true { self?.find() }
      }
    }
    
    controller.pickerBlock = {
      self.dismiss(animated: false)
    }
    
    controller.modalPresentationStyle = .popover
    
    if let popover = controller.popoverPresentationController {
      popover.permittedArrowDirections = .down
      popover.sourceView = self.view
      popover.sourceRect = CGRect( x: self.view.bounds.maxX - 20, y: self.view.bounds.maxY - 44, width: 1, height: 1 )
      popover.delegate = self
    }
    
    present(controller, animated: false, completion: nil)
  }

  @objc func focus(_ notification: Notification) {
    if let userInfo = notification.userInfo {
      if let bounds = userInfo["bounds"] as? CGRect,
        let page = userInfo["page"] as? Int16 {
        let focusPoint = CGPoint(x: bounds.midX, y: bounds.midY)
        self.showDocumentPage(self.document.pageCount as Int! - Int(page), atPdf: focusPoint)
      }
    }
    
    let button = UIButton(frame: CGRect(x:0, y:self.view.bounds.maxY - 20, width:20, height:20))
    button.setImage(UIImage(named: "LawSmall"), for: .normal)
    button.addTarget(self, action: #selector(backToLaw), for: .touchUpInside)
    self.view.addSubview(button)
    backToLawButton = button
    ReaderViewControllerSwift.startupLocation = nil
  }
  
  @objc func backToLaw() {
    let url = URL(string: "comcatalystwolaws:")!
    UIApplication.shared.openURL(url)
  }
  
  func findNext() {
    if let (bounds, page, foundLocations) = DataSource.shared.findNext() {
      let focusPoint = CGPoint(x: bounds.midX, y: bounds.midY)
      showDocumentPage(document.pageCount as Int! - Int(page), atPdf: focusPoint)
      findToolbar.update(foundLocations)
    }else {
      findToolbar.clearFind()
      clearFind()
    }
  }
  
  func findPrevious() {
    if let (bounds, page, foundLocations) = DataSource.shared.findPrevious() {
      let focusPoint = CGPoint(x: bounds.midX, y: bounds.midY)
      showDocumentPage(document.pageCount as Int! - Int(page), atPdf: focusPoint)
      findToolbar.update(foundLocations)
    }else {
      findToolbar.clearFind()
      clearFind()
    }
  }
  
  @objc func copySelection(_ sender: Any?) {
    guard let selection = document.selection else { return }
    let (string,_) = DataSource.shared.string(with: selection.selectedRange)
    UIPasteboard.general.string = string
  }
  
  @objc func lookup(_ sender: Any?) {
    guard let selection = document.selection else { return }
    let (string,_) = DataSource.shared.string(with: selection.selectedRange)
    
    let controller = UIReferenceLibraryViewController(term: string)
    present(controller, animated: true, completion: nil)
  }
  
  @objc func highlight(_:Any) {
    guard let selection = document.selection else { return }
    
    DataSource.shared.addHighlight(range: selection)
    contentViews.setNeedsDisplay()
    document.selection = nil
  }
  
  //MARK:- KNOBS AND SELECTION

  func setupKnobs(start: (CGRect, Int)?, end: (CGRect, Int)?) {
  
    for gesture in theScrollView.gestureRecognizers! {
      if gesture is MeganeLineGestureRecognizer { continue }
      if gesture.state == .began || gesture.state == .changed {
        setKnobsHidden(true)
        return
      }
    }
    
    if start == nil && end == nil {
      setKnobsHidden(true)
      return
    }
    
    knobs.setup(start: start, end: end, contentViews: contentViews)
    setKnobsHidden(false)
  }
  
  func setKnobsHidden(_ flag: Bool) {
    knobs.isHidden = flag
    
    if flag == true {
      highlightViewController_?.dismiss(animated: false, completion: nil)
      highlightViewController_ = nil
    }else {
      showMenu()
    }
  }
  
  var selectedRect: CGRect? {
    if topKnobFrame == nil { return nil }
    return bottomKnobFrame?.union(topKnobFrame!)
  }
  
  var topKnobFrame: CGRect? {
    var rect = knobs.knobFrame(for: .top, in: self.view, contentViews: contentViews)
    rect?.origin.x -= document.sideCutOff
    return rect
  }
  
  var bottomKnobFrame: CGRect? {
    var rect = knobs.knobFrame(for: .bottom, in: self.view, contentViews: contentViews)
    rect?.origin.x -= document.sideCutOff
    return rect
  }
  
  //MARK:- MENU
  
  override var canBecomeFirstResponder: Bool {
    return true
  }
  
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if NSStringFromSelector(action) == "copySelection:" { return true }
    //if NSStringFromSelector(action) == "_lookup:" { return true }

    return super.canPerformAction(action, withSender: sender)
  }
  
  func showMenu(difX: CGFloat = 0) {
    guard let selection = document.selection else { return }

    var animated = true
    if highlightViewController_ != nil {
      highlightViewController_?.dismiss(animated: false, completion: nil)
      highlightViewController_ = nil
      animated = false
    }
    
    guard selection.selectedRange.isEmpty == false else {
      return
    }
    
    for gesture in theScrollView.gestureRecognizers! {
      if gesture.state == .began || gesture.state == .changed {
        return
      }
    }
    
    guard var targetRect = selectedRect else { return }
    targetRect.origin.x -= difX
    
    let menu = UIMenuController.shared
    updateMenuItems()
    targetRect = targetRect.insetBy(dx: 0, dy: -20)
    menu.setTargetRect(targetRect, in: self.view)
    menu.setMenuVisible(true, animated: animated)
  }
  
  func updateMenuItems() {
    let menu = UIMenuController.shared
    let item1 = UIMenuItem(title: WLoc("Copy"), action: #selector(copySelection(_:)))
    let item2 = UIMenuItem(title: WLoc("Highlight"), action: #selector(highlight(_:)))
    let item3 = UIMenuItem(title: WLoc("Define"), action: #selector(lookup(_:)))
    
    menu.menuItems = [item1, item2, item3]
  }
  
  @objc func contentViewDidEndZooming(_ notification:Notification) {
    guard document.selection != nil else { return }
    
    guard var targetRect = selectedRect else { return }
    let menu = UIMenuController.shared
    
    targetRect = targetRect.insetBy(dx: 0, dy: -20)
    menu.setTargetRect(targetRect, in: self.view)
    menu.setMenuVisible(true, animated: true)
  }

  func showHighlightViewController(at targetRect: CGRect, existingHighlight: Any? = nil) {
    if highlightViewController_ != nil {
      highlightViewController_!.dismiss(animated: false, completion: nil)
    }
    
    let contentViewController = HighlightViewController()
    contentViewController.delegate = self
    if existingHighlight != nil {
      contentViewController.userInfo = ["highlight": existingHighlight!]
    }
    let notes = DataSource.shared.notesForEditingHighlight(existingHighlight!)
    contentViewController.notes = notes
    contentViewController.isTrashHidden = (existingHighlight != nil ? false : true )
    contentViewController.selectedColor = DataSource.shared.colorForEditingHighlight(existingHighlight)
    if existingHighlight != nil {
      contentViewController.forceRequireConfirmOnDeletion = notes?.isEmpty == false
    }
    highlightViewController_ = contentViewController
    let _ = contentViewController.view // instantiate
    
    let popover = contentViewController.popoverPresentationController
    popover?.permittedArrowDirections = [.up, .down]
    popover?.sourceView = self.view
    popover?.sourceRect = targetRect.insetBy(dx: -10, dy: -10)
    popover?.delegate = self
    //popover?.passthroughViews = [self.view]
        
    present(contentViewController, animated: true, completion: nil)

  }
}

//MARK:- DocumentViewModelDelegate
extension ReaderViewControllerSwift: DocumentViewModelDelegate {
  
  func focusedPageNumberDidChange() {
    arttitleLabel.attributedString = ArticleTitleViewModel(document: document).attributedTitle
    theScrollView.setAccessoryViewLabel( document.articleTitle )
  }
  
  func selectionChanged(old: CGRect, new: CGRect, pages: ClosedRange<Int16>, showKnobs: Bool) {
    
    if showKnobs {
      var (start, end) = contentViews.selectionBounds()
      start?.0.size.height = 0
      if let maxY = end?.0.maxY {
      end?.0.origin.y = maxY
      }
      end?.0.size.height = 0
      
      setupKnobs(start: start, end: end )
    }
    
    // DISPLAY
    let unionRect = old.union(new)
    contentViews.setNeedsDisplayIn(pages, rect: unionRect )
  }

}

//MARK:- UIScrollViewDelegate
extension ReaderViewControllerSwift: UIScrollViewDelegate {

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if ignoreDidScroll == false {  contentViews.layout(scrollView)  }
  }
  
  func handleScrollViewDidEnd(_ scrollView: UIScrollView) {
    
    document.contentOffsetX = scrollView.contentOffset.x
    contentViews.resetZoom(exceptFor: document.pageNumber)
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    handleScrollViewDidEnd(scrollView)
  }
  
  func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    handleScrollViewDidEnd(scrollView)
  }
  
  func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    //if highlightViewController_ != nil {
    let point: CGPoint = targetContentOffset[0]
    let difX = point.x - theScrollView.contentOffset.x
    
    showMenu(difX: difX)
    //}
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    let vel = scrollView.panGestureRecognizer.velocity(in: scrollView)
    if vel.x < -100 && findToolbar.alpha == 0 {
      findToolbar.show()
    }
    
    if vel.x > 100 && findToolbar.alpha == 1 {
      findToolbar.hide()
    }
  }
  
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    let vel = scrollView.panGestureRecognizer.velocity(in: scrollView)

    if vel.x < -100 && findToolbar.alpha == 0 {
     findToolbar.show()
    }
    
    if vel.x > 100 && findToolbar.alpha == 1 {
      findToolbar.hide()
    }
    
    highlightViewController_?.dismiss(animated: false, completion: nil)
    highlightViewController_ = nil
    //tap(nil)
  }
}

//MARK:- UIGestureRecognizerDelegate
extension ReaderViewControllerSwift: UIGestureRecognizerDelegate {
  
  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    
    let point = gestureRecognizer.location(in: self.view)

    if findToolbar.frame.contains(point) { return false }
    if gestureRecognizer is MeganeLineGestureRecognizer {
      if knobs.hitTest(point, in: self.view, contentViews: contentViews) != nil { return true }
      return false
    }
    
    if gestureRecognizer is UILongPressGestureRecognizer {
      if knobs.hitTest(point, in: self.view, contentViews: contentViews) != nil { return false }
      if document.selectedHighlight != nil { return false }
      return true
    }
    
    return true
  }
  
  func doubleTap(_ p: CGPoint) {
    //tap(nil)
    contentViews.select(at: p, in: theScrollView)
  }
  
  func tappedOnAttribute(key: String, value: String, at point: CGPoint) {
    print("Tapped: \(key) - \(value)")
  }
  
  func tap(_ p: CGPoint?, tapped: Bool = false) {
    
    // guard let selection = DataSource.shared.selection else { return }
    guard let point = p else {
      _ = document.selectHighlight(at: nil)
      return
    }
    
    if  knobs.hitTest(point, in: self.view, contentViews: contentViews) != nil { return }
    
    if findToolbar.frame.contains(point) { return }
    
    if tapped != true {
      contentViews.selectHighlight(at: point, in: theScrollView)
    }

    if tapped {
      
      document.selection = nil
      
      highlightViewController_?.dismiss(animated: false, completion: nil)
      highlightViewController_ = nil
      
      if let selectedHighlight = document.selectedHighlight {
        let pointInView = self.view.convert(p!, from: theScrollView)
        showHighlightViewController(at: CGRect(x: pointInView.x, y: pointInView.y, width: 11, height: 11), existingHighlight: selectedHighlight)
      }
    }
  }
  
  @objc func longPress(gesture: UILongPressGestureRecognizer) {
    guard let megane = MyMeganeLineView.shared() else { return }
    
    switch gesture.state {
    case .cancelled, .failed, .ended:
      megane.isHidden = true
      
      let seletion = document.selection
      document.selection = seletion
      return
    case .began:
      //tap(nil)
      setupKnobs(start: nil, end: nil)
      fallthrough
    default:
      let point = gesture.location(in: self.view)
      
      contentViews.select(at: point, in: self.view) {
        let pointInWindow = gesture.location(in: self.view.window)
        megane.interfaceOrientation = self.view.window!.rootViewController!.interfaceOrientation
        
        megane.window = self.view.window
        megane.focusCenter = pointInWindow
        megane.horizontalFocalOffset = 0
        megane.setNeedsDisplay()
        megane.isHidden = false
      }
    }
  }

  @objc func pan(gesture: MeganeLineGestureRecognizer) {
    guard let megane = MyMeganeLineView.shared() else { return }
    if gesture.state == .cancelled || gesture.state == .failed || gesture.state == .ended {
      
      // Extend to word rect
      
      /*
       if false == gesture.staticEnd {
       if gesture.bottom {
       let lastIndex = selectedRange.end.characterLoc + 1
       
       IndexedPosition * lastPosition = [IndexedPosition positionWithIndex:lastIndex inAttributedString:attributedString_];
       
       IndexedRange *range = (IndexedRange*)[tokenizer_ rangeEnclosingPosition: lastPosition withGranularity:UITextGranularityWord  inDirection:UITextStorageDirectionBackward];
       
       if( range )
       selectedTextNSRange_ = NSUnionRange(selectedTextNSRange_, range.range);
       
       }else  {
       IndexedPosition * position = [IndexedPosition positionWithIndex:selectedTextNSRange_.location inAttributedString:attributedString_];
       
       IndexedRange *range = (IndexedRange*)[tokenizer_ rangeEnclosingPosition: position withGranularity:UITextGranularityWord  inDirection:UITextStorageDirectionBackward];
       
       if range {
       selectedTextNSRange_ = NSUnionRange(selectedTextNSRange_, range.range)
       }
       }
       }
       */
      
      megane.isHidden = true
      setKnobsHidden(false)
      return
    }
    
    var touchedPoint = gesture.location(in: self.view)
    
    if gesture.state == .began {
      gesture.offset = CGSize.zero
      
      if let hitKnob = knobs.hitTest(touchedPoint, in: self.view, contentViews: contentViews) {
        gesture.bottom = hitKnob == .bottom
        gesture.offset = CGSize(width: touchedPoint.x - knobs.knobFrame(for: hitKnob, in: self.view, contentViews: contentViews)!.midX, height: 0)

      }else {
        print("= Not on knob")
        return
      }
    }
    
    touchedPoint.x -= gesture.offset.width
    
    contentViews.select(at: touchedPoint, extend: true, isBottom: gesture.bottom, in: self.view)
    
    if knobs.isHidden == false {
      megane.interfaceOrientation = self.view.window!.rootViewController!.interfaceOrientation
      let origin = gesture.bottom ? CGPoint(x: bottomKnobFrame!.midX - 20, y: bottomKnobFrame!.origin.y) :  CGPoint(x: topKnobFrame!.midX, y: topKnobFrame!.origin.y)
      megane.window = self.view.window
      megane.horizontalFocalOffset = gesture.bottom ? 20 : 0
      megane.focusCenter = self.view.convert(origin, to: self.view)
      megane.setNeedsDisplay()
      megane.isHidden = false
    }
  }
}

//MARK:-  HighlightViewControllerDelegate
extension ReaderViewControllerSwift: HighlightViewControllerDelegate {
  
  func highlightViewController( _ sender: HighlightViewController, setHighlight highlight: HighlightProtocol ) {
    
    if let userInfo = sender.userInfo, let value = userInfo["highlight"] {
      DataSource.shared.updateHighlight(value, highlight.color!, notes: nil)
      contentViews.setNeedsDisplay()
    }else {
      
      defer {
        defer {
          highlightViewController_?.dismiss(animated: false, completion: nil)
          highlightViewController_ = nil
        }
      }
      
      guard let selection = document.selection else { return }
      let serialId = shortUUIDString()
      sender.userInfo = ["uuid": serialId]
      
      // ADD HIGHLIHGT
      _ = DataSource.shared.addHighlight(highlight.color!, range: selection)
      
      //fetchHighlights()
      document.selection = nil
      contentViews.setNeedsDisplay()
    }
  }
  
  func highlightViewController( _ sender: HighlightViewController, willCloseWithChange: Bool ) {
  }
  
  func requestEditing(from highlightViewController: HighlightViewController ) {
    
    guard let userInfo = highlightViewController.userInfo, let token = userInfo["highlight"] else { return }
    guard let existingText = DataSource.shared.notesForEditingHighlight(token) else { return }
    
    let controller = NotesViewController()
    controller.originalNotes = existingText
    controller.deleteNotes = {
      DataSource.shared.deleteNotesForEditingHighlight(token)
      self.contentViews.setNeedsDisplay()

    }
    controller.setNotes = { text in
      DataSource.shared.setNotesForEditingHighlight(token, notes: text)
      self.contentViews.setNeedsDisplay()
    }
    
    controller.willCloseWithChange = { withChange in  }
    let nav = UINavigationController(rootViewController: controller)
    nav.modalPresentationStyle = .formSheet
    
    highlightViewController_?.dismiss(animated: false, completion: nil)
    present(nav, animated: true, completion: nil)
    highlightViewController_ = nil
  }
  
  func highlightViewController( _ sender: HighlightViewController, deleteWithConfirm requiresConfirm: Bool ) {
    guard let userInfo = sender.userInfo, let token = userInfo["highlight"] else { return }

    let block = {

      DataSource.shared.deleteEditingHighlight(token)
      self.contentViews.setNeedsDisplay()

      self.highlightViewController_?.dismiss(animated: false, completion: nil)
      self.highlightViewController_ = nil
    }
    
    if requiresConfirm == true {
      
      let controller = UIAlertController( title: nil, message: WLoc("Unhighlight Message"), preferredStyle: .actionSheet)
      let deleteAction = UIAlertAction(title: WLoc("Unhighlight"), style: .destructive) { (action) -> Void in
        block()
      }
      let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in }
      controller.addAction(deleteAction)
      controller.addAction(cancel)
      controller.modalPresentationStyle = .popover
      
      if let popover = controller.popoverPresentationController {
        popover.permittedArrowDirections = .down
        popover.sourceRect = presentedViewController!.popoverPresentationController!.sourceRect
        popover.sourceView = presentedViewController!.popoverPresentationController?.sourceView
      }
      
      sender.dismiss(animated: true) {
        
        self.highlightViewController_ = nil;
        self.present(controller, animated: true, completion: nil)
      }
      
    }else {
      block()
    }
  }
}


//MARK:- UIPopoverPresentationControllerDelegate
extension ReaderViewControllerSwift: UIPopoverPresentationControllerDelegate {
  public func adaptivePresentationStyle(for controller: UIPresentationController)
    -> UIModalPresentationStyle {
      return .none
  }
  
  public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection)
    -> UIModalPresentationStyle {
      return .none
  }
  
  public func popoverPresentationController( _ popoverPresentationController: UIPopoverPresentationController,
                                             willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>,
                                             in view: AutoreleasingUnsafeMutablePointer<UIView>) {
  }
  
  public func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
    
  }
}

//MARK:- SSRatingPickerViewControllerDelegate
extension ReaderViewControllerSwift: SSRatingPickerViewControllerDelegate {
  
  func ratingPickerDidFinish(_ controller:SSRatingPickerViewController, send: Bool) {
    
    if send {
      
      var ratings = SSRatingPickerViewModel()
      
      let rating = controller.ratingPicker.selectedNumberOfStars
      let title = controller.titleTextField.text
      let reviewText = controller.reviewTextView.text

      ratings.set(ratings: Int(rating), title: title!, reviewText: reviewText!)
      
    }else {
      SSRatingPickerViewModel().reviewLater()
    }
    
    dismiss(animated: true, completion: nil)
  }
}


//MARK:- IASKSettingsDelegate
extension ReaderViewControllerSwift: IASKSettingsDelegate {
  func settingsViewControllerDidEnd( _ sender: IASKAppSettingsViewController) {
    let ud = UserDefaults.standard
    UIApplication.shared.isIdleTimerDisabled = ud.bool(forKey: "AvoidScreenLock")
    dismiss( animated: true ) {
      self.updateTintColor()
    }
  }
}

