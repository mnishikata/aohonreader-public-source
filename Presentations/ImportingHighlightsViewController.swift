//
//  ImportingHighlightsViewController.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 21/03/17.
//
//

import UIKit

class ImportingHighlightsViewController: UIViewController {

  var url: URL!
  var exporting = false
  @IBOutlet weak var closeButton: UIButton!
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var lsbel: UILabel!
  @IBOutlet weak var indicator: UIActivityIndicatorView!
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    if exporting {
      lsbel.text = "ハイライトの移行処理中"
      
    }else {
      lsbel.text = "ハイライトを読み込んでいます"
    }
    textView.isHidden = true
      closeButton.isHidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if exporting == false {
      importHighlights(at: url)
    }
  }
  
  func importHighlights(at url: URL) {
    DataSource.shared.importHighlights(at: url) { attr in
      
      self.textView.isHidden = false
      self.closeButton.isHidden = false
      self.lsbel.isHidden = true
      self.indicator.isHidden = true
      self.textView.attributedText = attr
    }
  }
  
  func exportHighlights(completion: (Data)->Void) {
    DataSource.shared.exportingHighlights(completion: completion)
  }

  @IBAction func close(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
}
