//
//  Knob.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation

extension MySelectionKnobLayer {
  convenience init(isBottom: Bool) {
    self.init()
    self.contentsScale = UIScreen.main.scale
    self.contentsGravity = kCAGravityLeft
    self.bottom = isBottom
  }
}

enum KnobType {
  case top, bottom
}

protocol KnobViewProtocol: class {
  func displayFrameDidChange(viewModel: KnobViewModel)
}


class KnobViewController: KnobViewProtocol {
  private var top = MySelectionKnobLayer(isBottom: false)
  private var bottom = MySelectionKnobLayer(isBottom: true)
  
  private var topViewModel: KnobViewModel?
  private var bottomViewModel: KnobViewModel?

  var isHidden: Bool {
    get {
      return top.isHidden || bottom.isHidden
    }
    set {
      top.isHidden = newValue
      bottom.isHidden = newValue
    }
  }
  
  func displayFrameDidChange(viewModel: KnobViewModel) {
    
    let view = (viewModel.type == .top ? top : bottom)
    view.frame = viewModel.layerFrame
    view.setNeedsDisplay()
    view.removeAllAnimations()
  }
  
  func setup(start: (CGRect, Int)?, end: (CGRect, Int)?, contentViews: ContentViewController) {
    
    if start != nil {
      contentViews[start!.1]?.contentPage.layer.addSublayer(top)
      topViewModel = KnobViewModel(type: .top, delegate: self)
    }
    topViewModel?.mojiFrame = start!.0
    
    if end != nil {
      contentViews[end!.1]?.contentPage.layer.addSublayer(bottom)
      bottomViewModel = KnobViewModel(type: .bottom, delegate: self)
    }
    bottomViewModel?.mojiFrame = end!.0

    if topViewModel != nil && bottomViewModel != nil {
      topViewModel?.compromiseForIntersectionWith(other: bottomViewModel!)
    }
  }
  
  func hitTest(_ p: CGPoint, in view: UIView, contentViews: ContentViewController) -> KnobType? {
    
    if topViewModel != nil && hitTestSub(p, in: view, contentViews: contentViews, viewModel: topViewModel!) { return .top }
    if bottomViewModel != nil && hitTestSub(p, in: view, contentViews: contentViews, viewModel: bottomViewModel!) { return .bottom }
    return nil
  }
  
  func knobFrame(for type: KnobType, in view: UIView, contentViews: ContentViewController) -> CGRect? {
    if type == .top && topViewModel != nil {
     return frame(in: view, contentViews: contentViews, viewModel: topViewModel!)
    }
    
    if type == .bottom && bottomViewModel != nil {
      return frame(in: view, contentViews: contentViews, viewModel: bottomViewModel!)
    }
    
    return nil
  }
  
  private func hitTestSub(_ p: CGPoint, in view: UIView, contentViews: ContentViewController, viewModel: KnobViewModel) -> Bool {
    guard let selection = contentViews.document.selection else { return false }
    var inKnob = false
    
    let givenPageNumber = (viewModel.type == .top ? selection.pageRange.lowerBound : selection.pageRange.upperBound )
    contentViews.convertTo(p, givenPageNumber: givenPageNumber, in: view) { point in
      if viewModel.hitFrame.contains(point) == true {
        inKnob = true
      }
    }
    return inKnob
  }
  
  private func frame(in view: UIView, contentViews: ContentViewController, viewModel: KnobViewModel) -> CGRect? {
    
    guard let selection = contentViews.document.selection else { return nil }
    
    var convertedRect: CGRect? = nil
    let givenPageNumber = (viewModel.type == .top ? selection.pageRange.lowerBound : selection.pageRange.upperBound )
    
    contentViews.convertFrom(viewModel.mojiFrame, givenPageNumber: givenPageNumber, in: view) { rect in
      convertedRect = rect
    }
    
    return convertedRect
  }
  
}
