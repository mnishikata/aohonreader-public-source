//
//  HIghlightViewController.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata on 11/04/15.
//  Copyright (c) 2015 Catalystwo. All rights reserved.
//

import UIKit

protocol HighlightViewControllerDelegate : class {
  
  func highlightViewController( _ sender: HighlightViewController, setHighlight: HighlightProtocol )
  func requestEditing(from: HighlightViewController )
  func highlightViewController( _ sender: HighlightViewController, deleteWithConfirm: Bool )
}

final class HighlightViewController: UIViewController {
  
  var viewModel = HighlightViewModel()

  @IBOutlet weak var button0: HighlightButton!
  @IBOutlet weak var button1: HighlightButton!
  @IBOutlet weak var button2: HighlightButton!
  @IBOutlet weak var button3: HighlightButton!
  @IBOutlet weak var button5: HighlightButton!
  @IBOutlet weak var button4: HighlightButton!
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var trashButton: UIButton!
  
  weak var delegate: HighlightViewControllerDelegate?
  
  var notes: String! {
    get {
      return viewModel.notes
    }
    set {
      viewModel.notes = newValue
      textView?.text = newValue
    }
  }
  var userInfo: [String: Any]?
  var selectedColor: HighlightColor?
  var isTrashHidden: Bool = false
  var forceRequireConfirmOnDeletion: Bool = false

  init() {
    super.init(nibName: "HighlightViewController", bundle: Bundle(for: type(of: self)))
    modalPresentationStyle = .popover
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  @IBAction func unhighlight(_ sender: UIButton) {
    delegate?.highlightViewController( self, deleteWithConfirm: forceRequireConfirmOnDeletion || viewModel.requiresConfirmOnClosing )
  }
  
  @IBAction func highlight(_ sender: UIButton) {
    
    let tag = sender.tag
    let color = HighlightColor(rawValue: tag)
    viewModel.color = color
    delegate?.highlightViewController( self, setHighlight: viewModel )
    
    let array: [HighlightButton] = [button0, button1, button2, button3, button4, button5]
    for button in array {
      button.isSelected = button.color == color
      button.setNeedsDisplay()
    }
  }
  
  @IBAction func edit(_ sender: UIButton) {
    
    delegate?.requestEditing(from: self )
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    viewModel.setupTextViewVisiblity(textView)
 
    button0.color = .yellow
    button1.color = .green
    button2.color = .blue
    button3.color = .red
    button4.color = .purple
    button5.color = .underline
    
    trashButton.isEnabled = !isTrashHidden
    
    let array: [HighlightButton] = [button0, button1, button2, button3, button4, button5]
    for button in array {
      
      if button.color == selectedColor {
        button.isSelected = true
      }else {
        button.isSelected = false
      }
      button.setNeedsDisplay()
    }
    
    if let popover = self.popoverPresentationController {
      popover.backgroundColor = viewModel.backgroundColorScheme.popoverBackgroundColor
    }
    self.view.backgroundColor = .clear
    self.view.tintColor = viewModel.backgroundColorScheme.tintColor
    self.textView.textColor = viewModel.backgroundColorScheme.textColor
  }
  
  override var preferredContentSize: CGSize {
    set {}
    get {
      return viewModel.contentSize(with: textView)
    }
  }
  
  func export(_ sender: UIBarButtonItem) {
    
    let controller = UIActivityViewController(activityItems: [textView!.text], applicationActivities: [])
    controller.modalPresentationStyle = .popover
    let popover = controller.popoverPresentationController
    popover?.barButtonItem = sender
    
    present(controller, animated: true, completion: nil)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    let width = self.view.frame.size.width
    let newSize = textView.sizeThatFits( CGSize(width: width, height: 1000000) )
    let height = newSize.height
    
    if height > viewModel.maximumTextViewHeight {  textView.flashScrollIndicators() }
  }
}

final class HighlightButton: UIButton {
  
  var color: HighlightColor? = nil {
    didSet {
      setNeedsDisplay()
    }
  }
  
  override func draw(_ rect: CGRect) {
    guard color != nil else { return }
    guard let context = UIGraphicsGetCurrentContext() else { return }
    
    var circleRect = rect.integral.insetBy(dx: 5, dy: 5 );
    
    context.saveGState()
    
    if color == .underline {
      
      UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).set()
      context.fillEllipse(in: circleRect);
      let image = UIImage(named: "HighlightButtonUnderline")
      image?.draw( at: rect.origin )
    }else {
      color!.color().set()
      context.fillEllipse(in: circleRect)
    }
    
    if isSelected == true {
      circleRect = rect.integral.insetBy(dx: 2, dy: 2 )
      context.setLineWidth(2 )
      
      let frameColor = tintColor! as UIColor!
      context.setStrokeColor((frameColor?.cgColor)! )
      context.strokeEllipse(in: circleRect)
    }
    
    context.restoreGState();
  }
  
  override func titleRect( forContentRect contentRect: CGRect) -> CGRect {
    return CGRect.zero
  }
}
