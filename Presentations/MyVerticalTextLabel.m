//
//  MNVerticalTextLabel.m
//  TuiFramework
//
//  Created by Masatoshi Nishikata on 16/10/12.
//
//

#import "MyVerticalTextLabel.h"

@implementation MyVerticalTextLabel


#define MARGIN 7

-(void)drawRect:(CGRect)rect
{
  if( !line_  && attributedString_ )
  {
    line_ = CTLineCreateWithAttributedString((CFAttributedStringRef)attributedString_);
  }
  
  if( !attributedString_ || !line_ || attributedString_.length == 0 )
  {
    [super drawRect:rect];
    return;
  }
  
  CGFloat descent;
  CGFloat ascent;
  CGFloat leading;
  double width;
  
  width = CTLineGetTypographicBounds(line_, &ascent, &descent, &leading);
  
  CGRect backgroundRect = CGRectMake(0,0,rect.size.width,width + MARGIN*2);
  
  if( [self.backgroundScheme isEqualToString:@"Dark"] )
  {
    [[UIColor darkGrayColor] setFill];
  }
  
  else if( [self.backgroundScheme isEqualToString:@"Grey"] )
  {
    [[UIColor darkGrayColor] setFill];
  }
  
  else if( [self.backgroundScheme isEqualToString:@"Sepia"] )
  {
    [[UIColor colorWithRed:247.0/255.0 green:239.0/255.0 blue:224.0/255.0 alpha:1] setFill];
  }
  
  else if( [self.backgroundScheme isEqualToString:@"Paper"] )
  {
    [[UIColor colorWithRed:1 green:1 blue:240.0/255.0 alpha:1] setFill];
  }
  else {
    [[UIColor whiteColor] setFill];
  }
  
  UIRectFill(backgroundRect);
  
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  CGContextSaveGState(ctx);
  
  CGAffineTransform transform = CGAffineTransformMakeScale(1, -1);
  transform = CGAffineTransformTranslate(transform, 0,0);
  CGContextSetTextMatrix(ctx, transform);
  
  CGAffineTransform t;
  
  // Drawing left part
  
  t = CGAffineTransformMakeTranslation( descent - 4, MARGIN );
  
  CGContextConcatCTM(ctx, t);
  
		NSArray* runs = (NSArray*)CTLineGetGlyphRuns(line_);
		for( id run in runs )
    {
      CTRunDraw((CTRunRef)run, ctx, CFRangeMake(0, 0));
    }
  
  CGContextRestoreGState(ctx);
}

@end
