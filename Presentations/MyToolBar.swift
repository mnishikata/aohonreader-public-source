//
//  MyToolBar.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 5/12/16.
//
//

import UIKit

enum MyToolBarButtonType: Int {
  case clear = 0, previous, next, list, find, jump, label, font
  
  var button: UIButton {
    let model = MyToolBarButtonModel(type: self)
    
    let button = UIButton(type: .system)
    button.setImage(model.image, for: .normal)
    
    button.frame = model.frame
    button.autoresizingMask = [.flexibleTopMargin]

    return button
  }
}

protocol MyToolBarProtocol: class {
  func updateSize()
  func updateColor()
}


final class MyToolBar: UIView {
  
  var viewModel: MyToolBarModel!
  var buttons: [MyToolBarButtonType: UIView]!

  override init(frame: CGRect) {
    super.init(frame: frame)
    
    viewModel = MyToolBarModel()
    viewModel.delegate = self
    layer.shadowColor = UIColor(white:0, alpha:0.2).cgColor
    layer.shadowRadius = 5.0
    layer.shadowOpacity = 1
    layer.shadowOffset = CGSize(width: 0, height: 1)
    backgroundColor = .white
    autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin]
    
    let clear = MyToolBarButtonType.clear.button
    clear.addTarget(self, action: #selector(clearFind), for: .touchUpInside)
    addSubview(clear)
    
    let model = MyToolBarButtonModel(type: .label)
    let label = UILabel(frame: model.frame)
    label.font = UIFont.systemFont(ofSize: 12)
    label.textColor = UIColor.gray
    label.textAlignment = .center
    label.numberOfLines = 0
    label.autoresizingMask = [.flexibleTopMargin];
    addSubview(label)
    
    let prev = MyToolBarButtonType.previous.button
    prev.addTarget(self, action: #selector(findPrevious), for: .touchUpInside)
    addSubview(prev)
    
    let next = MyToolBarButtonType.next.button
    next.addTarget(self, action: #selector(findNext), for: .touchUpInside)
    addSubview(next)
    
    let listButton = MyToolBarButtonType.list.button
    listButton.addTarget(self, action: #selector(showHighlightList), for: .touchUpInside)
    addSubview(listButton)
    
    let findButton = MyToolBarButtonType.find.button
    findButton.addTarget(self, action: #selector(find), for: .touchUpInside)
    addSubview(findButton)
    
    let jumpButton = MyToolBarButtonType.jump.button
    jumpButton.addTarget(self, action: #selector(jump), for: .touchUpInside)
    addSubview(jumpButton)
    
    let fontButton = MyToolBarButtonType.font.button
    fontButton.addTarget(self, action: #selector(font), for: .touchUpInside)
    addSubview(fontButton)
    
    buttons = [.clear: clear, .label: label, .previous: prev, .next: next, .list: listButton, .find: findButton, .jump: jumpButton, .font: fontButton]
    //viewModel.extended = false
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func hide() {
    UIView.animate(withDuration: 0.1, animations: {
      self.transform = CGAffineTransform(translationX: self.frame.size.width, y: 0)
    }, completion: { success in
      self.alpha = 0
      self.transform = CGAffineTransform.identity
    })
  }
  
  func show() {
    alpha = 1
    transform = CGAffineTransform(translationX: frame.size.width, y: 0)
    UIView.animate(withDuration: 0.1, animations: { self.transform = .identity }, completion: nil)
  }
  
  func update(_ foundLocations: DataSource.FindResult?) {
    
    guard let foundLocations = foundLocations else  {
      // NOT FOUND
      clearFind()
      return
    }
    
    let label = buttons[.label] as? UILabel
    let nextButton = buttons[.next] as? UIButton
    let prevButton = buttons[.previous] as? UIButton
    nextButton?.isEnabled = viewModel.nextButtonIsEnabled(with: foundLocations)
    prevButton?.isEnabled = viewModel.previousButtonIsEnabled(with: foundLocations)
    
    label?.text = viewModel.labelString(with: foundLocations)
    viewModel.extended = true
  }
  
  var clearFindHandler: (()->Void)?
  var findPreviousHandler: (()->Void)?
  var findNextHandler: (()->Void)?
  var findHandler: (()->Void)?
  var jumpHandler: (()->Void)?
  var showHighlightListHandler: (()->Void)?
  var fontHandler: (()->Void)?

  @objc func clearFind() {
    viewModel.extended = false
    clearFindHandler?()
  }
  @objc func findPrevious() { findPreviousHandler?() }
  @objc func findNext() { findNextHandler?() }
  @objc func find() { findHandler?() }
  @objc func jump() { jumpHandler?() }
  @objc func showHighlightList() { showHighlightListHandler?() }
  @objc func font() { fontHandler?() }

}

extension MyToolBar: MyToolBarProtocol {
  func updateSize() {
    let size = viewModel.size
    let findToolbarRect = CGRect(x: frame.maxX-size.width, y: frame.maxY-size.height, width: size.width, height: size.height)
    if( frame.size.height != findToolbarRect.size.height ) {
      
      UIView.animate(withDuration: 0.1, animations: { self.frame = findToolbarRect }, completion: nil)
      buttons.forEach { $0.value.isHidden = !viewModel.visibleButtonTypes.contains($0.key) }
    }
  }
  
  func updateColor() {
    if viewModel.colorScheme.inverted {
      backgroundColor = viewModel.colorScheme.secondaryBackgroundColor
    }else {
      backgroundColor = viewModel.colorScheme.backgroundColor
    }
    tintColor = viewModel.colorScheme.tintColor
  }
}
