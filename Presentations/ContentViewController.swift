//
//  ContentViews.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation

class ContentViewController: NSObject {
  
  var document: DocumentViewModel!
  var max: Int! { return document!.pageCount as Int! }
  var canStartPanningHandler: ((_ contentView: ReaderContentView, _ fromPoint: CGPoint) -> Bool)?
  
  private var _views: [Int: ReaderContentView] = [:]
  subscript(page: Int) -> ReaderContentView? {
    get {
      return _views[page]
    }
    set {
      _views[page] = newValue
    }
  } 
  
  init(readerDocument object: DocumentViewModel) {
    document = object
  }
  
  func actualPage(_ page: Int) -> Int16 {
    return Int16(max - page)
  }
  
  func addContentView(_ scrollView: UIScrollView, page: Int)  {
    
    var viewRect = CGRect.zero
    viewRect.size = scrollView.bounds.size
    viewRect.size.width = document.pageWidth
    viewRect.size.height *= document.baseZoom
    
    viewRect.origin.x = document.pageWidth * CGFloat(page - 1)
    viewRect = viewRect.insetBy(dx: document.scrollViewOutset, dy: 0.0)
    viewRect.origin.y = document.offsetY
    let phrase = document.password
    
    let thePage = actualPage(page) + 1
    let contentView = ReaderContentView(frame: viewRect, fileURL: document.pdfDocRef, page: UInt(thePage), password: phrase)!
    contentView.message = self
    contentView.setBackgroundScheme(document.backgroundColorScheme.rawValue)
    
    _views[page] = contentView
    scrollView.addSubview(contentView)
  }
  
  func updateBackgroundScheme() {
    _views.forEach { (page: Int, contentView: ReaderContentView) in
      contentView.setBackgroundScheme(document.backgroundColorScheme.rawValue)
    }
  }
  
  func updateForSize(_ size: CGSize) {
    
    _views.forEach { (page: Int, contentView: ReaderContentView) in
      
      var viewRect = CGRect.zero
      viewRect.size = size
      viewRect.origin.x = document.pageWidth * CGFloat(page - 1) // Update X
      
      contentView.frame = viewRect.insetBy(dx: document.scrollViewOutset, dy: 0.0)
    }
  }
  
  func layout(_ scrollView: UIScrollView, invalidateAllPages: Bool = false) {

    if invalidateAllPages {
      _views.values.forEach { $0.removeFromSuperview() }
      _views.removeAll()
    }
    
    let viewWidth = document.pageWidth; // View width
    let contentOffsetX = scrollView.contentOffset.x; // Content offset X
    var pageB = Int((contentOffsetX + viewWidth - 1.0) / viewWidth); // Pages
    
    var pageA = Int(contentOffsetX / viewWidth)
    pageB += 2; // Add extra pages
    
    if pageA < 1 { pageA = 1 }
    if pageB > max { pageB = max }
    
    let pageRange = pageA ... pageB // Make page range (A to B)
    var pageSet = IndexSet(integersIn: pageRange)
    
    for page in _views.keys // Enumerate content views
    {
      if pageSet.contains(page) == false // Remove content view
      {
        let contentView = _views[page]
        contentView?.removeFromSuperview()
        _views[page] = nil
      }
      else // Visible content view - so remove it from page set
      {
        pageSet.remove(page)
      }
    }
    
     let pages = pageSet.count
    
    if pages > 0 // We have pages to add
    {
      var options: NSEnumerationOptions = .concurrent; // Default
      
      if pages == 2 // Handle case of only two content views
      {
        if (max > 2) && (pageSet.last == max) { options = .reverse }
      }
      else if pages == 3 // Handle three content views - show the middle one first
      {
        var workSet = pageSet
        options = .reverse;
        workSet.remove(pageSet.first!)
        workSet.remove(pageSet.last!)
        if let page = workSet.first {
          pageSet.remove(page)
          self.addContentView(scrollView, page: page)
        }
      }
      
      if  options == .reverse {
        pageSet.reversed().forEach {
          self.addContentView(scrollView, page: $0)
        }
      }else {
        pageSet.forEach {
          self.addContentView(scrollView, page: $0)
        }
      }
    }
  }
  
  func resetZoom(exceptFor page: Int? = nil) {
    _views.forEach { (thisPage, contentView) in
      if (thisPage != page) { contentView.zoomReset(animated: false) }
    }
  }
  
  func pageOrigin(page: Int, pdfOrigin: CGPoint) -> CGPoint? {
    var pdfPoint: CGPoint? = nil
    _views.forEach { (thisPage, contentView) in
      if thisPage == page {
        
        let targetP = contentView.contentPage.pdfPoint(toViewPoint: pdfOrigin)
        let p2 = contentView.convert(targetP, from: contentView.contentPage)
        
        contentView.contentPage.animate(at: targetP)
        pdfPoint = p2
      }
    }
    return pdfPoint
  }
  
  func setNeedsDisplay() {
    _views.values.forEach { $0.contentPage.setNeedsDisplay() }
  }
  
  func setNeedsDisplayIn(_ pageRange: ClosedRange<Int16>, rect: CGRect) {
    _views.forEach { (page, contentView) in
    
      if pageRange.contains( actualPage(page) ) {
        let affectedRect = contentView.contentPage.pdfRect(toViewRect: rect)
        contentView.contentPage.setNeedsDisplay(affectedRect)
      }
    }
  }
  
  func select(at p: CGPoint, extend: Bool = false, isBottom:Bool = false, in view: UIView, handler: (()->Void)? = nil) {
    _views.forEach { (page, contentView) in
      let p2 = view.convert(p, to: contentView.contentPage)
      if contentView.contentPage.bounds.contains(p2) {
        
        let point = contentView.contentPage.pdfPoint(fromViewPoint: p2)
        //NSLog("point %.0f %.0f ", point.x, point.y)
        if extend == false {
          document.selectPoint(point, page: actualPage(page))
        }else {
          if let location = DataSource.shared.mojiLocation(at: point, page: actualPage(page), boundaryBased: true) {
            
            if var selection = document.selection {
              
              if isBottom == false {
                if selection.selectedRange.upperBound >= location {
                  
                  selection.selectedRange = location ... selection.selectedRange!.upperBound
                  selection.pageRange = actualPage(page) ... selection.pageRange.upperBound
                  
                  document.selection = selection
                  
                }
              }else {
                if location >= selection.selectedRange!.lowerBound {
                  selection.selectedRange = selection.selectedRange!.lowerBound ... location
                  selection.pageRange = selection.pageRange.lowerBound ... actualPage(page)
                  
                  document.selection = selection
                }
              }
            }
          }
        }
        handler?()
      }
    }
  }

  func selectionBounds() -> (start: (CGRect, Int)?, end: (CGRect, Int)?) {
    
    guard let selectedMojis = document.selectedMojis else { return (nil,nil) }
    guard selectedMojis.isEmpty == false else { return (nil,nil) }
    guard let startMoji = selectedMojis.first else { return (nil,nil) }
    guard let endMoji = selectedMojis.last else { return (nil,nil) }
    
    var start: (CGRect, Int)?, end: (CGRect, Int)?

    _views.forEach { (page, contentView) in
      
      if startMoji.page == actualPage(page) {
        let startbounds = startMoji.bounds
        let rect = contentView.contentPage.pdfRect(toViewRect: startbounds)
        start = (rect, page)
      }
      
      if endMoji.page == actualPage(page) {
        let endbounds = endMoji.bounds
        let rect = contentView.contentPage.pdfRect(toViewRect: endbounds)
        end = (rect, page)
      }
    }
    
    return (start, end)
  }
  
  func selectHighlight(at p: CGPoint, in scrollView: UIScrollView) {
    _views.forEach { (page, contentView) in
      let p2 = scrollView.convert(p, to: contentView.contentPage)
      if contentView.contentPage.bounds.contains(p2) {
        
        let point = contentView.contentPage.pdfPoint(fromViewPoint: p2)
        //NSLog("point %.0f %.0f ", point.x, point.y)
        _ = document.selectHighlight(at: point, page: actualPage(page))
      }
    }
  }
  
  func convertTo(_ p: CGPoint, givenPageNumber: Int16, in view: UIView, handler: ((_ point: CGPoint)->Void)?)  {
    
    _views.forEach { (page, contentView) in
      
      if givenPageNumber == actualPage(page) {
        let p2 = view.convert(p, to: contentView.contentPage)
        
        handler?(p2)
      }
    }
  }

  func convertFrom(_ rect: CGRect, givenPageNumber: Int16, in view: UIView, handler: ((_ rect: CGRect)->Void)?)  {
    
    _views.forEach { (page, contentView) in
      
      if givenPageNumber == actualPage(page) {
        let convertedRect = view.convert(rect, from: contentView.contentPage)
        
        handler?(convertedRect)
      }
    }
  }
}


//MARK:- ReaderContentViewDelegate
extension ContentViewController: ReaderContentViewDelegate {
  
  func contentView(_ contentView: ReaderContentView!, touchesBegan touches: Set<AnyHashable>!) {
  }
  
  func contentViewCanStartPanning(_ contentView: ReaderContentView!, from p: CGPoint) -> Bool {
    return canStartPanningHandler?(contentView, p) ?? true
  }
}
