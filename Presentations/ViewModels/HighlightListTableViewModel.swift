//
//  HighlightListTableViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 18/12/16.
//
//

import Foundation

struct HighlightListTableViewModel {
  
  var rowHeight: CGFloat = 86
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func highlightAttributedString(at index: Int) -> NSAttributedString {
    let highlight = DataSource.shared.highlight(at: index)
    
    var title = highlight.articleEncodedTitle?.decodedArticleTitle ?? ""
    title = highlight.law.humanReadableTitle + title
    
    let boldFont = UIFont.boldSystemFont(ofSize: 14)
    let plainFont = UIFont.systemFont(ofSize: 14)
    
    let mattr = NSMutableAttributedString(string: "")
    let titleAttr = NSAttributedString(string: title + " ", attributes: [.font: boldFont, .foregroundColor: backgroundColorScheme.textColor])
    
    var attributes = [.font: plainFont, .backgroundColor: highlight.color.color(), .foregroundColor: backgroundColorScheme.textColor] as [NSAttributedStringKey : Any]
    
    if highlight.color == .underline {
      attributes = [.font: plainFont, .underlineStyle: NSNumber(value: 2),  .underlineColor: UIColor.red, .foregroundColor: backgroundColorScheme.textColor]
    }
    
    let bodyAttr = NSAttributedString(string: (highlight.highlightedText ?? ""), attributes: attributes)
    mattr.append(titleAttr)
    mattr.append(bodyAttr)
    
    if highlight.notes?.isEmpty == false {
      let bodyAttr = NSAttributedString(string: "💬", attributes: attributes)
      mattr.append(bodyAttr)
    }
    
    #if DEBUG

      let detailAttributes = [.font: plainFont] as [NSAttributedStringKey : Any]

      let details = " \(highlight.lawRaw)\(highlight.articleEncodedTitle ?? "") \(highlight.startString) [\(highlight.startN)] -- \(highlight.endString) [\(highlight.endN)],  [\(highlight.n)]"
      let detailedString = NSAttributedString(string: details, attributes: detailAttributes)
      mattr.append(detailedString)
      
    #endif
    
    return mattr
  }
  
  var numberOfHighlights: Int {
    return DataSource.shared.numberOfHighlights
  }
  
  var highlightExportList: String {
    let count = DataSource.shared.numberOfHighlights
    var string = ""
    for i in 0 ..< count {
      let highlight = DataSource.shared.highlight(at: i)
      let lawTitle = highlight.law.humanReadableTitle
      let article = highlight.articleEncodedTitle?.decodedArticleTitle ?? ""
      let body = highlight.highlightedText ?? ""
      let notes = highlight.notes ?? ""
      let colorName = highlight.color.naturalName()
      string += lawTitle + "\t" + article + "\t" + colorName + "\t" + body + "\t" + notes + "\n"
    }
    
    return string
  }

}
