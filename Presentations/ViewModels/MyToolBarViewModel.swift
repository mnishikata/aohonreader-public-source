//
//  MyToolBarViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 19/12/16.
//
//

import Foundation

class MyToolBarModel {
  
  weak var delegate: MyToolBarProtocol?
  
  var extended = false {
    didSet {
      delegate?.updateSize()
    }
  }
  
  var size: CGSize {
    if extended {
      return CGSize(width: 44, height: 350)
    }else {
      return CGSize(width: 44, height: 210)
    }
  }
  
  var colorScheme: BackgroundColorSchemeType = .White {
    didSet {
      delegate?.updateColor()
    }
  }

  var visibleButtonTypes: [MyToolBarButtonType] {
    if extended {
      return [.clear, .previous, .next, .list, .find, .jump, .label]
    }else {
      return [.font, .list, .find, .jump]
    }
  }
  
  func labelString(with foundLocations: DataSource.FindResult) -> String {
    let index = foundLocations.currentIndex!
    let total = foundLocations.results.count
    
    return NSString(format: "%ld\n——\n%ld", index + 1, total) as String
  }
  
  func nextButtonIsEnabled(with foundLocations: DataSource.FindResult) -> Bool {
    let index = foundLocations.currentIndex!
    let total = foundLocations.results.count
    return !( index + 1 == total )
  }
  
  func previousButtonIsEnabled(with foundLocations: DataSource.FindResult) -> Bool {
    let index = foundLocations.currentIndex!
    return (index != 0 )
  }
  
}


struct MyToolBarButtonModel {
  var type: MyToolBarButtonType
  
  init(type: MyToolBarButtonType) {
    self.type = type
  }
  
  var frame: CGRect {
    switch type {
    case .clear: return CGRect(x: 2, y: 0, width: 40, height: 40)
    case .previous: return CGRect(x: 2, y: 100, width: 40, height: 40)
    case .next: return CGRect(x: 2, y: 150, width: 40, height: 40)
    case .list: return CGRect(x: 2, y: 200, width: 40, height: 40)
    case .find: return CGRect(x: 2, y: 250, width: 40, height: 40)
    case .jump: return CGRect(x: 2, y: 300, width: 40, height: 40)
    case .label: return CGRect(x: 2, y: 40, width: 40, height: 60)
    case .font: return CGRect(x: 2, y: 150, width: 40, height: 40)
    }
  }
  
  var image: UIImage! {
    switch type {
    case .clear: return UIImage(named: "CCalEventCloseButton-flat")!
    case .previous: return UIImage(named: "verticalPrevious")!
    case .next: return UIImage(named: "verticalNext")!
    case .list: return UIImage(named: "DockSwitchToList-flat")!
    case .find: return UIImage(named: "verticalFind")!
    case .jump: return UIImage(named: "verticalJump")!
    case .label: return nil
    case .font: return UIImage(named: "Settings")!
    }
  }
}
