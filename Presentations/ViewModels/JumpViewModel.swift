//
//  JumpViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 18/12/16.
//
//

import Foundation

class JumpViewModel {
  var popoverStyle = false
  var showTab = false
  let keypadWidth: CGFloat = 320
  var inputtedString: String = ""
  var targetLaw: Law {
    set {
      UserDefaults.standard.set( Int(newValue.rawValue), forKey: "JumpViewControllerTargetLaw")
    }
    get {
      return Law(rawValue: Int8(UserDefaults.standard.integer(forKey: "JumpViewControllerTargetLaw")) ) ?? .patent
    }
  }
  
  var traitCollection: UITraitCollection
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  var bottomMargin: CGFloat = 0
  
  var contentSize: CGSize {
    var showsPicker = false
    
    if showTab == false  {
      showsPicker = false
    }
    
    if popoverStyle {
      if traitCollection.verticalSizeClass == .compact {
        if showsPicker == false {
          return CGSize(width: keypadWidth, height: 280)
        }
        return CGSize(width: keypadWidth + 180, height: 280)
      }
      
      if showsPicker == false {
        return CGSize(width: keypadWidth,height: 370)
      }
      return CGSize(width: keypadWidth,height: 320+162)
      
    }else {
      
      if showsPicker == false { return CGSize(width: keypadWidth, height: 150 + bottomMargin) }
      
      return CGSize(width: keypadWidth,height: 210 + bottomMargin) // 150 for short version
    }
  }
  
  init(traitCollection: UITraitCollection) {
    let popoverStyle = traitCollection.userInterfaceIdiom == .pad || UIScreen.main.bounds.size.width > 400.0
    self.popoverStyle = popoverStyle
    self.traitCollection = traitCollection
  }
  
  var nibName: String {
    if popoverStyle {
      return "JumpViewPopoverController"
      
    }else {
      return "JumpOnPhoneViewController"
    }
  }
  
  func setupKeypadViewForPopover(viewSize: CGSize) -> (CGRect, UIViewAutoresizing ) {

    if traitCollection.verticalSizeClass == .compact {
      
      let keyPadHeight: CGFloat = 180

      let frame = CGRect(x: viewSize.width - keypadWidth, y: viewSize.height - keyPadHeight, width: keypadWidth, height: keyPadHeight)
      let autoresizingMask: UIViewAutoresizing = [.flexibleTopMargin, .flexibleLeftMargin]
      return (frame, autoresizingMask)
    }else {
      let keyPadHeight: CGFloat = 240

      let frame = CGRect(x: 0, y: viewSize.height - keyPadHeight, width: viewSize.width, height: keyPadHeight)
      let autoresizingMask: UIViewAutoresizing = [.flexibleTopMargin, .flexibleWidth]
      return (frame, autoresizingMask)
    }

  }
  
  func insertText(_ string: String) {
    inputtedString = inputtedString + string
  }
  
  func deleteBackward() {
    if inputtedString == "" { return }
    inputtedString = String( inputtedString.characters.dropLast() )
  }
}
