//
//  KnobViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 19/12/16.
//
//

import Foundation

class KnobViewModel {
  fileprivate weak var delegate: KnobViewProtocol?
  let SELECTION_KNOB_INSET_X: CGFloat = -20
  let SELECTION_KNOB_INSET_Y: CGFloat = -20
  let SELECTION_OFFSET: CGFloat = 10
  
  let type: KnobType
  var mojiFrame: CGRect! {
    didSet {
      
      var selectionKnobEnclosure = mojiFrame.insetBy(dx: SELECTION_KNOB_INSET_X, dy: SELECTION_KNOB_INSET_Y)
      
      if type == .top {
        selectionKnobEnclosure.origin.x += SELECTION_OFFSET
      }
      else {
        selectionKnobEnclosure.origin.x -= SELECTION_OFFSET
      }
      
      hitFrame = selectionKnobEnclosure
      
      if mojiFrame.equalTo(CGRect.zero) == false {
        delegate?.displayFrameDidChange(viewModel: self)
      }
    }
  }
  private(set) var hitFrame: CGRect!
  
  var layerFrame: CGRect {
    if type == .top {
      return CGRect( x: mojiFrame.minX, y: mojiFrame.minY - 8, width: mojiFrame.size.width + 16, height: 16).integral
    }
    else {
      return CGRect( x: mojiFrame.minX  - 16, y: mojiFrame.minY - 8, width: mojiFrame.size.width + 16, height: 16).integral
    }
  }
  
  init(type: KnobType, delegate: KnobViewProtocol) {
    self.type = type
    self.delegate = delegate
  }
  
  func compromiseForIntersectionWith(other: KnobViewModel) {
    if hitFrame.intersects(other.hitFrame) == false { return }
    let intersection = hitFrame.intersection(other.hitFrame)
    let width_2 = intersection.size.width / 2
    let height_2 = intersection.size.height / 2
    
    // if knob rect is on the same line, cut width half.  If in different lines, cut height half
    if hitFrame.origin.x == other.hitFrame.origin.x {
      hitFrame.size.height -= height_2
      other.hitFrame.size.height -= height_2
      other.hitFrame.origin.y += height_2
    }else {
      
      hitFrame.origin.x += width_2
      hitFrame.size.width -= width_2
      other.hitFrame.size.width -= width_2
    }
  }
}
