//
//  SearchViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 18/12/16.
//
//

import Foundation

class SearchViewModel {

  let contentSize = CGSize(width: 320,height: 480)
  
  var lawMask: [Law] {
    set {
      UserDefaults.standard.set(newValue.map { $0.rawValue }, forKey: "SearchViewControllerLawMask")
    }
    
    get {
      let obj = UserDefaults.standard.object(forKey: "SearchViewControllerLawMask") as? [Int8]
      if obj == nil {
        return [.patent, .utility, .design, .trademark]
      }
      
      var array: [Law] = []
      if obj!.contains(Law.patent.rawValue) { array.append(.patent) }
      if obj!.contains(Law.utility.rawValue) { array.append(.utility) }
      if obj!.contains(Law.design.rawValue) { array.append(.design) }
      if obj!.contains(Law.trademark.rawValue) { array.append(.trademark) }
      if array.count == 0 {
        //array = [.patent, .utility, .design, .trademark]
      }
      return array
    }
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  func toggleLawSelection(_ thisLaw: Law) {
    if let idx = lawMask.index(of: thisLaw) {
      lawMask.remove(at: idx)
    }else {
      lawMask.append(thisLaw)
    }
  }
  
  func contains(law: Law) -> Bool {
    return lawMask.contains(law)
  }
  
  var numberOfLaws: Int {
   return lawMask.count
  }
  
  var dataSource: NSMutableArray = {
    var array = NSMutableArray()
    
    let ud = UserDefaults.standard
    ud.synchronize()
    
    if let savedArray = ud.object(forKey: "SavedHistory") as? NSArray {
      array = savedArray.mutableCopy() as! NSMutableArray
    }
    
    return array
  }()

  var numberOfItems: Int {
    return dataSource.count
  }
  
  var numberOfSections: Int {
    if numberOfItems > 0 { return 2 }
    return 0
  }
  
  func numberOfRows(at section: Int) -> Int {
    if section == 0 {
      return numberOfItems
    }
    return 1
  }
  
  func moveTextToTop(_ text: String) {
    dataSource.remove( text )
    dataSource.insert( text, at:0 )
    
    UserDefaults.standard.set( dataSource, forKey:"SavedHistory")
  }
  
  func moveToTop(at index: Int) -> String {
    let text = dataSource.object( at: index ) as! String

    dataSource.remove( text )
    dataSource.insert( text, at:0 )
    
    UserDefaults.standard.set( dataSource, forKey:"SavedHistory")
    return text
  }
  
  func removeAll() {
    dataSource.removeAllObjects()
    UserDefaults.standard.removeObject( forKey: "SavedHistory")
  }
  
  func remove(at index: Int) {
    dataSource.removeObject( at: index )
    UserDefaults.standard.set( dataSource, forKey:"SavedHistory")
  }
  
  func string(at index: Int) -> String {
    return dataSource.object( at: index ) as! String

  }
}
