//
//  ReaderViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 18/12/16.
//
//

import Foundation


struct Selection {
  var selectedRange: ClosedRange<Int>!
  var pageRange: ClosedRange<Int16>!
}

protocol PDFDocumentPresentable {
  var document: ReaderDocument! { set get }
  var pdfDocRef: CGPDFDocument { set get }
  
  var viewWidth: CGFloat { get set }
  var pageWidth: CGFloat { get }
  
  var contentWidth: CGFloat { get }
  var contentOffsetX: CGFloat { get }
  var pageCount: Int { get }
  
  var lastOpen: Date { get set }

  var fileURL: URL { get }
  var password: String? { get }

  func saveProperties()
  func updateProperties()
  
}

extension PDFDocumentPresentable {
  var SCROLLVIEW_OUTSET_SMALL: CGFloat { return 0.0 }
  var SCROLLVIEW_OUTSET_LARGE: CGFloat { return 0.0 }
  var baseZoom: CGFloat {
    
    if UIDevice.current.userInterfaceIdiom == .pad { return 1.15 }
    
    // iPhone X
    
    if UIScreen.main.bounds.size.height == 812 {
      return 1.7
    }
    
    return 1.55
  }
  var offsetY: CGFloat {
    
    if UIDevice.current.userInterfaceIdiom == .pad { return 0 }
    
    // iPhone X
    
    if UIScreen.main.bounds.size.height == 812 {
      return -80
    }
    
    return 0
  }
  var scrollViewOutset: CGFloat {
    
    if UIDevice.current.userInterfaceIdiom == .pad { return SCROLLVIEW_OUTSET_LARGE  }
    if UIScreen.main.bounds.size.height == 812 {
      return SCROLLVIEW_OUTSET_SMALL
    }
    return  SCROLLVIEW_OUTSET_SMALL
  }
  
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
}

protocol SelectionPresentable {
  var selection: Selection? { get set }
}

struct ArticleTitleViewModel  {

  private var thisPage: Int?
  private var colorScheme: BackgroundColorSchemeType
  init(document: DocumentViewModel) {
    thisPage = document.pageNumber
    colorScheme = document.backgroundColorScheme
  }
  
  var viewWidth: CGFloat { return DEVICE_IS_IPAD ? 18 : 16 }
  var attributedTitle: NSAttributedString {
   
    guard let thisPage = thisPage else {
      return NSAttributedString(string: "")
    }
    let fontSize: CGFloat = DEVICE_IS_IPAD ? 14 : 13
    let attributes: [NSAttributedStringKey : Any] = [.font: UIFont.systemFont(ofSize: fontSize), .foregroundColor: colorScheme.subTextColor, NSAttributedStringKey.verticalGlyphForm: 1]
    let boldAttributes: [NSAttributedStringKey : Any] = [.font: UIFont.boldSystemFont(ofSize: fontSize), .foregroundColor: colorScheme.textColor, NSAttributedStringKey.verticalGlyphForm: 1]
    
    if let article = DataSource.shared.pageTitle(atPage: Int16(thisPage)) {
      let law = NSMutableAttributedString(string: article.law.humanReadableTitle + " " , attributes: attributes)
      let article = NSMutableAttributedString(string: article.encodedTitle.decodedArticleTitle.transliterateToKanjiNumber(), attributes: boldAttributes)

      law.append(article)
      return law
    }
    
    return NSAttributedString(string: "", attributes: attributes)
  }

}

protocol DocumentViewModelDelegate: class {
  func focusedPageNumberDidChange()
  func selectionChanged(old: CGRect, new: CGRect, pages: ClosedRange<Int16>, showKnobs: Bool)
  //func highlightTouched(_ userInfo:[String: Any])
}

class DocumentViewModel: PDFDocumentPresentable, SelectionPresentable {
  
  static var currentDocumentViewModel: DocumentViewModel? = nil
  
  weak var delegate: DocumentViewModelDelegate?
  
  var document: ReaderDocument!
  var pdfDocRef: CGPDFDocument

  var viewWidth: CGFloat = 320
  var pageWidth: CGFloat { return viewWidth * baseZoom }
  
  var sideCutOff: CGFloat = 0 // pdf side offset
  
  var appIconName: String? {
    get {
      let ud = UserDefaults.standard
      ud.synchronize()
      if let appIconName = ud.object(forKey: "AppIcon") as? String {
        if appIconName == "Primary" { return nil }
        return appIconName
      }
      return nil
    }
    set {
      let ud = UserDefaults.standard
      ud.set(newValue, forKey: "AppIcon")
    }
  }
  
  init() {
    let doc = DataSource.shared.document!
    self.document = doc

    pdfDocRef = CGPDFDocumentCreateUsingUrl(doc.fileURL as CFURL!, doc.password).takeRetainedValue()
    DocumentViewModel.currentDocumentViewModel = self
  }
  
  var pageSize: CGSize {
    
    if let _PDFPageRef = pdfDocRef.page(at: 1) // Get page  .... COST MEMORY HERE
    {
      //CGPDFPageRetain(_PDFPageRef); // Retain the PDF page
      var _pageWidth: CGFloat = 0
      var _pageHeight: CGFloat = 0
      var _pageOffsetX: CGFloat = 0
      var _pageOffsetY: CGFloat = 0
      
      let cropBoxRect = _PDFPageRef.getBoxRect(CGPDFBox.cropBox);
      let mediaBoxRect = _PDFPageRef.getBoxRect(CGPDFBox.mediaBox);
      let effectiveRect = cropBoxRect.intersection(mediaBoxRect);
      
      let _pageAngle = _PDFPageRef.rotationAngle; // Angle
      
      switch (_pageAngle) // Page rotation angle (in degrees)
      {
      case 90: fallthrough
      case 270: // 90 and 270 degrees
        _pageWidth = effectiveRect.size.height;
        _pageHeight = effectiveRect.size.width;
        _pageOffsetX = effectiveRect.origin.y;
        _pageOffsetY = effectiveRect.origin.x;
        
      case 0: fallthrough
      case 180: fallthrough
      default:
        // 0 and 180 degrees
        _pageWidth = effectiveRect.size.width;
        _pageHeight = effectiveRect.size.height;
        _pageOffsetX = effectiveRect.origin.x;
        _pageOffsetY = effectiveRect.origin.y;
      }
      
      
      var page_w = _pageWidth; // Integer width
      var page_h = _pageHeight; // Integer height
      
      if page_w.truncatingRemainder(dividingBy: 2) == 1 { page_w -= 1 }
      if page_h.truncatingRemainder(dividingBy: 2) == 1 { page_h -= 1 }
      
      let size = CGSize(width: page_w * baseZoom, height: page_h * baseZoom); // View size

      return size
    }
    else {
      return CGSize(width: 320, height: 480) // dummy
    }
  }
  
  var contentWidth: CGFloat {
    return pageWidth * CGFloat(pageCount)
  }
  
  var contentOffsetX: CGFloat {
    get {
      return pageWidth * CGFloat(pageNumber - 1)
    }
    set {
      if contentWidth == viewWidth { return }
      
      let thisPage = pageCount - Int(newValue / pageWidth + 0.5)
      if thisPage != pageNumber {
        pageNumber = thisPage
      }
    }
  }
  
  var pageCount: Int {
    return document.pageCount as! Int
  }
  
  var pageNumber: Int {
    get {
      return document.pageNumber as! Int
    }
    set {
      
      if document.pageNumber as! Int != newValue {
        document.pageNumber = newValue as NSNumber!
        delegate?.focusedPageNumberDidChange()
      }
    }
  }

  var lastOpen: Date {
    get {
      return document.lastOpen
    }
    set {
      document.lastOpen = newValue
    }
  }
  
  var fileURL: URL {
    return document.fileURL as URL
  }
  
  var password: String? {
   return document.password
  }
  
  func saveProperties() {
    document.archiveDocumentProperties()
  }
  
  func updateProperties() {
    document.updateProperties()
  }
  
  var articleTitle: String {
    
    if contentWidth == viewWidth { return "" }
    let percent = (contentOffsetX / (contentWidth - viewWidth)) * 100;

    return NSString(format: "%.0f%%", percent) as String
  }
  
  //MARK:- SELECTIONS
  
  private func getSelectionDimensions(oldSelection: Selection? = nil, newSelection: Selection? = nil) -> (newMojis: [Moji]?, oldMojis: [Moji]?, affectedRectOld: CGRect, affectedRectNew: CGRect, affectedPages: ClosedRange<Int16>)? {
    var newMojis: [Moji]? = nil
    var oldMojis: [Moji]? = nil
    if oldSelection == nil && newSelection == nil { return nil }
    
    func getMojis(from selection: Selection) -> [Moji] {
      
      let pageRange = selection.pageRange!
      let mojiRange = selection.selectedRange!
      
      let pages = RealmRepository.shared.pages(in: pageRange)
      var mojis: [Moji] = []
      for page in pages {
        mojis += Array( page.mojis.filter("validCharacterNumber >= %@ && validCharacterNumber <= %@", mojiRange.lowerBound, mojiRange.upperBound).sorted(byKeyPath: "validCharacterNumber") )
      }
      return mojis
    }
    
    newMojis = (newSelection == nil) ? nil : getMojis(from: newSelection!)
    oldMojis = (oldSelection == nil) ? nil : getMojis(from: oldSelection!)
    
    // GET RECTS
    var affectedRectOld = CGRect.zero
    var affectedRectNew = CGRect.zero
    
    func affectedRect(from mojis: [Moji]) -> CGRect {
      var affectedRect = CGRect.zero
      if mojis.count > 500 {
        affectedRect.size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
      }else {
        for moji in mojis {
          let bounds =  moji.bounds
          affectedRect = affectedRect.union(bounds)
        }
      }
      return affectedRect
    }
    
    if oldMojis != nil { affectedRectOld = affectedRect(from: oldMojis!) }
    if newMojis != nil { affectedRectNew = affectedRect(from: newMojis!) }
    
    var affectedPages: ClosedRange<Int16>? = nil
    
    if oldMojis != nil && oldMojis!.isEmpty == false {
      affectedPages = oldMojis!.first!.page ... oldMojis!.last!.page
    }
    
    if newMojis != nil && newMojis!.isEmpty == false {
      let pagesNew:ClosedRange<Int16> =  newMojis!.first!.page ... newMojis!.last!.page
      if affectedPages == nil {
        affectedPages = pagesNew
      }else {
        affectedPages = min(affectedPages!.lowerBound, pagesNew.lowerBound) ... max(affectedPages!.upperBound, pagesNew.upperBound)
      }
    }
    
    return (newMojis, oldMojis, affectedRectOld, affectedRectNew, affectedPages!)
  }
  
  var selection: Selection? = nil {
    didSet {
      if oldValue == nil && selection == nil { return }
      
      guard let (newMojis, _ , affectedRectOld, affectedRectNew, affectedPages) = getSelectionDimensions(oldSelection: oldValue, newSelection: selection) else { return }
      selectedMojis = newMojis
      
      delegate?.selectionChanged(old: affectedRectOld, new: affectedRectNew, pages: affectedPages, showKnobs: true)
    }
  }
  
  private(set) var selectedMojis: [Moji]? = nil
  
  var selectedHighlight: Highlight? = nil {
    didSet {
      
      if oldValue == nil && selectedHighlight == nil { return }
      
      let oldSelection = (oldValue == nil) ? nil : Selection(selectedRange: oldValue!.startIndex ... oldValue!.endIndex, pageRange: oldValue!.startPage ... oldValue!.endPage)
      
      let newSelection = (selectedHighlight == nil) ? nil : Selection(selectedRange: selectedHighlight!.startIndex ... selectedHighlight!.endIndex, pageRange: selectedHighlight!.startPage ... selectedHighlight!.endPage)
      
      guard let (_, _ , affectedRectOld, affectedRectNew, affectedPages) = getSelectionDimensions(oldSelection: oldSelection, newSelection: newSelection) else { return }
      
      delegate?.selectionChanged(old: affectedRectOld, new: affectedRectNew, pages: affectedPages, showKnobs: false)
    }
  }
  
  func selectPoint(_ point: CGPoint, page: Int16) {
    
    if let location = DataSource.shared.mojiLocation(at: point, page: page) {
      
      let (range, pages) = DataSource.shared.wordRange(at: location, page: page)
      selection = Selection( selectedRange: range, pageRange: pages  )
    }
  }
  
  func selectHighlight(at point: CGPoint? = nil, page: Int16? = nil) -> Any? {
    
    guard let point = point else {
      selectedHighlight = nil
      return nil
    }
    
    guard let page = page else { return nil }
    
    guard let mojiLocation = DataSource.shared.mojiLocation(at: point, page: page) else { return nil }
    
    var foundHighlight: Highlight? = nil
    let objects = RealmRepository.shared.highlightsInPages(page...page)
    for highlight in objects {
      if highlight.startIndex <= mojiLocation &&  mojiLocation <= highlight.endIndex  {
        if foundHighlight == nil {
          foundHighlight = highlight
        }else if (foundHighlight!.endIndex - foundHighlight!.startIndex) > (highlight.endIndex - highlight.startIndex) {
          foundHighlight = highlight
        }
      }
    }
    self.selectedHighlight = foundHighlight
    return foundHighlight as Any
  }

  func enumerateUserHighlightMojiBounds(inPage page: Int16, handler: ((_ bounds:[CGRect], _ color: HighlightColor, _ notes: String?)->Void))  {
    let pageInt = Int16(page)
    let objects = RealmRepository.shared.highlightsInPages(pageInt...pageInt)
    
    //print("objects \(objects.count) at \(page)")
    for highlight in objects {
      if let pageObj = RealmRepository.shared.page(at: Int16(page)) {
        
        //print("- \(highlight.startIndex) - \(highlight.endIndex), color \(highlight.color.color())")
        let mojis = Array(pageObj.mojis.filter("page == %@ && validCharacterNumber >= %@ && validCharacterNumber <= %@", Int16(page), highlight.startIndex, highlight.endIndex)).map { $0.bounds }
        
        if self.selectedHighlight == highlight {
          HighlightDrawer.shared.drawHighlightInContext(mojis, color: .selection, hasShadow: true, isSelection: true)
        }
        
        handler(mojis, highlight.color, highlight.notes)
      }
    }
  }
  
  func enumerateUserDiffMojiBounds(inPage page: Int16, handler: ((_ bounds:[CGRect], _ color: HighlightColor, _ sideNote: String?)->Void))  {
    let pageInt = Int16(page)
    let objects = RealmRepository.shared.diffsInPages(pageInt...pageInt)
    
    //print("objects \(objects.count) at \(page)")
    for highlight in objects {
      if let pageObj = RealmRepository.shared.page(at: Int16(page)) {
        
        //print("- \(highlight.startIndex) - \(highlight.endIndex), color \(highlight.color.color())")
        let mojis = Array(pageObj.mojis.filter("page == %@ && validCharacterNumber >= %@ && validCharacterNumber <= %@", Int16(page), highlight.startIndex, highlight.endIndex)).map { $0.bounds }

        handler(mojis, highlight.color, highlight.notes)
      }
    }
  }
}

protocol RatingsSource {
  var rating: Int? { get }
  var title: String? { get }
  var reviewText: String? { get }
  var userUuid: String? { get }
  var version: String { get }
}

struct SSRatingPickerViewModel: RatingsSource {
  
  let ReviewCount = 2
  let ReviewLaterCount = 50

  private(set) var rating: Int?
  private(set) var title: String?
  private(set) var reviewText: String?
  private(set) var userUuid: String?
  
  var version: String {
    return ObjCBridge.versionString()
  }
 
  init() {
    let ub = NSUbiquitousKeyValueStore.default
    if let _ = ub.object(forKey: "userUuid") {
      
    }else {
      ub.set(shortUUIDString(), forKey: "userUuid")
      ub.synchronize()
    }
    
    rating = ub.object(forKey: "rating") as? Int
    title = ub.object(forKey: "title") as? String
    reviewText = ub.object(forKey: "reviewText") as? String
    userUuid = ub.object(forKey: "UserUuid")  as? String
    
  }
  
  mutating func set(ratings: Int, title: String, reviewText: String) {
    
    self.rating = ratings
    self.title = title
    self.reviewText = reviewText
    
    let ud = UserDefaults.standard
    // let version = ObjectiveCClass.versionString()
    //let key = "ReviewAppVersion" + version!
    let key = ObjCBridge.versionString() // "ReviewAppVersion" + version!
    
    ud.set(Int.max, forKey: key )
    ud.synchronize()
    
    let ub = NSUbiquitousKeyValueStore.default
    let version = ObjCBridge.versionString()

  
    ub.set(rating, forKey: "rating")
    ub.set(title, forKey: "title")
    ub.set(reviewText, forKey: "reviewText")
    ub.set(version, forKey: "review_version")
    ub.synchronize()
    
    DataSource.shared.saveRating(self)

  }
  
  func didReview() {
    
    let ud = UserDefaults.standard
    // let version = ObjectiveCClass.versionString()
    //let key = "ReviewAppVersion" + version!
    let key = ObjCBridge.versionString() // "ReviewAppVersion" + version!
    
    ud.set(Int.max, forKey: key )
    ud.synchronize()
    
    let ub = NSUbiquitousKeyValueStore.default
    let version = ObjCBridge.versionString()
    
    ub.set(version, forKey: "review_version")
    ub.synchronize()
    
    DataSource.shared.saveRating(self)
    
  }
  
  func reviewLater() {
    let ud = UserDefaults.standard
    let key = ObjCBridge.versionString()
    
    ud.set(ReviewLaterCount, forKey: key )
    ud.synchronize()
  }
  
  func shouldReview() -> Bool {

    let ud = UserDefaults.standard
    ud.synchronize()
    let ub = NSUbiquitousKeyValueStore.default
    ub.synchronize()
    let key = ObjCBridge.versionString()
    if key != ub.object(forKey: "review_version") as? String {
      
      let count = ud.integer( forKey: key )
      
      if count != Int.max {
        ud.set(count + 1, forKey: key )
        ud.synchronize()
        
        if count > ReviewCount  {
          //// Review message
          
          return true
        }
      }
    }
    
    return false
  }

}
