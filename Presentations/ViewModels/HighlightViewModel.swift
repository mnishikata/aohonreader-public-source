//
//  HighlightViewModel.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 18/12/16.
//
//

import Foundation

protocol HighlightProtocol {
  
  var notes: String! { get }
  var color: HighlightColor? { get }
}

struct HighlightViewModel: HighlightProtocol {
  
  let minimumContentWidth: CGFloat = 300
  let minimumContentHeight: CGFloat = 52
  let maximumTextViewHeight: CGFloat = 250
  
  var changedFlag = false
  var backgroundColorScheme: BackgroundColorSchemeType {
    let obj = UserDefaults.standard.object(forKey: "BackgroundColorScheme") as? String
    let backgroundColorScheme = BackgroundColorSchemeType( fromUserDefaults: obj )
    return backgroundColorScheme
  }
  
  var notes: String! {
    didSet {
      if oldValue != notes {
        changedFlag = true
      }
    }
  }
  var color: HighlightColor? {
    didSet {
      if oldValue != color {
        changedFlag = true
      }
    }
  }
  
  var requiresConfirmOnClosing: Bool {
    return notes?.isEmpty == false
  }
  
  func setupTextViewVisiblity(_ textView: UITextView?) {
    if notes != nil && notes.isEmpty == false {
      textView?.text = notes!
      textView?.isHidden = false
      
    }else {
      textView?.text = nil
      textView?.isHidden = true
    }
  }

  func contentSize(with textView: UITextView) -> CGSize {
    
    if (notes == nil || notes?.isEmpty == true) {
      return CGSize(width: minimumContentWidth, height: minimumContentHeight)
    }
    
    let frame = textView.frame
    var width = minimumContentWidth
    
    let newSize = textView.sizeThatFits( CGSize(width: width - 16, height: 1000000) )
    var height = newSize.height
    
    if width < minimumContentWidth { width = minimumContentWidth }
    if height > maximumTextViewHeight { height = maximumTextViewHeight }
    
    let viewHeight = frame.origin.y + height //+ ( self.view.bounds.maxY - frame.maxY )
    
    return CGSize(width: width, height: viewHeight)
  }
}
