//
//  MNVerticalTextLabel.h
//  TuiFramework
//
//  Created by Masatoshi Nishikata on 16/10/12.
//
//

#import "MyAttributedTextLabel.h"

@interface MyVerticalTextLabel : MyAttributedTextLabel
@property (nonatomic, strong) NSString* backgroundScheme;
@end
