//
//  HighlightListTableViewController.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 10/12/16.
//
//

import UIKit

class HighlightListTableViewController: UITableViewController {
  
  var jumpAction: ((_ startIndex: Int)->Void)? = nil
  let viewModel = HighlightListTableViewModel()
  
  var documentInteractionController: UIDocumentInteractionController? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let item = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
    self.navigationItem.leftBarButtonItem = item
    
    let exportButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(export(_:)))
    self.navigationItem.rightBarButtonItem = exportButton
    
    let nib = UINib(nibName: "HighlightCell", bundle: nil)
    self.tableView.register(nib, forCellReuseIdentifier: "HighlightCell")
    
    self.tableView.backgroundColor = viewModel.backgroundColorScheme.tableBackgroundColor
    if viewModel.backgroundColorScheme.inverted {
      tableView?.separatorColor = UIColor(white: 1, alpha: 0.3)
      tableView?.indicatorStyle = .white
    }
    title = WLoc("Highlights")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.tableView.reloadData()
    
    navigationController?.navigationBar.barTintColor = viewModel.backgroundColorScheme.tableBackgroundColor
    navigationController?.toolbar.barTintColor = viewModel.backgroundColorScheme.tableBackgroundColor
    navigationController?.navigationBar.barStyle = viewModel.backgroundColorScheme.barStyle
    navigationController?.view.tintColor = viewModel.backgroundColorScheme.tintColor
    navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: viewModel.backgroundColorScheme.textColor, .font: UIFont.preferredFont(forTextStyle: .headline)]
  }
  
  @objc func close() {
    dismiss(animated: true, completion: nil)
  }
  
  @objc func export(_ sender: UIBarButtonItem) {

    let controller = UIAlertController(title: WLoc("書き出し"), message: "", preferredStyle: .actionSheet)
    
    let action1 = UIAlertAction(title: WLoc("テキスト"), style: .default) { (action) -> Void in
      self.exportAsText(sender)
    }
    
    let action2 = UIAlertAction(title: WLoc("新しい版に移行"), style: .default) { (action) -> Void in
      self.exportToMigrate(sender)
    }
    
    
    let delete = UIAlertAction(title: WLoc("Delete"), style: .destructive) { (action) -> Void in
      self.delete()
    }
    
    
    let cancel = UIAlertAction(title: WLoc("Cancel"), style: .cancel) { (action) -> Void in  }
    
    controller.addAction(action1)
    #if EDITION20
    #else
      controller.addAction(action2)
    #endif
    //controller.addAction(delete)

    controller.addAction(cancel)
    controller.popoverPresentationController?.barButtonItem = sender
    present(controller, animated: true, completion: nil)
  }
  
  func delete() {
    let highlights = RealmRepository.shared.allHighlights
    for highlight in highlights {
    RealmRepository.shared.destroyHighlight(highlight)
    }
    ICloudRepository.shared.saveToICloud()
  }
  
  func exportAsText(_ sender: UIBarButtonItem) {
    let string = viewModel.highlightExportList
    
    let controller = UIActivityViewController(activityItems: [string], applicationActivities: [])
    controller.modalPresentationStyle = .popover
    let popover = controller.popoverPresentationController
    popover?.barButtonItem = sender
    
    present(controller, animated: true, completion: nil)
  }
  
  func exportToMigrate(_ sender: UIBarButtonItem) {
    
    let controller = ImportingHighlightsViewController()
    controller.exporting = true
    present(controller, animated: true) {
      
      controller.exportHighlights() { data in
        self.dismiss(animated: true) {
          
          // DO SOMETHING
          //UIDocumentInteractionController
          //com.catalystwo.Aohon.highlights
          let url = URL(fileURLWithPath: NSTemporaryDirectory().appending("tempHighlight.comcatalystwoaohonhighlights"))
          try? data.write(to: url)
          
          let controller = UIDocumentInteractionController(url: url)
          controller.uti = "com.catalystwo.Aohon.highlights"
          self.documentInteractionController = controller
          
          if false == controller.presentOpenInMenu(from: sender, animated: true) {
            ShowAlertMessage("No App was found", "Use this option to migrate highlight data to another Aohon app.", self)
          }
        }
      }
    }
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return viewModel.numberOfHighlights
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return viewModel.rowHeight
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "HighlightCell", for: indexPath)
    cell.backgroundColor = viewModel.backgroundColorScheme.tableBackgroundColor

    if viewModel.backgroundColorScheme.inverted {
      let view = UIView()
      view.backgroundColor = viewModel.backgroundColorScheme.tableSelectionColor
      cell.selectedBackgroundView = view
    }
    
    let label = cell.viewWithTag(1) as! UILabel
    
    label.attributedText = viewModel.highlightAttributedString(at: indexPath.row)
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    jumpAction?(DataSource.shared.locationOfHighlight(at: indexPath.row))
    if tableView.indexPathForSelectedRow != nil {
    tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
    }
  }
}

