//
//  StartingPointViewController.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 10/11/16.
//
//

import UIKit

class StartingPointViewController: UIViewController, UIDocumentPickerDelegate {

  @IBOutlet weak var highlight: UIButton!
  @IBOutlet weak var safariGuideButton: UIButton!
  @IBOutlet weak var videoButton: UIButton!
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
  override func viewDidLoad() {
    self.view.window?.backgroundColor = UIColor.white
    if DataSource.shared.isReady || RealmRepository.shared.isDocumentInstalled {
      self.view.subviews.forEach { $0.isHidden = true }
    }else {
      
      #if EDITION20
        textView.text = WLoc("Start Message 20")
        highlight.isHidden = true
      #else
        textView.text = WLoc("Start Message 19")
        safariGuideButton.isHidden = true
        //videoButton.isHidden = true
        highlight.isHidden = false
      #endif
      
      textView.sizeToFit()
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    if DataSource.shared.isReady {
      start()
      return
    }
    
    if RealmRepository.shared.isDocumentInstalled  {
      decompress(self)
      return
    }
  }
  
  @IBAction func highlightAction(_ sender: Any) {
    let controller = HighlightListTableViewController(style: .grouped)
    controller.jumpAction = { startIndex in }
    
    let nav = UINavigationController(rootViewController: controller)
    nav.modalPresentationStyle = .formSheet
    present(nav, animated: true, completion: nil)
  }
  
  @IBAction func shoeVideo(_ sender: Any) {
    let address = "http://catalystwo.com/AohonReaderNewVersion.html"
    UIApplication.shared.openURL(URL(string: address)!)
  }
  
  @IBAction func load(_ sender: UIButton) {
    
    if RealmRepository.shared.isDocumentInstalled  {
      decompress(self)
      return
    }
    
    /*
    #if DEBUG
      let url = Bundle.main.url(forResource: "all", withExtension: "pdf")!
      try! FileManager.default.copyItem(at: url, to: DataSource.shared.documentUrl)
      return
    #endif
    */
    
    let picker = UIDocumentPickerViewController(documentTypes: ["com.adobe.pdf"], in: .import)
    picker.delegate = self
    present(picker, animated: true, completion: nil)
  }
  
  @IBAction func openLink(_ sender: AnyObject) {
    let path = "https://www.jpo.go.jp/shiryou/hourei/kakokai/cikujyoukaisetu.htm"
    UIApplication.shared.openURL(URL(string: path)!)
  }
  
  @IBAction func decompress(_ sender: Any) {

    if DataSource.shared.isReady {
      start()
      return
    }
    activityIndicatorView.isHidden = false
    activityIndicatorView.startAnimating()
    
    DispatchQueue.main.async {
      defer { self.activityIndicatorView.stopAnimating() }
      if let err = DataSource.shared.installDatabase() {
        let alertController = UIAlertController(title: WLoc("?"), message: err.localizedDescription, preferredStyle: .alert)
        let cancel = UIAlertAction(title: WLoc("OK"), style: .cancel) { (action) -> Void in }
        
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
        return
      }
      
      // startAnimating
      self.start(animated: true)
    }
  }
  
  func start(animated: Bool = false) {
    
    let controller = ReaderViewControllerSwift(readerDocument: DocumentViewModel())
    present(controller, animated: animated, completion: nil)
  }
  
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    _ = url.startAccessingSecurityScopedResource()
    defer {  url.stopAccessingSecurityScopedResource() }
    
    
    do {
      if try DataSource.shared.installPDF(from: url) == false {
        
        let alertController = UIAlertController(title: WLoc("?"), message: WLoc("Not the right PDF"), preferredStyle: .alert)
        let cancel = UIAlertAction(title: WLoc("OK"), style: .cancel) { (action) -> Void in }
        
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
        
        return
      }
      
      // NEXT
      decompress(self)
      
    }catch let error {
      let alertController = UIAlertController(title: WLoc("?"), message: error.localizedDescription, preferredStyle: .alert)
      let cancel = UIAlertAction(title: WLoc("OK"), style: .cancel) { (action) -> Void in }
      
      alertController.addAction(cancel)
      present(alertController, animated: true, completion: nil)
    }
  }
  
  

}
